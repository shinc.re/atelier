
# 배치방식으로 ML 모델 예측결과 얻기 (BatchMLPredictInSingleEngine)

Atelier는 기존에 학습된 모델을 이용하여, 예측하는 기능을 제공합니다. 이 예제에서는 자료를 Batch 형태로 입력받아 예측하는 과정을 설명합니다.
기존에 학습된 모델은 ML 모델 학습하기 (https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.5.5.BatchAutoMLTrainInSingleEngine.md) 예제에서 학습된 모델을 활용합니다.

## 입력 데이터 준비하기
입력데이터는 Apache Spark이 제공하는 DataFrame 을 저장하고 있는 parquet 파일이어야 합니다. 이 때 DataFrame은 "features" 칼럼과 "label" 칼럼을 반드시 포함하고 있어야 합니다. "features" 칼럼은 VectorAssembler 형태의 자료여야 합니다 (참고: https://spark.apache.org/docs/latest/ml-features.html#vectorassembler). "label" 칼럼은 Classification의 경우에는 수치 혹은 문자열이어야 하며 (예: "cat", "dog"), Regression의 경우에는 수치값이어야 합니다.

본 예제에서는 Host PC의 "/home/csle/ksb-csle/pyML/autosparkml/datasets/iris_dataset" 폴더를 **Storage** 의 "dataset/iris_dataset" 로 업로드하여 사용합니다.

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여, 아래 과정을 거쳐 워크플로우를 생성합니다.


- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  |  BatchMLPredictInSingleEngine |  워크플로우 이름
description  | AutoML로 학습된 모델로부터 예측하는 예제 | 워크플로우 설명
isBatch  |  true | Batch 실행여부
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |   
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름



### 엔진 선택
기계학습모델 예측기능을 사용하기 위해 **Batch** 엔진을 드래그앤 드롭합니다.

#### Reader
파일을 읽기위해 **FileBatchReader** 를 드래그앤 드롭하고, 아래표와 같이 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  |  dataset/iris_dataset | 파일경로
fileType  | PARQUET |  파일형태
delimiter  |  , |  구분자
field  |   | 상세설정
header  | false  |  헤더포함여부

모델에 예측을 요청할 데이터를 **Storage** 에 아래의 그림과 같이 업로드 합니다.

![2.5.6_fileupload](https://i.imgur.com/tg6lvSD.png)



filePath는 직접 입력할 수도 있으며, 아래 화면처럼 GUI를 이용해 선택할 수도 있습니다 (filePath 속성에 "File" 버튼 클릭). 기타 속성은 기본값으로 두면 됩니다. <b>참고</b>: 상기하였듯이, 입력파일 형태는 parquet 만 지원하기 때문에 기타 속성을 변경하여도 적용되지 않습니다.

![2.5.6_read](https://i.imgur.com/CZkkiI7.png)

#### Writer
예측결과물을 파일형태로 저장하기 위해 **FileBatchWriter** 를 드래그앤 드롭합니다.

field  |value   | 설명
--|---|--
filePath  |  applications/autosparkml/sparkMLpredict |  저장경로
fileType  | CSV |  파일형태
delimiter  |  , |  구분자
field  |   | 상세설정
header  | false  |  헤더포함여부

Reader의 경우와 마찬가지로 filePath는 직접 입력할 수도 있으며, GUI를 이용해 선택할 수도 있습니다.

#### Controller
Controller 로는 **SparkSessionController** 를 드래그앤 드롭합니다.

#### Runner
SimpleSparkRunner 를 드래그앤 드롭합니다.

field  |value   | 설명
--|---|--
inJason  |  false | Jason 형태 파라미터 전달여부
sparkArgs  |   |  Spark 상세설정

sparkArgs 버튼을 누르면 아래와 같이 Apache Spark 실행환경을 세부설정할 수 있는 창이 뜹니다. 이 예제에서는 기본값을 사용합니다.

![2.5.6_spark_args](https://i.imgur.com/Vda2sCu.png)

#### Operator
SparkMLPredictOperator 를 드래그앤 드롭합니다. 속성값을 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
modelPath  | model/autosparkml/test/0000/  | 학습된 모델 경로
clsNameForModel  | org.apache.spark.ml.PipelineModel  |  모델 Class 명

modelPath는 모델경로입니다. <font color="red">따라서 반드시 기존에 학습된 모델이 존재해야 합니다.</font> 이 예쩨에서는 BatchMLTrainInSingleEngine 혹은 BatchAutoMLTrainInSingleEngine 예제에서 생성된 모델의 경로를 지정합니다 (예: /model/autosparkml/0000). 수동으로 입력할 수도 있고, "File" 버튼을 눌러 GUI를 이용해 선택할 수도 있습니다.

### 워크플로우 완성 화면

아래 그림은 위 과정을 거쳐 완성된 워크플로우 화면입니다.

<b>참고</b>: 이미 만들어진 워크플로우를 사용할 때에도, iris_dataset 은 HDFS 에 따로 업로드하여야 합니다. 또한 예측에 사용할 모델이 미리 학습되어 있어야 합니다.

![2.5.6_01](https://i.imgur.com/SOVc6Lm.png)

## 워크플로우 실행 및 모니터링하기


### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.


### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인
웹툴킷 상단 "Monitoring" 메뉴의 "Workflow" 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Inprogress</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)할 수 있으며, 종료된 워크를로우를 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수도 있습니다.

![2.5.6_02](https://i.imgur.com/Xko0HAk.png)

#### 워크플로우 로그 보기
위 화면에서 "WorkFlow History" 탭을 선택하면, Atelier가 수행한 워크플로우들의 목록 및 각각의 로그 (목록 최우측의 <b><span style="color:#6698FF">i</span></b> 버튼 클릭)를 확인할 수 있습니다.

![2.5.6_03](https://i.imgur.com/O4lcyRX.png)

위 화면에서 "Text 결과 파일 보기" 버튼을 누르면 학습성능 파일을 아래처럼 열어볼 수 있습니다. Classification 경우에는 $F_1$ 스코어가 기록됩니다. Regression의 경우에는 MAE (Mean Absolute Error), MSE (Mean Squared Error) 및 MAPE (Mean Absolute Percentage Error)가 기록됩니다.


![2.5.6_04](https://i.imgur.com/fEZSxP6.png)

## 결과 확인하기

### 최적학습모델 확인하기
워크플로우 실행의 결과물은 웹툴킷 상단의 **Storage** 메뉴에서 확인할 수 있습니다. 먼저 "applications" 폴더 안의 워크플로우 ID 폴더로 들어갑니다 (워크플로우 ID는 Monitoring 메뉴에서 확인 가능합니다). 해당 경로안에 "output" 폴더가 생성되어 있고 FileWriter 에서 설정한 "autosparkml" 파일이 생성되어 있으면 워크플로우가 성공적으로 수행된 것입니다 (아래 그림 참조). 생성된 파일은 파일은 위에서 설명한 "Text 결과 파일 보기" 버튼을 눌러 보이는 결과를 포함하는 파일입니다.

![2.5.6_05](https://i.imgur.com/9MVhLpL.png)


# 비식별 처리하기 (DeidentificationExample)

2016년 6월 정부합동부처는 개인 정보 보호 및 활용성 증대를 위해 '개인정보비식별 조치 가이드라인'을 발간하여, 가명처리, 총계처리, 데이터삭제, 데이터범주화, 잡음추가로 분류되는 총 17가지의 비식별 처리 기능을 정의하고 이를 활용하여 개인정보 데이터를 비식별 처리할 것을 권고하고 있습니다.

본 예제에서는 위의 가이드라인에 기반하여, 개인정보 및 개인민감정보가 담겨있는 데이터에 다양한 비식별 처리 기능을 적용하여 개인 정보를 보호하는 방법을 보여줍니다.

## 입력 데이터 준비하기
실행을 위해서 Host PC의 /home/csle/ksb-csle/examples/input 폴더에 존재하는 `adult.csv` 파일(성별, 나이 등의 개인정보가 담겨 있습니다)을 사용자 Storage에 웹툴킷을 이용하여 dataset/input 폴더에 등록합니다.

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 개인정보를 비식별처리하는 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | DeidentificationExample | 워크플로우 이름     
description  | 원본 데이터에 총계처리, 가명처리, 부분식별자삭제, 암호화 비식별화 기능을 적용한 예제  | 워크플로우를 설명하는 글
isBatch  | true | 배치 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |   
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름

### 엔진 선택
비식별 처리는 기본적으로 파일 및 대용량의 데이터를 한꺼번에 읽어 들여 비식별 처리 및 적정성 검증을 수행한 후 그 결과를 파일 및 기타 저장공간에 저장하는 형식으로 이루어집니다. 모든 작업이 배치성으로 동작하기에, 배치성 작업을 수행할 수 있는 Batch 엔진을 드래그앤 드롭합니다.

각 엔진에는 어떤 데이터를 읽어올 것인지 (reader), 읽은 데이터를 어떤 방법으로 처리할 건지 (operator), 처리된 데이터는 어디에 저장할 것인지 (writer), 전체적인 작업을 어떤 식으로 제어하며 (controller), 구동할 것인지 (runner) 등의 정보를 입력해야 합니다.

- 엔진 속성

순번  | 엔진 Type | NickName  
--|---|---
1  | Batch  | DeIdentificationEngine  

#### Reader

BeeAI **Stroage** 에 등록된 성별, 주소 등의 개인정보가 담겨있는 `adult.csv` 파일을 읽어들입니다. 파일을 읽어들이기에, FileBatchReader를 선택하고 아래와 같이 속성을 지정해줍니다.

field  |value   | 설명
--|---|--
filePath  | dataset/input/adult.csv |  입력 파일 경로를 입력합니다.
fileType  |  CSV |  읽을 파일의 타입을 입력합니다.
delimiter  |  ; |  컬럼간 구분자를 지정합니다.  
filed  | 입력없음  | 컬럼의 정보를 입력합니다 (헤더를 포함하여 읽어들였기에 여기선 입력을 안합니다).  
header  |  true |  파일의 header 포함 유무를 입력합니다.
saveMode  | OVERWRITE |  사용하지 않음


#### Writer
최종 비식별 처리된 결과를 어떻게 저장할 건지 지정합니다. 여기선 파일로 저장할 것이기에, FileBatchWriter 를 선택하고 아래와 같이 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  | output/didentification.csv |  결과 파일의 저장경로를 입력합니다.
fileType  |  CSV |  저장파일의 타입을 입력합니다.
delimiter  |  , |  컬럼간 구분자를 지정합니다.  
header  |  true |  저장파일의 header 포함 유무를 입력합니다.
saveMode  | OVERWRITE  |  파일 저장 방식을 선택합니다.

#### Controller
Controller 로는 SparkSessionController 를 선택합니다. Spark 환경에서 배치에 사용하는 컨트롤러입니다.


#### Runner
SimpleSparkRunner를 선택합니다.

field  |value   | 설명
--|---|--
master |  local [ * ] |  Spark local 모드를 입력합니다.
numExecutors  | 2 |  Spark의 executor 갯수를 입력합니다.
executorCores  | 2  |  Spark의 현재 할당된 코어수를 입력합니다.
executerMemory  |  1g |  Spark의 executor 메모리 정보를 입력합니다.
driverMemory  |  1g |  Spark 드라이버에 할당할 메모리 정보를 입력합니다.
totalCores  |  2 |  Spark의전체 할당 가능한 코어수를 입력합니다.
sparkVersion  | 2.3.0 |  Spark 버전 정보를 입력합니다.

#### Operator
`adult.csv` 파일은 sex, age, race, marital-status, education, native-country, workclass, occupation, salary-class, 이렇게 총 9개의 컬럼으로 이루어져 있습니다.

본 예제에서는 위의 컬럼 중 age 컬럼에는 총계처리 기법(AggregationOperator)을, race 컬럼에는 휴리스틱 가명화 기법(HeuristicOperator)을, marital-status 컬럼에는 데이터 삭제 기법 중 식별자 부분 삭제 기법(PartialIdenReductionOperator)을, occupation 컬럼에는 가명처리 기법 중 암호화 기법(EncryptionOperator)을 적용하여 비식별 처리하는 방법을 보여줍니다.

비식별 처리 관련 모든 Operator에는 필수적으로 1) 처리할 컬럼에 대한 속성정보와, 2) 비식별 처리 후 적정성을 검증할 방법에 대한 정보를 포함하게 됩니다.

먼저, 데이터 컬럼에 대한 속성정보는 Operator의 fieldInfo를 클릭하여 설정합니다. fieldInfo는 아래와 같은 형식으로 정의되며, 여기서는 총계처리를 예제로 들어 설명합니다.

field  |value   | 설명
--|---|--
key  |  1 |  총계처리 기능을 적용할 칼럼 ID를 입력합니다.
type  |  INTEGER |  컬럼의 타입을 명시합니다 (ex., STRING, DOUBLE, ...).   
value  | age  |  컬럼의 이름을 입력합니다.  
attrType  | SENSITIVE  |  컬럼의 속성을 정의합니다 (IDENTIFIER,QUASI-IDENTIFIER, ...).
autoConfigured  |  true |  컬럼의 hierarchy를 자동으로 생성할건지, 파일로 읽어올것인지 결정합니다.
nLevels  |  5 |  자동으로 컬럼 hierarchy 생성시 최대 level을 몇으로 할건지 정의합니다.
filePath  | 입력없음  |  컬럼 내 데이터 hierarchy를 파일로 읽어올 경우, 그 파일의 위치를 명시해줍니다.

컬럼의 hierarchy는 데이터를 어떤 형식으로 일반화할건지 나타내는 것으로, 일반화 기법 적용시 필수적으로 정의되어야 합니다. 가령, '55132'라는 zip code 데이터가 주어진 경우 한번 일반화를 수행하면 '5514*'로 변경될 수 있습니다. 일반화 후에도 적정성 평가를 통과 못하면 추가적으로 일반화를 더 수행하여 '551**''와 같은 데이터로 변경을 하게 됩니다. 일반화 단계에 따른 데이터의 변화를 일반화 단계 정보라 하며, 이 정보는 사용자가 직접 파일 형식으로 저장하여 그 파일로부터 읽어들이게 됩니다. 또는 autoConfigured true 설정을 통해 자동적으로 일반화 단계 정보를 구성할 수도 있습니다 (자동 구성시, 데이터의 syntatic 정보는 고려되지 않은 채, 숫자 값의 크기 또는 문자열의 길이를 기반으로 단순히 일반화 단계가 정의됩니다)

데이터 처리자는 비식별 처리 후 개인정보 및 민감정보 제거여부에 대해 적정성 검증을 수행해야 합니다 (6월 발간된 비식별 조치가이드라인에서는 k-익명성 기반 적정성 검증만이라도 필수적으로 수행하길 권고하고 있습니다). 적정성 검증에 대한 정보는 다음과 같은 형식으로 정의됩니다. Operator 내 check라는 버튼을 클릭한 후 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
measureLoss  |  AECS |  원본 데이터 대비 비식별 처리된 데이터의 데이터 손실량을 측정합니다.
measureRisk  |  UNIQUENESS |  비식별 처리된 데이터의 재식별 위험성을 측정합니다.
checkAnonymity  |  KANONYMITYCHECK |  적정성 검증 수행 방법을 정의합니다.

비식별 처리 Operator들은 위의 컬럼 속성 정보와 적정성 검증 방법을 필수적으로 설정해야 합니다.

 1. **AggregationOperator**
age 컬럼은 개인의 속성을 나타내는 민감정보에 해당하기에, 다음의 설정을 통해 총계 처리 기능을 적용합니다.

field  |value   | 설명
--|---|--
selectedColumnId  |  1 |  총계처리 기능을 적용할 칼럼 ID를 입력합니다.
method  |  AVG |  어떤방법으로 총계처리할지를 입력합니다 (ex., MIN, MAX, AVG...).   
fieldInfo | 아래의 표 참고 | 컬럼의 속성을 정의합니다.
check  |  위의 공통 설정 |  적정성 평가를 어떻게 수행할지 정의합니다.

fieldInfo 설정은 다음과 같이 합니다.

field  |value   | 설명
--|---|--
key  |  1 |  총계처리 기능을 적용할 칼럼 ID를 입력합니다.
type  |  INTEGER |  컬럼의 타입을 명시합니다 (ex., STRING, DOUBLE, ...).   
value  | age  |  컬럼의 이름을 입력합니다.  
attrType  | SENSITIVE  |  컬럼의 속성을 정의합니다 (IDENTIFIER,QUASI-IDENTIFIER, ...).
autoConfigured  |  true |  컬럼의 hierarchy를 자동으로 생성할건지, 파일로 읽어올것인지 결정합니다.
nLevels  |  5 |  자동으로 컬럼 hierarchy 생성시 최대 level을 몇으로 할건지 정의합니다.
filePath  | 입력없음  |  컬럼 내 데이터 hierarchy를 파일로 읽어올 경우, 그 파일의 위치를 명시해줍니다.

위와 같이 설정하면 age 컬럼은 평균값으로 대체됩니다.

 2. **HeuristicOperator**
race 컬럼을 개인 민감정보로 가정하고, 다음의 설정을 통해 휴리스틱 가명처리 기능을 적용할 수 있습니다. 가명처리는 데이터 처리자가 manually 입력하거나, 랜덤문자열로 대체할 수 있습니다.

field  |value   | 설명
--|---|--
selectedColumnId |  2 |  가명처리 기능을 적용할 칼럼 ID를 입력합니다.
method  | HEUR_RANDOM  |  가명처리 방법을 선택합니다 (HEUR_RANDOM, HEUR_MANUAL, HEUR_FILE).
manualInfo  | - |  특정 데이터를 원하는 값으로 변경합니다.
randInfo  | 아래의 표 참고  |  데이터를 랜덤 문자열로 변경합니다.
fieldInfo | 아래의 표 참고 | 컬럼의 속성을 정의합니다.
check  |  위의 공통 설정 |  적정성 평가를 어떻게 수행할지 정의합니다.

본 예제에서는 데이터를 랜덤 문자열로 변경합니다. 랜덤 문자열을 생성을 위해, randInfo 클릭 후 다음과 같이 설정합니다.

field  |value   | 설명
--|---|--
randMethod  | MIXED | 숫자, 문자를 섞어서 랜덤문자를 생성합니다.  
length |  5 |  랜덤 문자열의 길이는 5로 정합니다.

fieldInfo 설정은 다음과 같이 합니다.

field  |value   | 설명
--|---|--
key  |  2 |  기능을 적용할 칼럼 ID를 입력합니다.
type  |  STRING |  컬럼의 타입을 명시합니다.   
value  | race  |  컬럼의 이름을 입력합니다.  
attrType  | SENSITIVE  |  컬럼의 속성을 정의합니다.
autoConfigured  |  true |  컬럼의 hierarchy를 자동으로 생성할건지, 파일로 읽어올것인지 결정합니다.
nLevels  |  5 |  자동으로 컬럼 hierarchy 생성시 최대 level을 몇으로 할건지 정의합니다.
filePath  | 입력없음  |  컬럼 내 데이터 hierarchy를 파일로 읽어올 경우, 그 파일의 위치를 명시해줍니다.


위와 같이 설정하면 race 컬럼의 각 데이터는 숫자, 문자 5글자로 이루어진 랜덤 문자로 대체되게 됩니다.

 3. **PartialIdenReductionOperator**
marital-status 컬럼에 식별자 부분 삭제 기능을 적용합니다. 가이드라인에 제시된 예시 ('서울특별시 송파구 가락본동 78번지 → 서울시 송파구'로 변경)에 기반하여 이 기능을 외국에서 사용하는 일반화 기법으로 간주합니다.

field  |value   | 설명
--|---|--
method |  DELETE |  삭제할건지 대체할 건지를 나타내는 필드이지만, 현재는 사용되지 않습니다.
columnHandlePolicy  | ONEBYONE |  식별자 컬럼 각각에 적용합니다.
generalizedColumnInfo   |  아래의 표 참고  |  일반화를 어떻게 수행할건지 정의합니다.
fieldInfo | 아래의 표 참고 | 컬럼의 속성을 정의합니다.
check  |  위의 공통 설정 |  적정성 평가를 어떻게 수행할지 정의합니다.

일반화 기법 적용시 일반화 단계에 대한 정보가 제공되어야 합니다. generalizedColumnInfo 를 다음과 같이 입력합니다.

field  |value   | 설명
--|---|--
selectedColumnId |  3 | 일반화 기능을 적용할 칼럼 ID를 입력합니다.  
numLevels  | 4  |  컬럼 hierarchy 생성시 최대 level을 몇으로 할건지 정의합니다.
curLevel  |  2 |  수행하고자 하는 일반화 단계를 입력합니다.

fieldInfo 설정은 다음과 같이 합니다.

field  |value   | 설명
--|---|--
key  |  3 |  기능을 적용할 칼럼 ID를 입력합니다.
type  |  STRING |  컬럼의 타입을 명시합니다.   
value  | marital-status  |  컬럼의 이름을 입력합니다.  
attrType  | IDENTIFIER  |  컬럼의 속성을 정의합니다.
autoConfigured  |  true |  컬럼의 hierarchy를 자동으로 생성할건지, 파일로 읽어올것인지 결정합니다.
nLevels  |  5 |  자동으로 컬럼 hierarchy 생성시 최대 level을 몇으로 할건지 정의합니다.
filePath  | 입력없음  |  컬럼 내 데이터 hierarchy를 파일로 읽어올 경우, 그 파일의 위치를 명시해줍니다.

위와 같이 설정시, marital-status의 데이터를 총 4단계로 구분하여 2단계로 일반화 기법을 적용하게 됩니다. 가령 marital-status내의 한 데이터가 8자리 문자열로 이루어진 'abcdefgh'라면, 1단계 일반화는 'abcdef*', 2단계 일반화는 'abcd*', 3단계 일반화는 'ab*'가 됩니다.

 4. **EncryptionOperator**
occupation 컬럼에 암호화 기법을 적용합니다.

field  |value   | 설명
--|---|--
selectedColumnId |  7  | 암호화 기능을 적용할 칼럼 ID를 입력합니다.
key  |  SHA1 |  key 생성시 사용할 알고리즘을 선택합니다.
method  | AES  |  암호화 방법을 선택합니다.
fieldInfo | 아래의 표 참고 | 컬럼의 속성을 정의합니다.
check  |  위의 공통 설정 |  적정성 평가를 어떻게 수행할지 정의합니다.

fieldInfo 설정은 다음과 같이 합니다.

field  |value   | 설명
--|---|--
key  |  7 |  기능을 적용할 칼럼 ID를 입력합니다.
type  |  STRING |  컬럼의 타입을 명시합니다.   
value  | occupation  |  컬럼의 이름을 입력합니다.  
attrType  | IDENTIFIER  |  컬럼의 속성을 정의합니다.
autoConfigured  |  true |  컬럼의 hierarchy를 자동으로 생성할건지, 파일로 읽어올것인지 결정합니다.
nLevels  |  5 |  자동으로 컬럼 hierarchy 생성시 최대 level을 몇으로 할건지 정의합니다.
filePath  | 입력없음  |  컬럼 내 데이터 hierarchy를 파일로 읽어올 경우, 그 파일의 위치를 명시해줍니다.

<br>

![2.05.03_01](https://i.imgur.com/Dl579D0.png)

<br>

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![2.05.03_02](https://i.imgur.com/rHKSsgg.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![2.05.03_03](https://i.imgur.com/VN6FgoH.png)


<br>

### 워크플로우 모니터링 하기

Atelier WebToolKit 상단 "Monitoring" 메뉴의 "Workflow" 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다. 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.


![2.05.03_05](https://i.imgur.com/oQnQRqJ.png)


### 결과 파일 확인하기

Atelier WebToolKit 데이터 Storage 메뉴에서도 FileWriter 에서 지정한 위치에 didentification.csv 파일이 생성된 것을 확인할 수 있습니다.

![2.05.03_06](https://i.imgur.com/H0h2039.png)

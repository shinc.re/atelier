
# 배치방식으로 데이터 처리하기 (KMeansExample)


CSV 파일을 읽어서, KMeans 클러스터링을 수행하는 배치데이터 처리 예제를 설명합니다.

![2.05.02.KMeans_intro](https://i.imgur.com/hEvBJvO.png)

## 입력 데이터 준비하기

본 예제에서는 Host PC의 /home/csle/ksb-csle/examples/input 폴더에 존재하는 input_kmeans.csv 파일을 사용자 Storage 에 등록하여 사용합니다. KSB 웹툴킷 상단의 **Storage** 메뉴를 클릭하여 데이터 저장소 관리화면으로 이동합니다. 파일을 업로드할 폴더(dataset/input)에서 <span style="color:#6698FF">File Upload</span> 버튼을 클릭하여 input_kmeans.csv 파일을 업로드 합니다. 이 때 입력파일을 설명하는 이름(Name)과 설명(Description)을 함께 입력할 수 있습니다.

![파일 등록](./images/2.5.2_FileUpload.png)

HDFS의 dataset/input 폴더에 파일이 업로드 된 것을 확인할 수 있습니다.

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | KMeansExample | 워크플로우 이름     
description  | KMeans 클러스터링을 수행하는 배치데이터 처리 예제  | 워크플로우를 설명하는 글
isBatch  | true | 배치 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름

### 엔진 선택
Storage 에 등록된 파일을 입력 받아 전처리를 수행한 후 로컬 파일에 저장하기 위해 **Batch** 엔진을 드래그앤 드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName  | 설명
--|---|---|---
1  | Batch  | ProcessingEngine | KMeans 클러스터링 수행


#### Reader
Atelier Framework 데이터 저장소에 등록된 파일을 읽기 위해 **FileBatchReader** 를 드래그 앤 드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  | dataset/input_kmeans.csv |  입력 파일 경로
fileType  |  CSV |  입력 파일 타입
delimiter  |  , |  구분자  
header  |  false |  header 포함 유무
filed  | DATA1 / DOUBLE  <br>DATA2 / DOUBLE  <br>DATA3 / INTEGER  <br>DATA4 / DOUBLE  <br>DATA5 / DOUBLE  | 파일에서 읽을 칼럼(key)과 타입(type)

- filePath 입력방법
<span style="color:#6698FF">File</span> 버튼을 클릭하여 사용자 Storage에 등록된 파일을 선택합니다.

![2.05.02_01](https://i.imgur.com/yCDFGch.png)


#### Writer
**FileBatchWriter** 를 드래그 앤 드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  | output/result_kmeans |  저장 파일 경로
fileType  |  CSV |  저장 파일 타입
delimiter  |  , |  구분자  
header  |  false |  header 포함 유무
saveMode  | OVERWRITE  |  파일 저장 방식

#### Controller
**SparkSessionController** 를 선택합니다. Spark 환경에서 배치 처리에 범용적으로 사용하는 컨트롤러입니다.


#### Runner
**SimpleSparkRunner** 를 선택한 후 디폴트 속성값을 사용합니다.

field  |value   | 설명
--|---|--
master |  local [ * ] |  Spark 모드.
executerMemory  |  1g |  Spark의 executor 메모리
totalCores  |  2 |  Spark 전체 할당 가능 코어수
cores  | 2  |  Spark 현재 할당 코어수
numExecutors  | 2 |  Spark의 executor 갯수
sparkVersion  | 2.3.0 |  Spark 버전 정보
driverMemory  |  1g |  Spark 드라이버에 할당할 메모리 정보


#### Operator
KMeans 클러스터링을 수행하기 위하여 **KMeansOperator** 를 선택한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
k_value  |  2 |  클러스터 개수
maxIterations  |  100 | 최대 반복 회수
maxRuns  | 1  |  

<br>

![2.05.02_02](https://i.imgur.com/cR3cRcK.png)

<br>

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![2.05.02_03](https://i.imgur.com/MQPHByV.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![2.05.02_04](https://i.imgur.com/3fETEZF.png)

<br>


### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인

Atelier WebToolKit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 작성한 워크플로우 및 각 엔진의 상태를 확인할 수 있습니다. 또한 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다.

![2.05.02_05](https://i.imgur.com/LJjJ8Tm.png)


## 결과 확인하기
Storage 메뉴에서 해당 폴더에 결과 파일이 생성된 것을 확인 할 수 있습니다.

![2.05.02_06](https://i.imgur.com/H2ZD3P2.png)


# OAuth 2.0 기반 외부 플랫폼의 OpenAPI 를 활용한 데이터 수집 및 배치 처리하기 (KMeansMedbizEdgeExample)



OAuth 2.0 기반 외부 플랫폼의 OpenAPI 를 활용하여 데이터를 수집하고 배치 처리하는 예제를 설명합니다. 외부 플랫폼은 medbiz 헬스케어플랫폼을 사용하겠습니다. medbiz 헬스케어플랫폼은 원주의료기기테크노밸리, 강원임베디드소프트웨어연구센터, 건강보험심사평가원, 원주세브란스기독병원이 개발한 IoT 기술 기반 질병예방과 빅데이터 기반 맞춤형 의료서비스를 제공하는 플랫폼입니다. 자세한 정보는 MEDBIZ 소개 홈페이지 (https://medbiz.or.kr/introduce) 를 참고합니다.  

![2.05.08.KMeans_Medbiz_intro](https://i.imgur.com/6h9Xwsx.png)

## Medbiz 헬스케어 플랫폼 계정 정보
실습 및 예제 수행을 위해 미리 만들어 놓은 테스트용 계정 정보는 다음과 같습니다.
- 아이디: ksbtest
- 비밀번호: ksb5302!

## 입력 데이터 준비하기
Medbiz 헬스케어플랫폼에서는 등록된 헬스케어 디바이스의 측정데이터를 수집/ 저장하고, 사용자 개인 파일을 저장하고, 공공데이터(기상, 교통, 의료 등) 를 수집/ 저장하여 분석에 활용할 수 있도록 클라우드 저장소를 제공합니다. 본 예제에서는 medbiz 클라우드 저장소에 저장되어있는 kmeans_sample.csv 파일을 BeeAI 로 읽어들여 처리합니다. 파일은 다음과 같이 저장되어있습니다.

![Uploading 2.05.08_01.png… (ulsw51bb3)]()

<br>

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | 2.05.08.KMeansMedbizEdgeExample | 워크플로우 이름     
description  | Medbiz인증정보를 활용하여 읽어온 데이터를 KMeans 클러스터링 처리하는 예제  | 워크플로우를 설명하는 글
isBatch  | true | 배치 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름

### 엔진 선택
Storage 에 등록된 파일을 입력 받아 전처리를 수행한 후 로컬 파일에 저장하기 위해 **Batch** 엔진을 드래그앤 드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName  | 설명
--|---|---|---
1  | Batch  | ProcessingEngine | KMeans 클러스터링 수행


#### Reader
medbiz 클라우드 저장소에 저장되어있는 파일을 읽기 위해 **MedbizFileBatchReader** 를 드래그 앤 드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
url  | https://openapi.medbiz.or.kr |  입력 파일 경로
fileUri  | input/kmeans_sample.csv  | medbiz 저장소의 파일 경로  
accessToken  | 98144c50-9bb3-4cfa-812a-badc6d216114  |  medbiz OpenAPI (VFS API)를 이용하기 위해 발급 받은 OAuth 2.0 기반 토큰 (토큰 발급은 Registry 탭의 Authentication 메뉴를 이용합니다)
fileType  |  CSV |  입력 파일 타입
delimiter  |  , |  구분자  
header  |  false |  header 포함 유무
filed  | DATA1 / DOUBLE  <br>DATA2 / DOUBLE  <br>DATA3 / INTEGER  <br>DATA4 / DOUBLE  <br>DATA5 / DOUBLE  | 파일에서 읽을 칼럼(key)과 타입(type)


#### Writer
medbiz 클라우드 저장소에 KMeans 클러스터링 처리한 결과 파일을 읽기 위해 **MedbizFileBatchWriter** 를 드래그 앤 드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
url  | https://openapi.medbiz.or.kr |  입력 파일 경로
fileUri  | output/kmeans_ample_result2.csv  | medbiz 저장소의 파일 경로 (medbiz 클라우드 저장소의 output 폴더에는 동일한 이름의 파일이 저장될 수 없습니다. 동일한 파일명이 있는 경우에는 결과 파일명을 변경해주세요.)
accessToken  | 98144c50-9bb3-4cfa-812a-badc6d216114  |  medbiz OpenAPI (VFS API)를 이용하기 위해 발급 받은 OAuth 2.0 기반 토큰 (토큰 발급은 Registry 탭의 Authentication 메뉴를 이용합니다)
fileType  |  CSV |  입력 파일 타입
delimiter  |  , |  구분자  
header  |  false |  header 포함 유무
filed  | -  |
saveMode  | OVERWRITE  |  


#### Controller
**SparkSessionController** 를 선택합니다. Spark 환경에서 배치 처리에 범용적으로 사용하는 컨트롤러입니다.


#### Runner
**SimpleSparkRunner** 를 선택한 후 디폴트 속성값을 사용합니다.

field  |value   | 설명
--|---|--
master |  local [ * ] |  Spark 모드.
executerMemory  |  1g |  Spark의 executor 메모리
totalCores  |  2 |  Spark 전체 할당 가능 코어수
cores  | 2  |  Spark 현재 할당 코어수
numExecutors  | 2 |  Spark의 executor 갯수
sparkVersion  | 2.3.0 |  Spark 버전 정보
driverMemory  |  1g |  Spark 드라이버에 할당할 메모리 정보


#### Operator
KMeans 클러스터링을 수행하기 위하여 **KMeansOperator** 를 선택한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
k_value  |  2 |  클러스터 개수
maxIterations  |  100 | 최대 반복 회수
maxRuns  | 1  |  

<br>

![2.05.08_02](https://i.imgur.com/GslLCHo.png)

<br>

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![2.05.08_03](https://i.imgur.com/IwGjyl4.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![2.05.08_04](https://i.imgur.com/j4xmGIp.jpg)




### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인
Atelier WebToolKit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 작성한 워크플로우 및 각 엔진의 상태를 확인할 수 있습니다. 또한 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다.

![2.05.08_05](https://i.imgur.com/DLgX73h.png)


## 결과 확인하기
medbiz 클라우드 저장소에 결과 파일 kmeans_ample_result2.csv 파일이 생성된 것을 확인 할 수 있습니다.

![2.05.08_06](https://i.imgur.com/WrkvzY1.png)


# 스트림 방식으로 MNIST 모델 서빙하기 (TfStreamPredictionMnist)

Kafka 스트림으로 입력받은 이미지 데이터를 텐서플로우 기반의 MNIST 모델을 이용하여 분류한 결과를 Kafka 스트림으로 출력하는 예제를 설명합니다.

## 모델 등록하기
웹툴킷 화면에서 [Storage] 탭을 클릭하여, MNIST 모델을 등록합니다.

### 모델 폴더 생성하기
웹툴킷 화면에서 [New Directory] 메뉴를 클릭하여, MNIST 모델을 등록할 폴더를 생성합니다. 경로는 /model/mnist 로 설정합니다.

![2.5.12_1_01](https://i.imgur.com/dl7MlHj.png)

### 모델 업로드
웹툴킷 화면에서 [File Upload] 메뉴를 클릭하여, 모델을 등록합니다.

![2.5.12_1_02](https://i.imgur.com/xXnklDk.png)

### 모델 등록 완료
웹툴킷 화면에서 모델이 등록된 것을 확인할 수 있습니다.

![2.5.12_1_04](https://i.imgur.com/SzTjljf.png)

## 워크플로우 생성하기
워크플로우 편집기를 이용하여, 다음의 절차에 따라 워크플로우를 생성합니다.
- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | TfStreamPredictionMnist | 워크플로우 이름     
description  | 텐서플로우 기반 딥러닝 모델을 스트림 서빙하는 예제  | 워크플로우를 설명하는 글
isBatch  | false | 스트림 처리 하는 워크플로우 이므로, true 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |   
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름


### 엔진 선택
본 예제는 스트림 타입의 모델 서빙 서비스를 제공하므로, **MiniBatch** 엔진을 선택합니다.

#### Reader
Kafka 스트림을 입력받기 위해 **KafkaReader** 를 선택하고, 아래 표와 같이 속성을 설정합니다.

field | value | 설명
--|---|--
topic | mnist_input | 이미지 데이터를 입력받기 위한 Kafka Topic 이름
zooKeeperConnect | csle1:2181 | Zookeeper 접속주소 (IP, 포트번호)
groupId | mnist_input | Kafka 통신을 위한 그룹 아이디
bootStrapServer | csle1:9092 | Kafka 서버 접속주소 (IP, 포트번호)

#### Writer
이미지 분류 결과를 Kafka 스트림으로 출력하기 위해 **KafkaWriter** 를 선택하고, 아래 표와 같이 속성을 설정합니다.

field | value | 설명
--|---|--
topic | mnist_output | 이미지 분류 결과를 출력하기 위한 Kafka Topic 이름
zooKeeperConnect | csle1:2181 | Zookeeper 접속주소 (IP, 포트번호)
groupId | mnist_output | Kafka 통신을 위한 그룹 아이디
bootStrapServer | csle1:9092 | Kafka 서버 접속주소 (IP, 포트번호)

#### Controller
Spark 기반의 스트림 데이터 처리를 위해 **SparkStreamController** 를 선택하고, 아래 표와 같이 속성을 설정합니다.

field | value | 설명
--|---|--
operationPeriod | 1 | 스트림 데이터를 처리하는 주기 (초 단위)

#### Runner
Spark 환경 설정을 위해 **SimpleSparkRunner** 를 선택하고, 아래 표와 같이 속성을 설정합니다.

field | value | 설명
--|---|--
inJason | false | false로 설정하는 경우 json 형태의 파라메타를 커맨드라인 파라미터 형태로 변환하여 호출되는 외부 시스템에 전달. True 이면 json 형태의 파라메타 형태 그대로 외부시스템에 전달
sparkArgs | 아래 표 참고 | Spark Application 설정

sparkArgs 설정은 다음과 같이 합니다.

field | value | 설명
--|---|--
master | local[\*] | 마스터
numExecutors | 4 | Spark Executor 개수  
excuterCores | 2 | 각 Spark Executor의 core 개수  
executerMemory | 1g | 각 Spark Executor의 메모리 크기
driverMemory | 1g | Spark Driver 메모리

#### Operator
본 예제의 경우, 데이터 전처리가 필요 없기에 아래 1개의 Operator를 이용합니다.

1. **TensorflowPredictOperator**
텐서플로우 기반의 MNIST 모델을 이용하여, 입력 이미지의 분류 결과를 출력합니다.

field | value | 설명
--|---|--
modelServerUri | model/mnist | MNIST 모델 경로
modelName | mnist | 모델 이름
signatureName | predict_images | 이미지 분류 signature 이름

완성된 워크플로우는 다음과 같습니다.

![2.5.12_1_05](https://i.imgur.com/nbis68c.png)


## 워크플로우 실행 및 모니터링하기



### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.


### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.





### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인
웹툴킷 [Monitoring] 화면에서 [Workflow] 탭을 선택하여, 위에서 실행한 워크플로우와 엔진의 동작 상태를 확인할 수 있습니다.
정상적으로 실행되어 Status 값이 Inprogress인 것을 확인할 수 있습니다.

![2.5.12_3_02](https://i.imgur.com/MpRhLdD.png)

#### DashBoard 에서 모델 서빙상태 확인

![2.5.12.DashBoard](https://i.imgur.com/aT08mpK.png)

#### 워크플로우 로그 보기
[Workflow History] 탭을 선택하여, 현재 동작 중인 워크플로우의 로그 정보를 확인할 수 있습니다.

![2.5.12_3_03](https://i.imgur.com/BjtQPsa.png)

## 결과 확인하기
Atelier Toolbox에 포함된 테스트 스크립트를 이용하여, 스트림 방식의 모델 서빙 기능을 확인하는 과정입니다.

#### (1) 이미지 분류 결과 수신 대기
Atelier Toolbox에 포함된 ```consume-mnist-output.sh``` 스크립트를 실행하여, MNIST 이미지 분류 결과를 Kafka 서버로부터 수신할 수 있도록 대기합니다.
호스트 PC에서 터미널 창을 열어, 아래의 명령을 수행합니다.
```bash
$ docker exec -it csle1 bash
$ cd ~/ksb-csle/examples/models/mnist/client/kafka-json
$ ./consume-mnist-output.sh
```


#### (2) 분류할 이미지 데이터 전송
Atelier Toolbox에 포함된 ```publish-mnist-input.sh``` 스크립트를 실행하여, MNIST 이미지 데이터를 Kafka 서버로 전송합니다.
호스트 PC에서 터미널 창을 열어, 아래의 명령을 수행합니다.
```bash
$ docker exec -it csle1 bash
$ cd ~/ksb-csle/examples/models/mnist/client/kafka-json
$ ./publish-mnist-input.sh
```
![2.5.12_4_02](https://i.imgur.com/XR93i8s.png)

#### (3) 이미지 분류 결과 수신 확인
위 (1)번 과정에서 실행한 터미널 창에서, MNIST 이미지 데이터와 분류 결과를 확인할 수 있습니다.

![2.5.12_4_03](https://i.imgur.com/Gzop52v.png)

## 워크플로우 종료하기
웹툴킷 [Monitoring] 화면의 [Workflow] 탭에서, 현재 Status가 Inprogress인 '2.5.12.TfStreamPredictionMnist' 워크플로우의 정지(<span style="color:red">&#9724;</span>) 버튼을 클릭하여 종료시킵니다.

## 워크플로우 저장하기
워크플로우 편집기에서 작성한 워크플로우를 "Save Workflow" 메뉴를 이용하여 저장합니다.

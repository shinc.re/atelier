
# 온디맨드 방식으로 Inception 모델 서빙하기 (InceptionServingExample)

텐서플로우 기반 Inception 모델을 이용한 이미지 분류 RESTful 서비스 예제를 소개합니다.

## 모델 등록하기
웹툴킷 화면에서 [Storage] 탭을 클릭하여, Inception 모델을 등록합니다.
Inception 모델은 /home/csle/ksb-csle/examples/models/inception/ 에 위치해 있습니다.

웹툴킷 화면에서 모델이 model/model/inception/ 경로에 등록된 것을 확인할 수 있습니다.

![2.5.11_1_01](https://i.imgur.com/jbVJXYl.png)

## 워크플로우 생성하기
워크플로우 편집기를 이용하여, 다음의 절차에 따라 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | InceptionServingExample | 워크플로우 이름     
description  | 텐서플로우 기반 딥러닝 모델을 온디맨드 서빙하는 예제  | 워크플로우를 설명하는 글
isBatch  | true | 배치 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |   
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름


### 엔진 선택
본 예제는 온 디맨드 타입의 분류 서비스를 제공하므로, **OnDemandServing** 엔진을 선택합니다.

#### Reader
본 예제에서는 Reader를 사용하지 않습니다.

#### Writer
본 예제에서는 Writer를 사용하지 않습니다.

#### Controller
본 예제의 Controller는 **TensorflowServingController** 를 사용하고, 속성을 기본 값으로 설정합니다.

#### Runner
본 예제의 Runner는 **TensorflowServingRunner** 를 사용하고, 속성을 다음과 같이 설정합니다.

field  |value   | 설명
--|---|--
modelBasePath  |  model/model/inception/ |  모델 경로
port  |  8002 |  이미지 분류 결과를 서비스할 포트 번호
modelName  |   inception |  서비스에 사용할 inception 모델의 이름
options  |  아래 ***GPU디바이스 지정 및 메모리 사용량 세부 설정*** 표 참고 |  TensorFlow Serving 에 이용할 GPU 디바이스 세부 설정
'modelBasePath' 설정은 위에서 등록한 Inception 모델을 선택합니다.
![2.5.11_2_01](https://i.imgur.com/Z7UNc4B.png)

##### 서빙용 GPU 디바이스 지정 및 메모리 사용량 세부 설정

key  | value   | 설명
--|---|--
config.gpu_options.visible_device_list  |  1 |  지정해서 사용할 GPU 디바이스의 아이디
config.gpu_options.per_process_gpu_memory_fraction  | 0.1  | GPU 디바이스 메모리 사용량   
model.auto.load | true | 새로운 모델이 생성되면, **TensorflowServingRuuner** 가 자동으로 로딩해서 서빙을 수행할지에 대한 설정
웹툴킷에서 설정한 화면은 다음과 같습니다.

![2.5.11_02](https://i.imgur.com/f4R8gr5.png)



#### Operator
본 예제의 Operator는 **TensorflowServingOperator** 를 사용하고, 속성을 기본 값으로 설정합니다.

완성된 워크플로우는 다음과 같습니다.

![2.5.11_03](https://i.imgur.com/xmufdCy.png)


## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.


### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.






### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인
웹툴킷 [Monitoring] 화면에서 [Workflow] 탭을 선택하여, 위에서 실행한 워크플로우와 엔진의 동작 상태를 확인할 수 있습니다.
정상적으로 실행되어 Status 값이 Running 인 것을 확인할 수 있습니다.

![2.5.11_04](https://i.imgur.com/lPjzKkF.png)

#### 워크플로우 로그 보기
[Workflow History] 탭을 선택하여, 현재 동작 중인 워크플로우의 로그 정보를 확인할 수 있습니다.

![2.5.11_05](https://i.imgur.com/BayF4hZ.png)

#### 서빙 서비스 상태 보기
웹툴킷 [Dashboard] 탭을 선택하면, 화면 하단의 'Services' 테이블에서 위에서 실행한 온 디멘드 서빙 서비스에 대한 정보를 확인할 수 있습니다.

![2.5.11_06](https://i.imgur.com/VtLzdCe.png)

#### 모델 서빙을 위한 GPU 디바이스 사용 현황 보기
서빙에 사용중인 GPU 사용 현황을 확인한다. 서버에 장착된 GPU 디바이스 목록을 확인하는 명령은 다음과 같다. 아래 그림에서는 두 개의 GPU가 하나의 서버에 장착된 것을 확인할 수 있다.
```
nvidia-smi
```
![2.5.11_07](https://i.imgur.com/7hKngzO.png)

1번 GPU 의 사용량 현황을 확인하는 명령은 다음과 같다. GPU 사용량을 지속적으로 모니터링 하고 싶으면, watch 명령을 붙이도록 한다.
```
watch nvidia-smi -i 1
```



![2.5.11_08](https://i.imgur.com/XN8LuUV.png)

참고로, GPU 메모리 사용량 지정 옵션(config.gpu_options.per_process_gpu_memory_fraction)을 별도로 설정하지 않으면, 아래와 같이 GPU 1에서 가용한 모든 메모리를 사용하며 서빙하는 것을 확인할 수 있다.

![2.5.11_09](https://i.imgur.com/qZIep2r.png)


## 결과 확인하기
Postman 프로그램을 이용하여, 본 예제의 이미지 분류 서비스의 기능을 확인하는 과정입니다.

#### Inception 모델의 메타데이터 조회
- 웹툴킷 [Dashboard] 화면 'Services' 테이블의 'URL' 정보를 이용하여 Inceptin 모델의 메타데이터를 조회할 수 있습니다.
- 좌측 상단의 콤보박스에서 'GET'을 선택하고, Request-URL 값을 ``http://0.0.0.0:8002/model``로 설정합니다.
- [Send] 버튼을 클릭하여 메타데이터 조회를 요청을 하면, 다음과 같이 모델의 메타데이터를 확인할 수 있습니다.

![2.5.11_10](https://i.imgur.com/wyfKp8h.png)

#### 이미지 분류 질의 결과 보기
- 위에서 조회한 메타데이터를 이용하여 이미지 분류 질의 요청과 그에 대한 결과를 확인할 수 있습니다.
- 좌측 상단의 콤보박스에서 'POST'를 선택하고, Request-URL 값을 ``http://0.0.0.0:8002/model/predict_images/images`` 로 설정합니다.
  - predict_images: 이미지 분류 signature 이름
  - images: 입력 tensor의 이름
- [Headers] 메뉴에서 KEY 필드에서 'Content-Type'"'을 VALUE 필드에서 'image/jpeg'을 입력합니다.

![2.5.11_11](https://i.imgur.com/OM3wYsr.png)

- [Body] 메뉴에서 [binary] 버튼을 선택하고, [파일 선택] 버튼을 클릭하여 분류하고자 하는 이미지 파일을 선택합니다.

![2.5.11_12](https://i.imgur.com/UxQtINn.png)

- [Send] 버튼을 클릭하여 이미지 분류 질의 요청을 하면, 다음과 같이 이미지 분류 결과를 확인할 수 있습니다.
(Inception 모델의 경우, score가 가장 높은 순서로 5개의 분류 카테고리를 알려줍니다.)

![2.5.11_13](https://i.imgur.com/StfyfWt.png)

## 워크플로우 종료하기
웹툴킷 [Monitoring] 화면의 [Workflow] 탭에서, 현재 Status가 Inprogress 인 InceptionServingExample 워크플로우의 정지(<span style="color:red">&#9724;</span>) 버튼을 클릭하여 종료시킵니다.

## 워크플로우 저장하기
워크플로우 편집기에서 작성한 워크플로우를 "Save Workflow" 메뉴를 이용하여 저장합니다.


# 외부 플랫폼(EdgeX)의 데이터를 BeeAI에 적재하기

EdgeX 플랫폼으로부터 스트림으로 입력되는 데이터를 파일로 저장하는 예제를 설명합니다.

![2.05.15.edgeXtoFile_intro](https://i.imgur.com/wjBqFqY.png)



## 입력 데이터 준비하기
본 예제에서는 외부의 EdgeX 플랫폼으로부터 데이터를 수신합니다. 하나 이상의 센서가 데이터를 발생하고 있는 EdgeX 플랫폼을 가정하며, EdgeX 플랫폼의 구동 방법은 본 문서에서 설명하지 않습니다.


## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | EdgeXToFileExample | 워크플로우 이름     
description  | EdgeX로부터 데이터 적재하기  | 워크플로우를 설명하는 글
isBatch  | false | 스트림 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
runtype  | 즉시실행 |
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름

### 엔진 선택
본 예제에서는 스트림 데이터를 입력받아 처리한 뒤 MongoDB에 적재하므로 **MiniBatch** 엔진을 드래그앤 드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName
--|---|---
1  | MiniBatch  | ProcessingEngine  |

#### Reader
EdgeX 플랫폼으로부터 전달되어 오는 데이터를 입력 받기 위해 **EdgeXMqttReader** 를 드래그 앤 드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
name  |  BeeaiClient |  EdgeX 플랫폼에 등록될 이름을 입력합니다.
port  |  http://129.254.195.35 |  EdgeX 플랫폼의 주소를 입력합니다.
port  |  48071 |  EdgeX 플랫폼의 Export service 포트번호를 입력합니다.
deviceIdentifiers  |   | 수집할 데이터가 발생하고 있는 센서의 아이디 (예: sensorID_1)
valueDescriptorIdentifiers  |  | 수집되는 데이터 중에서 필터링할 컬럼의 이름  (예: temperature)
brokerAddress  |  129.254.173.228 |  데이터를 중계해 줄 MQTT 브로커의 주소를 입력합니다.
brokerPort  |  1883 |  데이터를 중계해 줄 MQTT 브로커의 포트번호를 입력합니다.
topic  |  BeeaiTopic |  MQTT 토픽을 입력합니다.


#### Writer
**FileMiniBatchWriter** 를 드래그앤 드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  | output/edgex.csv |  저장 파일 경로
fileType  |  CSV |  저장 파일 타입
delimiter  |  , |  구분자  
header  |  false |  header 포함 유무
saveMode  | APPEND  |  파일 저장 방식


#### Controller
**SparkStreamController** 를 드래그앤 드롭한 후 아래 표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
operationPeriod  |  3 |  스트리밍 처리 주기 (초 단위)


#### Runner
**SimpleSparkRunner** 를 드래그앤 드롭합니다. 디폴트 속성값을 사용합니다.


#### Operator
**EdgeXFormatOperator** 를 드래그앤 드롭합니다. 디폴트 속성값을 사용합니다.

EdgeXFormatOperator는 EdgeX 플랫폼에서 전송하는 데이터를 2차원 데이터프레임으로 변환합니다.


<br>

![2.05.15_01](https://i.imgur.com/nAV7AT1.png)

<br>

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![2.05.15_02](https://i.imgur.com/8fG7JWm.png)

<br>

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![2.05.15_03](https://i.imgur.com/wBz3SiO.png)

<br>

### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인
Atelier WebToolKit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다.

![2.05.15_04](https://i.imgur.com/elGZWXG.png)


## 데이터 적재 결과 확인하기
Storage 메뉴에서 해당 폴더에 결과 파일이 생성된 것을 확인 할 수 있습니다.

![2.05.15_05](https://i.imgur.com/e4CzVMS.png)


### 결과 파일 (subscription 미적용)
EdgeXMqttReader 의 subscription 속성에 값을 세팅하지 않으면 EdgeX 플랫폼에서 발생하는 데이터의 모든 컬럼의 값을 결과 아래와 같이 파일에 저장합니다.

![2.05.15_06](https://i.imgur.com/QPZU7Cb.png)


### 결과 파일 (subscription 적용)
EdgeXMqttReader 의 subscription 속성에 값을 세팅하면 EdgeX 플랫폼에서 발생하는 데이터의 컬럼들 중 명시한 속성값의 데이터를 결과 아래와 같이 파일에 저장합니다. 예를들어 "temperature" 라고 명시하면, 아래와 같이 데이터를 수신하여 파일로 저장합니다.

![2.05.15_07](https://i.imgur.com/kzl8xQS.png)

## 워크플로우 종료하기
본 예제는 지속적으로 스트림데이터가 입력되므로 사용자가 워크플로우를 종료해야 합니다.  
Atelier WebToolKit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)합니다.


## 워크플로우 저장하기
워크플로우 편집 화면에서 작성한 워크플로우를 "Save Workflow" 메뉴를 이용하여 저장합니다.

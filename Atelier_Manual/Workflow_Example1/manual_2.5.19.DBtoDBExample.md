
# 관계형데이터베이스 테이블의 데이터를 수집 및 적재하기 (DBToDBExample)

PostgreSQL 데이터베이스 테이블 저장되어있는 데이터를 읽어서, PostgreSQL 의 테이블에 배치형태로 저장하는 예제를 설명합니다.

![2.05.19.dbTodb_intro](https://i.imgur.com/rZx5LwP.png)

## PostgreSQL 데이터베이스 접속 및 테이블 내용 확인

본 예제에서는 KSB 툴박스 내부의 PostgreSQL DB의 beeaidb에 존재하는 "KsbManifest" 테이블을 동일한 데이터베이스의 다른 테이블을 새로 생성해서 저장하는 실습을 합니다.
테이블의 내용을 확인하는 명령어는 다음과 같습니다.

```
psql -h csle1 -U csle -d beeaidb
비밀번호 csle1234
csledb=# \d
select * from "KsbManifest";
\q
```

![2.05.19_01](https://i.imgur.com/RyL4qsR.png)


## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | 2.05.19.TableRW[Postgresql] | 워크플로우 이름     
description  | TableBatchReader와TableBatchWriter를이용해서Postgresql의 테이블을 읽고 쓰는 예제  | 워크플로우를 설명하는 글
isBatch  | true | 배치 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | true | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름

### 엔진 선택
PostgreSQL 데이터베이스에 저장되어있는 테이블 내용을 읽고, 해당 내용을 다른 테이블을 만들어 저장하기 위해  **Batch** 엔진을 드래그앤 드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName  | 설명
--|---|---|---
1  | Batch  | TableReadWriteEngine | PostgreSQL 의 테이블을 읽고 쓰는 엔진


#### Reader
PostgreSQL 에 저장되어있는 테이블의 내용을 읽기 위해 **TableBatchReader** 를 드래그 앤 드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
jdbcUrl  | jdbc:postgresql://csle1/beeaidb | jdbc 주소와 데이터베이스 이름 (mariaDB 의 경우에는 jdbc:mysql://csle1:3306/beeaidb 를 입력 )
jdbcDriver  |  org.postgresql.Driver |  PostgreSQL 용 jdbc드라이버 패키지 이름 (mariaDB 의 경우는 org.mariadb.jdbc.Driver 를 입력 )
tableName  |  "KsbManifest" |  테이블 이름 (mariaDB 의 경우에는 테이블 이름에 쌍따옴표 빼기)  
userName  | csle |  데이터베이스의 사용자 이름
password  | csle1234  | 데이터베이스의 비밀번호
writeMode  | - |   기존에 테이블이 만들어져 있으면, 새로 생성하지 않고 내용을 이어서 쓸지, 해당테이블을 삭제하고 새로 테이블을 만들어서 내용을 쓸지 옵션
primaryKey  | "ksbVersion"  | 테이블의 스키마에 설정해 놓은 기본키



#### Writer
PostgreSQL 에 저장되어있는 테이블의 내용을 읽기 위해 **TableBatchWriter** 를 드래그 앤 드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
jdbcUrl  | jdbc:postgresql://csle1/beeaidb | jdbc 주소와 데이터베이스 이름
jdbcDriver  |  org.postgresql.Driver |  PostgreSQL 용 jdbc드라이버 패키지 이름
tableName  |  "resultTable" |  테이블 이름   
userName  | csle |  데이터베이스의 사용자 이름
password  | csle1234  | 데이터베이스의 비밀번호
writeMode  | OVERWRITE |  기존에 테이블이 만들어져 있으면, 지우고 새로 생성한 테이블에 내용을 쓰기
primaryKey  | "ksbVersion"  | 테이블의 스키마에 설정해 놓은 기본키





#### Controller
**SparkSessionController** 를 선택합니다. Spark 환경에서 배치 처리에 범용적으로 사용하는 컨트롤러입니다.


#### Runner
**SimpleSparkRunner** 를 선택한 후 디폴트 속성값을 사용합니다.

field  |value   | 설명
--|---|--
master |  local [ * ] |  Spark 모드.
executerMemory  |  1g |  Spark의 executor 메모리
totalCores  |  2 |  Spark 전체 할당 가능 코어수
cores  | 2  |  Spark 현재 할당 코어수
numExecutors  | 2 |  Spark의 executor 갯수
sparkVersion  | 2.3.0 |  Spark 버전 정보
driverMemory  |  1g |  Spark 드라이버에 할당할 메모리 정보


#### Operator
이 예제에서는 operator 컴퍼넌트를 사용하지 않습니다.

<br>

![2.05.19_02](https://i.imgur.com/BBhhPxR.png)

<br>

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![2.05.19_03](https://i.imgur.com/YliTBzT.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![2.05.19_04](https://i.imgur.com/PVwHLIj.png)




### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인
Atelier WebToolKit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 작성한 워크플로우 및 각 엔진의 상태를 확인할 수 있습니다. 또한 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다.

![2.05.19_05](https://i.imgur.com/gReJBYR.png)


## 결과 확인하기
PostgreSQL 에 접속해서, 신규테이블 "resultTable" 이 생성되었고, "KsbManifest" 테이블의 내용이 동일하게 작성되어 저장된 것을 확인 할 수 있습니다.

![2.05.19_06](https://i.imgur.com/XY4uylS.png)

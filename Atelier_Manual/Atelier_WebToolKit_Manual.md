
# Atelier WebToolKit 개요

**Atelier WebToolKit** 은 사용자가 프레임워크를 효과적으로 사용할 수 있도록 지원하는 웹기반의 저작 도구입니다.  
**Atelier WebToolKit** 은 워크플로우를 정의하고 실행하는 기능과 실행 과정을 모니터링하는 기능, 그리고 관련 데이터를 업로드하고 결과를 시각화하는 기능 등을 지원합니다.  
**Atelier WebToolKit** 은 회원가입을 통해 로그인을 하여 사용하며 사용자별로 워크플로우를 관리합니다.

<br>

## Web을 통한 접속 및 로그인
먼저 Chrome 브라우저(또는 HTML5 표준 호환 브라우저)를 열고 Atelier WebToolKit에 접속합니다.  
로그인 페이지를 볼 수 있으며, 다음과 같이 기본적으로 제공하는 사용자 정보(수퍼관리자)가 저장되어 있습니다. 암호는 환경에 따라 다를 수 있으므로, 실제 환경을 생성한 관리자에게 문의 바랍니다.

사용자 ID 와 암호를 입력한 후 로그인 합니다. 위 아이디로 로그인할 경우 워크플로우 예제가 저장되어 있습니다.

![login](https://i.imgur.com/TN7c6Mm.png)

<br>

## 기능 및 메뉴 구성
Atelier WebToolKit에 로그인을 하면 Main Page로 Dashboard 화면이 보이고, 화면 상단에 다음과 같이 7개의 메뉴가 있습니다.

![mainpage](https://i.imgur.com/532e35v.png)

- 일반 사용자 기능
  - Dashboard
  - Workbench
  - Registry
  - Storage
  - Monitoring

<br>

- 관리자 기능
  - Component
  - Management

<br>

### Dashboard

대시보드에서는 Atelier WebToolKit에 로그인 한 사용자가 작성하여 저장하거나 실행한 워크플로우 정보 및 현재 서비스 되고 있는 정보를 한 눈에 파악할 수 있습니다.  
(다른 사용자가 작성하여 저장하거나 실행한 워크플로우는 나타나지 않습니다.)  

*Workflow TOP 10* 에서는 수행된 워크플로우의 통계 정보(일별 배포된 워크플로우 개수, 일별 실행중인 워크플로우 개수, 배포된 엔진들의 상태)를 보여줍니다.  
*Workflow* 에서는 사용자가 작성하여 저장한 워크플로우의 리스트를 보여줍니다.  
삭제(<span style="color:red">&#9747;</span>) 버튼을 눌러 해당 워크플로우를 삭제할 수 있습니다.  

또한, 특정 기간에 작성된 워크플로우를 검색하여 볼 수 있습니다.  
*Services* 에서는 현재 서비스 되고 있는 서빙 엔진들(온디맨드방식 처리 엔진, 스트림방식 처리 엔진)의 세부 정보를 보여줍니다.

![Dashboard](https://i.imgur.com/ILtk8mq.png)

<br>

### Workbench
Workbench 는 편집화면과 운영화면으로 구성됩니다.  
편집화면에서는 그래픽 기반의 워크플로우 편집 기능을 사용하여 워크플로우를 작성할 수 있습니다.  
또한 워크플로우를 저장하고 로딩할 수 있으며 별도의 Json 파일로 Import/Export 할 수 있습니다.  
편집화면에서 제작한 인공지능 워크플로우는 운영화면에서 빌드하고 배포할 수 있습니다.

![Workbench](https://i.imgur.com/dygqJLE.png)

<br>

### Registry
Registry 는 Atelier를 이용한 인공지능 서비스 제작에 필요한 다양한 종류의 자원들을 등록하고 관리하는 기능을 제공합니다.  
등록할 수 있는 자원의 유형은 **API, Container Image, Dataset, ML Model, DL Model, Platform, PyModule, Training Code, Authentication, Datasource, Subscripion** 입니다.  
자원을 등록할 때에는 자원 유형을 선택한 후, 우측 상단의 +Register 버튼을 누르고, 해당 자원의 주요 정보를 입력하고 저장해야합니다.

![Workbench](https://i.imgur.com/4Rzd9WW.png)

<br>

### Storage
Atelier는 **하둡파일시스템(HDFS)** 을 기본 저장소로 사용합니다.  
HDFS 저장소에서는 사용자의 로컬파일시스템과 Swift 저장소에 저장되어있는 파일이나 폴더를 HDFS에 업로드 하거나 다운로드 하기 위한 인터페이스를 제공합니다.  

워크플로우에서 사용하는 데이터 파일, 모델 파일, 워크플로우 실행 결과 파일, 코드 등을 관리할 수 있습니다. 사용자가 원하는 파일 내용을 확인하거나 신규 디렉토리를 생성하고 파일을 업로드할 수 있는 기능 등을 제공합니다.

![Storage](https://i.imgur.com/Zlvii7m.png)

### Monitoring
 진행내역/상태 모니터링에서는 (1) Atelier에 제출된 워크플로우(워크플로우에 속한 엔진 포함)의 목록 및 상태를 확인할 수 있습니다. 또한 워크플로우 및 엔진의 Status에 따라 실행 중인 워크플로우 및 엔진을 종료(<span style="color:red">&#9724;</span>)하거나, 재실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다. 또한 *Workflow Histroy* 탭에서 엔진이 실행 되어 생성된 결과파일을 확인하고 그래프로 시각화 하거나 로그 기록을 볼 수 있습니다.

![Monitoring](https://i.imgur.com/r0Mq8Rr.png)

<br>

### Component (관리자 메뉴)
컴퍼넌트 관리는 관리자 권한을 가진 사용자만 접근할 수 있습니다. 컴퍼넌트 관리에서는 SW 개발자가 만든 신규 컴퍼넌트를 **Atelier WebToolKit** 에서 사용할 수 있도록 등록하거나, 기존의 컴퍼넌트 동작이 변경되어었을 경우 수정하는 기능을 제공합니다.  

또한 컴퍼넌트들의 조합으로 특정 용도의 엔진을 만들어서 엔진 템플릿으로 등록 할 수 있으며, WebToolKit 컴퍼넌트들의 버전을 관리할 수 있습니다.

![Component](https://i.imgur.com/UTzPcUx.png)

<br>

### Management (관리자 메뉴)
Management 탭은 관리자 권한을 가진 사용자만 접근할 수 있습니다.  
*System Configuration* 에서는 WebToolKit 관리자가 쉽게 웹툴킷 정보(게이트웨이 Ip, Port, 버전 정보 등)를 확인하고 수정할 수 있습니다. *User Configuration* 에서는 회원 목록을 확인하고 회원 가입 승인, 관리자 권한 부여, 회원 삭제 등을 수행할 수 있습니다.  

![Management](https://i.imgur.com/vlCb2H4.png)

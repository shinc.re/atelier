
# 다중 분류 작업 학습 모델 생성하기

본 예제에서는 iris 데이터 셋을 활용하여 다중 분류 작업 모델을 생성하는 방법을 다룹니다.

## 입력 데이터 준비하기

### 데이터 로딩하기

실행을 위해서 Host PC의 /home/csle/ksb-csle/examples/input 폴더에 존재하는 `iris.csv` 파일을 사용자 Storage에 웹툴킷을 이용하여 dataset/input 폴더에 등록합니다.

### 데이터 셋 설명하기

Iris (붓꽃)데이터 셋은 붓꽃의 꽃 모양에 관한 여러가지 정보가 포함되어 있습니다.

속성 | 설명
--|---
sepal_length | 꽃 받침 길이 정보
sepal_width | 꽃 받침 너비 정보
petal_length | 꽃잎의 길이 정보
petal_width | 꽃잎의 너비 정보  
species | 꽃 종류 정보로 setosa, versicolor, virginica로 구분됨  

본 예제에서는 위의 4가지 꽃의 형태 정보를 활용하여 꽃의 종류 (species)를 분류하는 다중 분류 학습 모델을 생성하는 방법에 대해 설명합니다.

## 워크플로우 생성하기

워크플로우 편집화면을 이용하여 다중 분류 학습 모델을 생성하는 워크플로우를 아래와 같이 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | 다중 분류 학습 모델 예제 | 워크플로우 이름     
description  | Multi-Layer Perceptron 모델을 이용한 분류 작업 모델 생성 예제입니다.  | 워크플로우를 설명
isBatch | true | 배치 처리를 하는 워크플로우로 true로 지정
verbose | true | 디버깅을 위해 로그정보를 보고자할 경우 true로 지정
RunType | 즉시실행 | 실행 타입
Project | Spark ML Tutorials | 워크플로우의 프로젝트 이름

### 엔진 선택

모델 생성을 위한 학습 데이터는 파일 기반 배치 형태로 읽어들일 수도 있고, kafka 등의 스트림 형태로도 읽어들일 수도 있습니다. 따라서 BatchEngine 또는 MiniBatchEngine 둘 다 사용가능하며, 본 예제에서는 Batch 엔진을 사용하여 학습 모델을 생성하도록 하겠습니다.

- 엔진 속성

순번  | 엔진 Type | NickName  
--|---|---
1  | Batch  | MulticlassClassificationEngine  

엔진에는 어떤 데이터를 읽어와 (reader) 어떤 방법으로 처리하여 (operator) 그 결과를 어디에 저장할 것인지 (writer), 그리고 위의 전체적인 작업을 어떤 식으로 제어하며 (controller) 어떻게 구동할 것인지 (runner) 등의 정보를 입력해야 합니다.

#### Reader

Atelier **Stroage** 에 붓똧 모양의 형태 정보가 담겨있는 `iris.csv` 파일을 읽어들입니다. FileBatchReader를 드래그앤드랍한 후 아래와 같이 속성을 지정해줍니다.

field  |value   | 설명
--|---|--
filePath  | dataset/input/iris.csv |  입력 파일 경로를 입력합니다.
fileType  |  CSV |  읽을 파일의 타입을 입력합니다.
delimiter  |  , |  컬럼간 구분자를 지정합니다.  
header  |  true |  파일의 header 포함 유무를 입력합니다.
field  | 입력없음  | 컬럼의 정보를 입력합니다 (데이터 내 헤더 정보가 포함되어 있기에 여기선 입력을 안합니다).  
saveMode  | OVERWRITE |  Writer에서 사용되는 옵션으로 Reader에서는 사용되지 않습니다.

#### Writer

워크플로우 내에 있는 SparkML 학습 오퍼레이터에 의해 학습모델이 생성되게 되며, 그 모델에 의해 추론된 결과값이 원래의 데이터 옆에 새로운 컬럼으로 append되게 됩니다. 추론 결과값이 포함된 데이터를 어디에 파일에 저장할 것이기에, FileBatchWriter를 선택하고 아래와 같이 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  | output/ml/multiClassification.csv |  결과 파일의 저장경로를 입력합니다.
fileType  |  CSV |  저장파일의 타입을 입력합니다.
delimiter  |  , |  컬럼간 구분자를 지정합니다.  
header  |  true |  header 정보를 포함해서 저장할건지 그 유무를 입력합니다.
saveMode  | OVERWRITE  |  파일 저장 방식을 선택합니다.

#### Controller

Spark기반 데이터 처리 기법 및 Spark ML을 활용하여 모델을 생성합니다. Spark 환경에서 사용되는 SparkSessionController를 선택합니다.

#### Runner

SimpleSparkRunner를 선택합니다.

field  |value   | 설명
--|---|--
master |  local [ * ] |  Spark local 모드를 입력합니다.
numExecutors  | 2 |  Spark의 executor 갯수를 입력합니다.
executorCores  | 2  |  Spark의 현재 할당된 코어수를 입력합니다.
executerMemory  |  1g |  Spark의 executor 메모리 정보를 입력합니다.
driverMemory  |  1g |  Spark 드라이버에 할당할 메모리 정보를 입력합니다.
totalCores  |  2 |  Spark의전체 할당 가능한 코어수를 입력합니다.
sparkVersion  | 2.4.2 |  Spark 버전 정보를 입력합니다.

#### Operator

Atelier 에서 제공하는 Spark ML에 대한 기본 동작을 이해하기 위해서는 여기를 참조하시기 바랍니다. 여기서는 예제 실행을 위한 설정 절차만 설명합니다.

Atelier 에서 제공하는 기계학습 Operator들은 공통적으로 아래의 사항들을 입력받아야 합니다.
- Label 데이터로 사용하고자 하는 컬럼명, Feature 데이터로 사용하고자 하는 컬럼명 정보
- 모델이 저장될 Base path 정보
- k-fold 정보 및 train-test split ratio 정보
- 모델 성능을 측정하기 위한 메트릭 정보
또한 공통 설정 사항과 더불어 사용하고자 하는 학습 모델의 Tuning 파라미터 값 정보도 입력받아야 합니다.

Atelier WebToolKit에서 RandomForestRegressor Operator를 드래그 앤 드롭한 후 아래의 순서로 설정합니다.

![1.0.2_01](https://i.imgur.com/dW73yZm.png)

1. 오른쪽 randomForestTrainer 세부 설정 화면에서 trainInfo 버튼을 클릭합니다.
2. 학습을 수행하기 위한 기본적인 정보를 아래와 같이 입력합니다.

field  |value   | 설명
--|---|--
labelColumnName |  species |  붓꽃 종류 'species'를 분류하는 학습 모델입니다.
featureColumnNames  | * |  'species'를 제외한 모든 컬럼을 Feature로 사용합니다.
\_path  | /user/biuser/model/iris/mlp  |  개인 계정의 model/iris/mlp 밑에 학습된 모델을 저장합니다.
numFolds  |  5 |  5-fold cross-validation을 수행합니다.
testSplitRatio  |  0.3 |  test 데이터 셋은 전체 데이터 셋의 30%만 사용합니다.
metric  |  accuracy |  모델의 성능은 accuracy 메트릭을 사용합니다..
mlType  | MULTICLASS_CLLASSIFICATION |  setosa, versicolor, virginica인지 분류하는 다중 분류 작업 모델입니다.

3. Multi-Layer Perceptron (MLP) 세부 파라미터를 튜닝합니다. MLP에서 사용할 Layer 정보를 입력해야 합니다. layers 버튼을 클릭한 후 그림과 동일하게 입력합니다.

field  |value   | 설명
--|---|--
numFeatures |  4 |  4개의 컬럼(sepal_length, sepal_length, ...)을 Feature로 사용하기에 4를 입력합니다. 4가 아닐 경우 에러가 발생합니다.
numClasses  |  3 |  분류하고자 하는 꽃 종류(species)는 setosa, versicolor, virginica 이렇게 3가지입니다. 3을 입력합니다. 3이 아닌 경우 에러가 발생합니다.
hiddenLayers  |  5, 20  |  사용하고자 하는 hidden layers 갯수를 입력합니다.


## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![1.0.2_02](https://i.imgur.com/PvGt8Wr.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

### 워크플로우 모니터링 하기

Atelier WebToolKit 상단 "Monitoring" 메뉴의 "Workflow" 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다. 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.

### 결과 파일 확인하기

"Repository" 탭에서 학습된 모델이 제대로 생성되었는지 확인합니다. 설정한 BasePath '/model/boston/rf' 에 모델이 생성된 시간 정보를 model ID로 하여 잘 저장되어 있음을 확인할 수 있습니다.

![1.0.2_03](https://i.imgur.com/UvQoCTK.png)

history.csv 파일을 열어 학습된 모델의 성능을 확인합니다. 여러 학습 모델 파라미터 중 tree의 갯수가 20개일 때, 그리고 maxDepth가 10일 때 성능이 가장 좋았음을 확인할 수 있습니다. 그 때 RMSE, MSE, R2, MAE 성능 값도 확인합니다.

![1.0.2_04](https://i.imgur.com/nuhvWdR.png)


# Atelier Spark ML 이해하기


튜토리얼 설명에 앞서 Atelier에서 제공하는 기계학습 모델 생성 방법에 대해 소개합니다.  
Atelier는 Spark Machine Learning을 활용한 기계학습 모델 생성 방법을 지원하며, 손쉬운 사용을 위해 아래의 기능들을 지원합니다.
- Atelier SparkML은 기계학습 모델 생성을 위한 파이프라인 구성을 자동으로 지원합니다.
- Atelier SparkML은 모델 성능 측정, 파라미터 튜닝 및 Cross-Validation 작업을 손쉽게 수행할 수 있도록 지원합니다.
- Atelier SparkML은 간단한 모델 관리 기능을 지원합니다.

각 기능에 대해 설명하겠습니다.

## 파이프라인 구성하기

![1.0.0_01](https://i.imgur.com/HzPmmii.png)

위 화면은 Spark ML을 활용한 기계학습 모델 생성을 위한 샘플 코드를 나타낸 것입니다. 사용하고자 하는 기계학습 알고리즘 선택 후 필수적으로 Label 컬럼으로 사용할 컬럼명과 Feature 컬럼으로 사용할 컬럼명 정보를 입력해야 합니다 (위 샘플 코드에서는 'label'이라는 컬럼을 Label 정보로 'indexedFeatures'라는 컬럼을 Feature정보로 활용하여 Gradient Boosting Tree 알고리즘을 구성하는 샘플 코드를 보여줍니다.) 이 때 다음의 사항들을 고려해야 합니다.
- 다수 개의 컬럼을 Feature로 사용하고자 하는 경우, 다수 개의 컬럼을 하나의 컬럼으로 assemble하는 VectorAssembler라는 데이터 처리 작업이 필요합니다. 그 후 이 컬럼을 Feature 컬럼으로 등록해야 합니다.
- 기본적으로 Spark ML은 숫자형 타입의 정보만을 학습 데이터로 사용할 수 있습니다. 문자열이 포함된 데이터가 있는 경우 먼저 categorical타입의 indexed된 숫자형으로 바꿔주는 StringIndexer라는 데이터 처리 기능을 수행해주어야 합니다.
- Label 컬럼이 문자열로 구성된 경우 (e.x., iris 데이터 셋) 기계학습 모델은 Label 정보를 index된 형태의 숫자형 데이터로 변경한 후 모델을 구성하게 됩니다. 위 모델을 사용하여 데이터를 추론하면 Label 정보가 출력되는 데 이때 출력되는 Label 정보는 문자열 타입이 아닌 index화된 숫자형 형태로 출력되게 됩니다. 따라서 이런 숫자형 형태의 데이터를 다시 Label 정보로 바꾸어주는 LabelConverter라는 데이터 처리 기능을 추가로 수행해 주어야 합니다.

Spark ML 사용시 학습 알고리즘 전 후에 위의 여러가지 데이터 처리 기능을 추가하여 파이프 형태로 일련의 데이터 처리 라인을 만들어야 합니다 (ML 파이프라인을 구성한다라고 표현합니다). 이런 작업이 어렵진 않지만 Spark ML을 처음 접해본 사용자에게는 다소 생생소하며 약간은 번거로운 작업입니다.

Atelier 에서는 위의 일련의 파이프라인 구성을 자동으로 생성할 수 있게 지원합니다.

![1.0.0_02](https://i.imgur.com/PU2SRcF.png)

Atelier 에서 제공하는 SparkML 기반 기계학습 Operator에는 공통적으로 trainInfo라는 필드를 채우는 것을 필요로 합니다. trainInfo버튼을 클릭하면 오른쪽 세부 설정 항목이 나타납니다. 여기서 labelColumnName에 Label정보로 사용하고자 하는 컬럼명 이름을, featureColumnNames에 Features정보로 사용하고자 하는 컬럼명 이름을 입력하면 됩니다. 다수 개의 컬럼을 Features 정보로 사용하고자 하는 경우에는 사용하고자 하는 모든 컬럼명을 콤머로 분리하여 입력해주면 됩니다. \*를 입력한 경우 Label 정보로 사용하는 컬럼을 제외한 모든 컬럼들을 Feature 정보로 사용하게 됩니다.  

iris 데이터 셋을 예시로 하여 위 설정사항을 설명하겠습니다. iris 데이터 셋은 꽃 모양 형태 (sepal_length, sepal_width, petal_length, petal_width) 데이터를 Feature로 활용하여 붓꽃 종류 (species)를 분류하기 위한 데이터 셋입니다. 다른 데이터 셋과 달리 Label 데이터인 species 컬럼이 'setosa, versicolor, virginica' 이렇게 문자열 형태로 주어진다는 특징이 있습니다. 이런 iris 데이터 셋에 위의 trainInfo 설정과 동일하게 설정하면 아래의 ML 파이프라인이 자동으로 구성됩니다.

![1.0.0_03](https://i.imgur.com/sOw4J2i.png)

먼저 Label 데이터인 'species'컬럼을 접근하여 스키마 정보를 추론하게 됩니다. 문자열 데이터는 학습 데이터로 사용할 수 없기에 Label 데이터를 index화된 형태의 숫자형 데이터로 변경하는 StringIndexer 작업을 파이프라인의 맨 처음 stage에 등록하게 됩니다. 그 후 Feature로 사용할 데이터를 찾게 됩니다. \*로 설정되었기에 Label 컬럼을 제외한 모든 컬럼을 Feature로 사용하게 되며, 이를 위해 꽃 모양 형태 정보를 담고 있는 4개의 컬럼을 하나의 Feature 컬럼으로 만드는 VectorAssembler 작업을 파이프라인에 등록하게 됩니다 (Feature 컬럼 중 문자열 데이터가 포함되어 있다면 그 컬럼을 숫자형 데이터로 변경하는 StringIndexer작업이 선행 등록됩니다). 그 후 학습을 수행할 수 있게 GBTRegressor 알고리즘이 파이프라인에 등록하게 됩니다. Label정보가 index화 되었기에 학습 모델 추론 결과 또한 index화된 숫자형 형태가 됩니다. 다시 원래의 Label 정보로 변경할 수 있게 LabelConverter 작업을 전체 파이프라인의 맨 마지막 stage에 등록하게 됩니다.

Atelier 기반 SparkML 에서는 Label 컬럼명과 Feature 컬럼명만 명시해 주면, 위의 복잡한 ML 파이프라인 구성 작업을 자동적으로 수행해줍니다. 모델을 저장하면 GBTRegressor알고리즘만 저장되는게 아니라 위 그림의 파이프라인 전체가 저장됩니다.


### 모델 성능 측정, Cross-validation, 그리고 파라미터 튜닝하기

모델의 ML 파이프라인 구성이 완료되면 학습 데이터를 활용하여 training과정을 수행하게 됩니다. 이 때 모델의 성능을 어떻게 측정할지 명시해줘야 합니다. 일반적으로 Binary Classification 작업인 경우에는 areaUnderPR, areadUnderROC metric을, Multi-class Clasisfication 작업에는 fmeasure, weightPrecision, weightedRecall, accuracy metric을, Regression 작업인 경우에는 rmse, mse, r2, mae metric을, Clustering 작업인 경우에는 silhouette, squaredEucllidean, cosine metric을 사용합니다 (현재 Atelier에서는 Clustering 작업 관련 metric은 지원 안됩니다). 성능 Metric을 설정하면 모델 파이프라인에 의해 측정된 예측값과 실제 Ground-truth값과의 차이를 선택된 metric 계산 방법에 의거하여 계산하게 됩니다.

![1.0.0_04](https://i.imgur.com/wMyOIm8.png)

모델의 성능은 train set과 test set을 어떻게 나누냐에 따라 달라지게 됩니다. 이런 성능 편향을 방지하기 위해 일반적으로 k cross-validation(교차검증)이라 하여 테스트 셋을 바꿔가며 모델 성능을 평가하게 됩니다. Atelier에서는 위 trainInfo 설정 화면의 numFolds 항목에서 cross-validation 정보를 입력받습니다. 그림에서와 같이 5로 설정하면 전체 학습 데이터를 5개로 나눈 후 첫번째 데이터 셋을 test set으로 설정하여 학습을 수행하게 됩니다. 그 후 2번째 데이터 셋을 test set으로 설정하여 학습을 수행하고, 이런 식으로 반복해서 최종 5번째 데이터 셋을 test set으로 설정하여 모델 학습 과정을 수행하게 됩니다. 이렇게 5번 반복해서 수행한 학습 결과를 평균 내 학습 모델의 성능으로 최종 판단하게 됩니다.

![1.0.0_05](https://i.imgur.com/BrVhj8n.png)

각 학습 모델에는 성능을 주요하게 결정하는 여러가지 파라미터가 있습니다. 가령 Random Forest 학습 모델의 경우, Forest를 구성하는 tree의 갯수를 몇개로 할 건지 tree내 depth는 얼마로 설정할 건지에 따라 그 성능이 주요하게 바뀝니다. 이런 주요 성능 값을 바꿔가며 주어진 데이터에 최적화된 학습 모델을 만들어 가는 작업을 파라미터 튜닝 작업이라고 합니다.

Atelier에서는 이런 파라미터 튜닝 작업을 손쉽게 할 수 있게 지원합니다. 아래 그림은 Atelier에서 제공하는 Random Forest 파라미터 튜닝 방법을 보여주고 있습니다.

![1.0.0_06](https://i.imgur.com/sNZgqb5.png)

여기서는 간단하게 여러 파라미터 중 numTrees와 maxDepth만을 변경하도록 설정하였습니다. 이렇게 설정하고 실행하면 먼저 numTrees를 10개로, maxDepth를 5로 설정한 후 위에서 설정했던 ML 파이프라인을 자동 구성하여 모델을 학습시켜 성능 metric을 측정하게 됩니다. 그 다음 numTress를 10개로, maxDepth를 10으로 설정한 후 모델을 학습시켜 성능 metric을 측정하게 되고 이런 식으로 총 4번 학습 모델을 돌리게 됩니다. 그 중 가장 좋은 성능 값을 갖는 ML 파이프라인을 최종 학습 모델로 도출해줍니다.

R, Scikitlearn등 다른 학습 모델 툴과는 달리 Spark ML에서는 여러가지 파라미터를 설정할 수 있습니다. 가령 R의 경우 Random Forest 학습 파라미터는 mtry 하나입니다 (wrapping과정을 통해 ntrees를 추가할 수도 있습니다). 그에 반해 Spark ML기반의 파라미터는 위 화면에서 보듯 상당히 많은 수의 파라미터를 조절할 수 있습니다. 모델 전문가라면 많은 파라미터를 세부적으로 조절하여 학습 모델을 fine-tuning하는 게 장점일 수 있겠지만 처음 접해본 입장에서는 상당히 어려울 수 있습니다. 추후 학습 모델 성능을 주요하게 결정하는 파라미터를 제시하여 우선순위 순으로 조절할 수 있도록 할 예정입니다.

![1.0.0_07](https://i.imgur.com/ctBe8z9.png)

파라미터 튜닝 값까지 입력하면, 최종적으로 기계학습 모델을 위한 전체 파이프라인이 위와 같이 구성되게 됩니다. 워크플로우를 실행하면 파이프라인과 Estimator가 Cross-validatior에서 설정한 k번만큼 실행되어 그 평균값으로 모델 성능이 측정되게 됩니다. 이런 과정을 파라미터 튜닝에 설정한 모든 파라미터의 조합에 대해 실행되고 최종적으로 성능이 가장 좋은 ML 파이프라인 모델이 모델 저장소 (trainInfo의 \_path에 설정한 path)에 저장되게 됩니다.


### 생성된 모델 관리하기

Atelier에서 제공하는 간단한 모델 관리 기능에 대해 소개합니다.

파라미터 튜닝에 의해 최종 선택된 ML 파이프라인 모델은 생성된 날짜를 ID로 하여 모델 저장소에 저장되게 됩니다. 재학습이 될 때에도, 그 생성 시점을 기점으로 모델 저장소에 저장되게 됩니다. 아래 그림은 GBT (Gradient Boosting Tree) 알고리즘을 사용하여 생성한 모델 파이프라인의 저장소를 나타낸 것입니다.

![1.0.0_08](https://i.imgur.com/TlNKFw0.png)

history.csv파일에는 생성된 모델 파이프라인의 성능에 대한 정보가 담겨 있습니다. 아래 그림은 이를 나타냅니다.

![1.0.0_09](https://i.imgur.com/D9pxkSx.png)

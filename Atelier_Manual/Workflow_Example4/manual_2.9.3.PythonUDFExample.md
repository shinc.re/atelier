
# PythonUDF 기반 데이터 처리하기 (PythonUDFDockerizeExample)

Dockerize 기능을 이용하여 Python 으로 구현된 사용자 정의 함수를 Docker 이미지로 변환하고 REST API 를 추가하는 기능입니다.
이 예제에서는 파이썬의 이미지 처리 라이브러리 Pillow 를 이용하여 BeeAI 에서 입력 이미지를 처리하는 예제를 설명합니다.
Dockerze 기능에 대한 자세한 설명은 매뉴얼(**Python 모듈 사용하기: Dockerize**)을 참고합니다.

![2.9.3.PythonUDF_intro](https://i.imgur.com/fmEfn2S.png)

## 입력 데이터 준비하기

본 예제에서는 Host PC의 /home/csle/ksb-csle/examples/pyModules/Tutorials/modules 폴더에 존재하는 images 폴더를 사용자의 HDFS (pymodule/Tutorials/modules/image/) 에 등록하여 사용합니다. 웹툴킷 상단의 **Storage** 메뉴를 클릭하여 데이터 저장소 관리화면으로 이동합니다.

![2.9.3.pythonUDF_modules](https://i.imgur.com/ShxkEMi.png)

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | 2.9.3.PythonUDFDockerizeExample | 워크플로우 이름     
description  | python UDF 기반 image처리 테스트  | 워크플로우를 설명하는 글
isBatch  | true | 배치 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | true | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |
Project | BeeAI Tutorials  |   워크플로우가 소속될 프로젝트 이름

### 엔진 선택
Storage 에 등록된 파일을 입력 받아 전처리를 수행한 후 로컬 파일에 저장하기 위해 **OnDemandExternalServing** 엔진을 드래그앤 드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName  | 설명
--|---|---|---
1  | OnDemandExternalServing  | ImageProcessingEngine | 사용자의 이미지처리용 PythonUDF 를 Dockerize화 하는 엔진


#### Reader
**OnDemandExternalServing** 엔진에는 Reader 컴퍼넌트를 설정하지 않습니다.


#### Writer
**OnDemandExternalServing** 엔진에는 Writer 컴퍼넌트를 설정하지 않습니다.

#### Controller
**OnDemandExternalServing** 엔진에는 Controller 컴퍼넌트를 설정하지 않습니다.


#### Runner
**PyModuleRunner** 를 선택합니다.

field  |value   | 설명
--|---|--
port |  18081 |  RESTful API 에서 사용할 포트번호
name  | dockerize_image |  사용자의 PythonUDF 가 Docker 이미지화 될 때 부여될 Docker이미지의 이름 (<span style="color:red">name 은 영문소문자로만 작성해야합니다. 특수문자는 \_  만 허용됩니다. </span>)
path  | /pymodule/modules/image/ | 사용자의 PythonUDF 가 저장되어있는 저장소의 경로
libraries  | - |  
userArgs  | - |  


#### Operator
**OnDemandExternalServing** 엔진에는 Operator 컴퍼넌트를 설정하지 않습니다.


<br>

![2.9.3_01](https://i.imgur.com/q28n61h.png)

<br>

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![2.9.3_02](https://i.imgur.com/M4SaGK2.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![2.9.3_03](https://i.imgur.com/ILIoxDP.png)



### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인

Atelier WebToolKit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다.

![2.9.3_04](https://i.imgur.com/S7LUq67.png)


## 결과 확인하기
Host PC 에서 docker ps 명령을 이용하여 실행중인 Python UDF 의 Docker 컨테이너를 확인합니다.

![2.9.3_05](https://i.imgur.com/gkn1N05.png)

준비되어 있는 테스트 프로그램 (/home/csle/ksb-csle/examples/pyModules/Tutorials/client.py) 를 실행하여 이미지 처리 결과를 확인합니다.

![2.9.3_06](https://i.imgur.com/q81kGDi.png)

PythonUDF 기반 데이터 처리는 다양한 종류의 입력 데이터에 대해 활용할 수 있습니다. BeeAI Tutorials 프로젝트의 2.9.3.PythonUDFDockerizeExample(json) 워크플로우와 2.9.3.PythonUDFDockerizeExample(nparray) 워크플로우를 사용해서 파이썬 사용자 정의 함수 기반의 json 문자열 처리 기능과 nparray 데이터 처리 기능도 실습해보시기 바랍니다.


# AutoML 학습하기 (BatchAutoMLTrainInSingleEngine)

이 예제에서는 자동기계학습 (**AutoML**: Automated Machine Learning)을 이용하여 최적모델을 학습하는 방법을 설명합니다.

자동기계학습은 기계학습 전문가의 도움없이 주어진 입력자료에 대해 최적의 기계학습모델을 자동으로 찾아내는 기술입니다. Atelier Framework는 독자적으로 개발된 Auto-SparkML (Automated Machine Learning for Apache Spark)을 통해 Big-Data 분석용 AutoML 기능을 제공합니다.


![2.5.5.BatchAutoML_intro](https://i.imgur.com/nEnfvLM.png)


## 입력 데이터 준비하기

입력데이터는 Apache Spark이 제공하는 DataFrame 을 저장하고 있는 parquet 파일이어야 합니다. 이 때 DataFrame은 "features" 칼럼과 "label" 칼럼을 반드시 포함하고 있어야 합니다. "features" 칼럼은 VectorAssembler 형태의 자료여야 합니다 (참고: https://spark.apache.org/docs/latest/ml-features.html#vectorassembler). "label" 칼럼은 Classification의 경우에는 수치 혹은 문자열이어야 하며 (예: "cat", "dog"), Regression의 경우에는 수치값이어야 합니다.

본 예제에서는 Host PC의 "/home/csle/ksb-csle/pyML/autosparkml/datasets/iris_dataset" 폴더를 Storage 의 "dataset/iris_dataset" 로 업로드하여 사용합니다. 이 자료는 DataFrame 을 저장하고 있는 parquet 형태의 자료입니다.

<b>참고</b>: 입력데이터가 CSV 형태일 때 parquet로 변환하는 예제는 이 매뉴얼을 참고하면 됩니다: <a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.7.3.CSV2Parquet.md">CSV 파일 Parquet 변환</a>

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래 과정을 거쳐 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  |  BatchAutoMLTrainInSingleEngine |  워크플로우 이름
description  | AutoML 학습 예제 | 워크플로우 설명
isBatch  |  true | Batch 실행여부
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |   
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름


### 엔진 선택
AutoML 기능을 사용하기 위해 **External** 엔진을 드래그앤 드롭합니다.


#### Reader
파일형태의 입력자료를 읽기위해 **FileBatchReader** 를 드래그앤 드롭하고, 아래표와 같이 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  |  dataset/iris_dataset | 파일경로
fileType  | CSV |  파일형태
delimiter  |  , |  구분자
field  |   | 상세설정
header  | false  |  헤더포함여부

filePath는 직접 입력할 수도 있으며, 아래 화면처럼 GUI를 이용해 선택할 수도 있습니다 (filePath 속성에 "File" 버튼 클릭). 기타 속성은 기본값으로 두면 됩니다. <b>참고</b>: 상기하였듯이, 입력파일 형태는 parquet 만 지원하기 때문에 기타 속성을 변경하여도 적용되지 않습니다.

![2.5.5_01](https://i.imgur.com/DU97q5r.png)

#### Writer
학습결과물을 파일형태로 저장하기 위해 **FileBatchWriter** 를 드래그앤 드롭합니다.

field  |value   | 설명
--|---|--
filePath  |  autosparkml |  저장경로
fileType  | CSV |  파일형태
delimiter  |  , |  구분자
field  |   | 상세설정
header  | false  |  헤더포함여부

Reader의 경우와 마찬가지로 filePath는 직접 입력할 수도 있으며, GUI를 이용해 선택할 수도 있습니다. 이 외의 속성은 기본값으로 설정합니다.

#### Controller
Controller는 **PySparkMLTrainer** 를 사용합니다.

#### Runner
**PySparkRunner** 를 드래그앤 드롭합니다.


sparkArgs 버튼을 누르면 아래와 같이 Apache Spark 실행환경을 설정할 수 있는 창이 뜹니다. 이 예제에서는 기본값을 사용합니다.

![2.5.5_02](https://i.imgur.com/Qlk6aBJ.png)

#### Operator
AutoML 기능을 사용하기 위해 **AutoSparkMLOperator** 를 드래그앤 드롭합니다. AutoML은 모든 과정이 자동으로 수행되지만 기계학습 유형은 선택해야 합니다 (예: Classification 혹은 Regression).

field  |value   | 설명
--|---|--
type  |  Classification |  기계학습 유형


### 워크플로우 완성 화면

아래 그림은 위 과정을 거쳐 완성된 워크플로우 화면입니다.

 <b>참고</b>: 이미 만들어진 워크플로우를 사용할 때에도, iris_dataset 은 Storage 에 따로 업로드해야 합니다.

![2.5.5_03](https://i.imgur.com/ve7b3Tu.png)




## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![2.5.5_04](https://i.imgur.com/OVGKFVE.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![2.5.5_05](https://i.imgur.com/c6E37R4.png)



### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인
웹툴킷 상단 "Monitoring" 메뉴의 "Workflow" 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)할 수 있으며, 종료된 워크를로우를 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수도 있습니다.

![2.5.5_06](https://i.imgur.com/pu9F38w.png)

#### 워크플로우 로그 보기
위 화면에서 "WorkFlow History" 탭을 선택하면, Atelier가 수행한 워크플로우들의 목록 및 각각의 로그 (목록 최우측의 <b><span style="color:#6698FF">i</span></b> 버튼 클릭)를 확인할 수 있습니다.

![2.5.5_07](https://i.imgur.com/FhNh7wi.png)

위 화면에서 "Text 결과 파일 보기" 버튼을 누르면 학습성능 파일을 아래처럼 열어볼 수 있습니다. Classification 경우에는 $F_1$ 스코어가 기록됩니다. Regression의 경우에는 MAE (Mean Absolute Error), MSE (Mean Squared Error) 및 MAPE (Mean Absolute Percentage Error)가 기록됩니다.


![2.5.5_08](https://i.imgur.com/6gmvfjz.png)

## 결과 확인하기

### 최적학습모델 확인하기
워크플로우 실행의 결과물인 최적학습모델은 FileBatchWriter 속성에서 설정한 저장경로에 저장되어 있으며, 웹툴킷 상단의 "Stroage" 메뉴에서 확인할 수 있습니다. 해당 저장경로 ("autosparkml") 안에 모델을 포함하는 폴더 및 파일이 생성되어 있으면 워크플로우가 성공적으로 수행된 것입니다 (아래 그림 참조). 동일한 폴더 아래에 있는 "acc.csv" 파일은 위에서 설명한 "Text 결과 파일 보기" 버튼을 눌러 보이는 결과를 포함하는 파일입니다.

![2.5.5_09](https://i.imgur.com/0x3H6pd.png)


# CSV 파일 Parquet로 저장하기

이 예제에서는 CSV 자료를 Batch 형태로 입력받아 Parquet 파일로 저장하는 방법에 대해 설명합니다.

<b>참고</b>: Parquet 파일은 Atelier가 제공하는 <a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.5.5.BatchAutoMLTrainInSingleEngine.md">AutoML 기능</a> 혹은 <a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.5.4.BatchMLTrainInSingleEngine.md">ML학습 기능</a>을 사용하기 위해 필요한 입력자료 형태입니다.

## 입력 데이터 준비하기
입력데이터는 헤더 정보를 포함하는 CSV 파일이어야 합니다. 본 예제에서는 Host PC의 "/home/csle/ksb-csle/examples/input/iris.csv" 를 사용자 Stroage 의 "dataset/input" 폴더에 업로드하여 사용합니다.

<b>참고</b>: 이전 버전의 KSB 프레임워크를 사용하고 있다면 iris.csv 가 없을 수도 있습니다. 이 경우 <a href="https://gist.github.com/curran/a08a1080b88344b0c8a7">여기에서</a> 다운받으면 됩니다.

iris.csv 는 다음과 같은 값을 저장하고 있습니다.

```sh
sepal_length,sepal_width,petal_length,petal_width,species
5.1,3.5,1.4,0.2,setosa
4.9,3.0,1.4,0.2,setosa
4.7,3.2,1.3,0.2,setosa
4.6,3.1,1.5,0.2,setosa
5.0,3.6,1.4,0.2,setosa
5.4,3.9,1.7,0.4,setosa
```

첫번째 줄은 헤더이고, 두번째 줄부터 자료값입니다. 각각의 값은 "," 로 분리되어 있어야 합니다.

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여, 아래 과정을 거쳐 워크플로우를 생성합니다.

field  |value   | 설명
--|---|--
name  |  2.7.3.CSV2Parquet |  워크플로우 이름
description  | CSV 파일 Parquet로 변환하기 | 워크플로우 설명
isBatch  |  true | Batch 실행여부
RunType  |  즉시실행  |  
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름

### 엔진 선택
기계학습모델 학습기능을 사용하기 위해 **Batch** 엔진을 드래그앤 드롭합니다.

#### Reader
파일을 읽기위해 **FileBatchReader** 를 드래그앤 드롭하고, 아래표와 같이 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  |  dataset/input/iris.csv | 파일경로
fileType  | CSV |  파일형태
delimiter  |  , |  구분자
field  |   | 상세설정
header  | true  |  헤더포함여부

![2.7.3_01](https://i.imgur.com/9BapzXE.png)

filePath는 직접 입력할 수도 있으며, 아래 화면처럼 GUI를 이용해 선택할 수도 있습니다 (filePath 속성에 "File" 버튼 클릭). 기타 속성은 기본값으로 두면 됩니다.

![2.7.3_02](https://i.imgur.com/tEbKdIV.png)

#### Writer
Parquet 파일을 저장하기 위해 **FileBatchWriter**를 드래그앤 드롭합니다.

field  |value   | 설명
--|---|--
filePath  |  dataset/input/iris.parquet |  저장경로 및 파일이름
fileType  | PARQUET |  파일형태
delimiter  |  , |  구분자
field  |   | 상세설정
header  | true  |  헤더포함여부

![2.7.3_03](https://i.imgur.com/mv8AcXH.png)

Reader의 경우와 마찬가지로 filePath는 직접 입력할 수도 있으며, GUI를 이용해 선택할 수도 있습니다.

#### Controller
Controller 로는 **SparkSessionController** 를 드래그앤 드롭합니다.

#### Runner
**SimpleSparkRunner** 를 드래그앤 드롭합니다.

field  |value   | 설명
--|---|--
inJason  |  false | Jason 형태 파라미터 전달여부
sparkArgs  |   |  Spark 상세설정

sparkArgs 버튼을 누르면 Apache Spark 실행환경을 세부설정할 수 있는 창이 뜹니다. 이 예제에서는 기본값을 사용합니다.

#### Operator

이 예제에서는 CSV 파일의 칼럼들을 제어하기 위해 두개의 Operator를 사용합니다. 첫번째 Operator 는 **VectorizeColumnOperator** 입니다. 이 Operator 는 CSV 의 특정 칼럼을 선택해 Vector 로 합쳐 하나의 칼럼에 할당합니다. 속성값을 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
srcColumnNames  | sepal_length,sepal_width,petal_length,petal_width  | Vectorize 할 칼럼이름. "," 로 구분.
destColumnNames  | features  |  Vectorize 된 칼럼 이름
keepSrcColumns  | true  |  기존 칼럼 유지 여부

![2.7.3_04](https://i.imgur.com/jR9xSCR.png)

두번째로 사용할 Operator 는 **RenameColumnOperator** 입니다. 이 Operator 는 CSV의 특정칼럼 이름을 원하는 이름으로 변경해줍니다. 속성값을 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
existingName  | species | 기존 칼럼 이름
newName  | label  |  새로운 칼럼 이름

![2.7.3_05](https://i.imgur.com/e3j6SR8.png)

위 두 과정을 거치게 되면, 1) `sepal_length,sepal_width,petal_length,petal_width` 총 4개의 칼럼을 Vectorize 한 칼럼이 "features" 라는 이름으로 생성되고, 2) `species` 라는 이름을 가졌던 칼럼이 `label` 이라는 이름의 칼럼으로 변경됩니다.

### 워크플로우 완성 화면

아래 그림은 위 과정을 거쳐 완성된 워크플로우 화면입니다.

![2.7.3_06](https://i.imgur.com/nhwWbnS.png)

## 워크플로우 실행 및 모니터링하기

### 워크플로우 실행하기
위에서 작성한 워크플로우를 실행하기 위해서는 워크플로우 편집기 좌측의 '운영화면' 메뉴를  누릅니다.


### 워크플로우 모니터링 하기

#### 워크플로우 빌드 하기
작성한 워크플로우를 빌드하여 path 정보 등을 절대값으로 변환하고, 대상 파일이 존재하는 지 등을 체크하여 워크플로우가 실제로 실행되는데 문제가 없는지 확인합니다.

![2.7.3_07](https://i.imgur.com/533ud4B.png)

#### 워크플로우 배포(실행) 하기
워크플로우가 잘 빌드 되었으면, 이제 배포 버튼을 눌러 워크플로우 Job을 수행 합니다.
배포가 시작되면, Engine 실행 로그를 통해 진행상태를 확인할 수 있습니다.

![2.7.3_08](https://i.imgur.com/dFAd3Ju.png)

##  변환된 Parquet 파일 확인하기
실행결과물은 웹툴킷 상단의 "Storage" 메뉴에서 확인할 수 있습니다. FileBatchWriter에서 설정한 경로 "dataset/input/iris.parquet" 에 파일이 생성되어 있으면 워크플로우가 성공적으로 수행된 것입니다. "iris.parquet" 파일 내부는 "Text 결과 파일 보기"를 클릭해 확인할 수 있습니다 (아래 그림 참조).

<b>참고</b>: 생성된 파일은 parquet 파일로 일반 텍스트 형태가 아닙니다. 따라서 제대로 된 내용확인을 위해서는 Spark 프로그래밍이 필요합니다 (https://spark.apache.org/docs/latest/sql-programming-guide.html#parquet-files).

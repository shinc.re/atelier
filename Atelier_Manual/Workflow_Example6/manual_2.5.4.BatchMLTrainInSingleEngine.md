
# ML 모델 학습하기 (BatchMLTrainInSingleEngine)

이 예제에서는 기계학습모델을 학습하는 방법을 설명합니다.

Atelier는 다수의 기계학습모델 학습기능을 제공합니다. 제공하는 모델은 Classification 3종, Regression 3종으로 각각 아래와 같습니다.

Type | 기계학습모델
--|--
<span style="color:green">Classification</span>  |   
&nbsp; | Naive Bayes  |  
&nbsp; |  Decision Tree |  
&nbsp; |  Random Forests |  
<span style="color:green">Regression</span>  |  
&nbsp; |  Decision Tree
&nbsp; |  Linear Regression
&nbsp; |  Random Forests

이 예제에서는 Decision Tree 모델을 학습하는 과정을 보여줍니다.

## 입력 데이터 준비하기

입력데이터는 Apache Spark이 제공하는 DataFrame 을 저장하고 있는 parquet 파일이어야 합니다. 이 때 DataFrame은 "features" 칼럼과 "label" 칼럼을 반드시 포함하고 있어야 합니다. "features" 칼럼은 VectorAssembler 형태의 자료여야 합니다 (참고: https://spark.apache.org/docs/latest/ml-features.html#vectorassembler). "label" 칼럼은 Classification의 경우에는 수치 혹은 문자열이어야 하며 (예: "cat", "dog"), Regression의 경우에는 수치값이어야 합니다.

본 예제에서는 Host PC의 "/home/csle/ksb-csle/pyML/autosparkml/datasets/iris_dataset" 폴더를 Storage 의 "dataset/iris_dataset" 로 업로드하여 사용합니다. 이 자료는 DataFrame 을 저장하고 있는 parquet 형태의 자료입니다.

<b>참고</b>: 입력데이터가 CSV 형태일 때 parquet로 변환하는 예제는 이 매뉴얼을 참고하면 됩니다: <a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.7.3.CSV2Parquet.md">CSV 파일 Parquet 변환</a>

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여, 아래 과정을 거쳐 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  |  BatchMLTrainInSingleEngine |  워크플로우 이름
description  | DecisionTree 분류 예제 | 워크플로우 설명
isBatch  |  true | Batch 실행여부
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정  
RunType | 즉시실행 |   
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름


### 엔진 선택
기계학습모델 학습기능을 사용하기 위해 **External** 엔진을 드래그앤 드롭합니다.




#### Reader
파일형태의 입력자료를 읽기위해 **FileBatchReader** 를 드래그앤 드롭하고, 아래표와 같이 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  |  dataset/iris_dataset | 파일경로
fileType  | CSV |  파일형태
delimiter  |  , |  구분자
field  |   | 상세설정
header  | false  |  헤더포함여부

filePath는 직접 입력할 수도 있으며, 아래 화면처럼 GUI를 이용해 선택할 수도 있습니다 (filePath 속성에 "File" 버튼 클릭). 기타 속성은 기본값으로 두면 됩니다. <b>참고</b>: 상기하였듯이, 입력파일 형태는 parquet 만 지원하기 때문에 기타 속성을 변경하여도 적용되지 않습니다.

![2.5.4_01](https://i.imgur.com/89AWqmu.png)

#### Writer
학습결과물을 파일형태로 저장하기 위해 **FileBatchWriter** 를 드래그앤 드롭합니다.

field  |value   | 설명
--|---|--
filePath  |  decisiontree/ |  저장경로
fileType  | CSV |  파일형태
delimiter  |  , |  구분자
field  |   | 상세설정
header  | false  |  헤더포함여부

Reader의 경우와 마찬가지로 filePath는 직접 입력할 수도 있으며, GUI를 이용해 선택할 수도 있습니다. 이 외의 속성은 기본값으로 설정합니다.

#### Controller
Controller로는 PySparkMLTrainer 를 Drag & Drop 합니다.

#### Runner
PySparkRunner를  Drag & Drop 합니다.

sparkArgs 버튼을 누르면 아래와 같이 Apache Spark 실행환경을 설정할 수 있는 창이 뜹니다. 이 예제에서는 기본값을 사용합니다.

![2.5.4_02](https://i.imgur.com/fTjOjaO.png)

#### Operator
Decision Tree 모델을 학습하기 위해 **DecisionTreeClassifier** 를 드래그앤 드롭합니다. 속성값은 따로 설정하지 않아도 Atelier가 자동으로 최적의 값을 찾아 최적모델을 학습합니다. 이는 위 테이블에서 나열한 Classification 3종 및 Regression 3종 모델의 경우에도 마찬가지입니다.

field  |value   | 설명
--|---|--
maxDepth  | 0 보다 큰 값  |  Tree node의 최대 깊이
maxBins  | 0 보다 큰 값  |  Tree 최대 Bin 개수

maxDepth 값이 클수록 좋은 성능의 모델이 학습될 가능성은 높아지지만 학습속도는 느려집니다. maxBins은 입력자료값을 이산화하는 최대 개수를 나타냅니다. 입력샘플 수보다는 작아야 합니다. maxBins 값이 클수록 모델성능이 높아질 가능성이 있지만, 학습속도는 느려집니다.

### 워크플로우 완성 화면

![2.5.4_03](https://i.imgur.com/G4ADNJA.png)



## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![2.5.4_04](https://i.imgur.com/M4bREFo.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![2.5.4_05](https://i.imgur.com/FnGVY5d.png)


### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인
"Monitoring" 메뉴의 "Workflow" 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)할 수 있으며, 종료된 워크를로우를 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수도 있습니다.

![2.5.4_06](https://i.imgur.com/h1GQK8r.png)

## 결과 확인하기

### 최적학습모델 확인하기
워크플로우 실행의 결과물인 최적학습모델은 FileBatchWriter 속성에서 설정한 저장경로에 저장되어 있으며, Atelier WebToolKit 상단의 "Stroage" 메뉴에서 확인할 수 있습니다. 해당 저장경로 ("decisiontree") 안에 모델을 포함하는 폴더 및 파일이 생성되어 있으면 워크플로우가 성공적으로 수행된 것입니다 (아래 그림 참조). 동일한 폴더 아래에 있는 "acc.csv" 파일은 위에서 설명한 "Text 결과 파일 보기" 버튼을 눌러 보이는 결과를 포함하는 파일입니다.

![2.5.4_07](https://i.imgur.com/aE3SQ5m.png)

![2.5.4_08](https://i.imgur.com/4wpY5Ml.png)

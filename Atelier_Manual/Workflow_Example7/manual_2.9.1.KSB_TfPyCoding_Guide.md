
# Atelier와 연동을 위한 TensorFlow 학습코드 작성하기

Atelier는 파이썬 기반 사용자의 TensorFlow 코드로 파라미터를 전달하고, 텐서플로우 모델 서빙용으로 export된 모델을 하둡파일시스템(HDFS)으로 복사해오는 방식으로 연동이 됩니다.

![2.9.1_introduction](https://i.imgur.com/IbYCGSd.png)

따라서 사용자는 TensorFlow 모델 학습을 위한 코드 작성 시, 프레임워크에서 전달하는 파라미터(입력파일 경로, 모델 저장 경로 등)를 받는 부분, 학습된 모델을 텐서플로우 서빙용으로 export 하는 부분을 구현해야 합니다.

Atelier에서 텐서플로우 기반 모델을 학습하고 서빙하는 절차는 아래와 같습니다. **교통속도 예측 튜토리얼** 을 참고합니다.
1. Atelier에서 모델 입력 데이터 (모델 출력 데이터, 즉 label 포함) 를 가공하여 csv 파일로 저장합니다. 하나의 파일에 입력 데이터와 학습을 위한 target 이 같이 저장되어야 합니다. <a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.6.1.TrafficPreprocessing.md">교통속도 스트림 데이터 전처리하기</a> 를 참고합니다.

2. 사용자 파이썬 코드에서 앞에서 가공한 csv 파일을 읽고 딥러닝 모델 학습을 합니다. 프레임워크와 사용자 파이썬 코드는 입력 파일 경로, 모델 export 경로 등의 파라미터를 전달할 수 있도록 연동되어 있습니다. <a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.6.2.TrafficTraining.md">교통속도예측 텐서플로우 모델 학습하기</a> 를 참고합니다.

3. 학습 후 export 한 모델을 프레임워크에서 REST API 서비스가 가능하도록 띄우고, 클라이언트는 쿼리를 통해 예측 결과를 받을 수 있습니다. 사용자는 온디맨드 방식으로 서비스를 구성하거나 (<a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.6.3.TrafficOnDemandServing.md">온디맨드 방식으로 교통속도예측 모델 서빙하기</a> 참고) 스트림 방식으로 서비스를 구성할 수 있습니다 (<a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.6.4.TrafficStreamServing.md">스트림 방식으로 교통속도예측 모델 서빙하기</a> 참고).


## 프레임워크에서 전달하는 파라미터를 받는 부분 작성
사용자 TensorFlow 코드를 Atelier와 연동하기 위해서는 기본적으로 4개의 파라미터를 Atelier로부터 전달 받습니다. 그 밖의 사용자 정의 파라미터는 **DLTrainOperator** 의 additionalParams 필드를 이용하여 전달받을 수 있습니다.

```python
from absl import app, flags, logging

FLAGS = flags.FLAGS
# 기본적인 4개의 파라미터 선언
flags.DEFINE_string('dataset', '/tmp/pts/data/dataset.csv', 'Dataset path')
flags.DEFINE_string('checkpoint_dir', '/tmp/pts/ckpts', 'Checkpoint directory')
flags.DEFINE_string('model_version', '0000', 'Model version')
flags.DEFINE_string('model_dir', '/tmp/pts/model/0000', 'Model directory')
# 사용자 정의 파라미터 선언
flags.DEFINE_bool('is_train', True, 'Training mode')
flags.DEFINE_integer('num_epochs', 3, 'Number of epoch')
```

<br>
Atelier에서 전달하는 파라미터는 웹툴킷에서 설정한 컴퍼넌트의 속성 값입니다.

### --dataset : FileBatchReader의 path

**FileBatchReader** 의 속성 중 path 에 입력 데이터셋의 경로를 지정합니다. 이 값은 사용자 TensorFlow 코드의 --dataset 파라미터로 전달됩니다.
아래와 같이 3가지 경로가 입력 가능합니다.

![2.9.1_FileReader](https://i.imgur.com/T9hnn6J.png)

1) 로컬 파일시스템 절대경로
   file:///home/csle/ksb-csle/examples/input/{선택한 경로 및 파일명}
2) hdfs 절대경로
   hdfs://{host}:{port}/user/{user_id}/dataset/{선택한 경로 및 파일명}
3) Hdfs 상대경로
   dataset/{선택한 경로 및 파일명}


### --model_dir : FileBatchWriter의 path

**FileBatchWriter** 의 속성 중 path에 학습된 모델을 저장할 경로를 지정합니다. 이 값은 사용자 TensorFlow 코드의 --model_dir 파라미터로 전달됩니다. 폴더명까지만 입력합니다. 해당 폴더에 자동으로 최신버전의 폴더를 생성한 후 모델이 저장됩니다.
아래와 같이 3가지 경로가 입력 가능합니다.

![2.9.1_FileWriter](https://i.imgur.com/vFJbNIz.png)

1) 로컬 파일시스템 절대경로
   file:///home/csle/ksb-csle/{선택한 경로}
2) hdfs 절대경로
   hdfs://{host}:{port}/user/{user_id}/model/{선택한 경로}
3) hdfs 상대경로
   model/{선택한 경로}
   위와 같이 입력하는 경우, 자동으로 **사용자 기본 파일경로** 가 앞에 추가됩니다. 따라서 아래와 같은 위치에 모델이 저장됩니다.  hdfs://{host}:{port}/user/{user_id}/applications/{workflow_id}/output/model/{선택한 경로}

### --checkpoint_dir : DLTrainOperator의 path

DLTrainOperator의 속성 중 path에 체크포인트를 저장할 경로를 지정합니다. 이 값은 사용자 TensorFlow 코드의 --checkpoint_dir 파라미터로 전달됩니다. 폴더명까지만 입력합니다.
아래와 같이 로컬 file system만 지원합니다.

![2.9.1.checkpoint_dir](https://i.imgur.com/IgEby0L.png)


1) 로컬 파일시스템 절대경로
   file:///home/csle/ksb-csle/{선택한 경로}

### --model_version
새로운 모델의 버전 정보를 전달합니다.


### 그 밖의 파라미터 : DLTrainOperator의 additionalParams

DLTrainOperator의 속성 중 additionalParams에 사용자가 필요한 파라미터의 이름 및 값을 설정합니다.
name 에 파라미터 이름을 입력하고, value 에 전달할 값을 입력합니다.


## 입력파일을 전처리 하는 부분 작성

pandas 라이브러리를 이용하여 입력 데이터셋을 읽어 pandas dataframe을 리턴 받습니다.
```python
def main(argv):
  logging.info('Load dataset')
  df = pd.read_csv(FLAGS.dataset, header=None)

  logging.info('Initialize a DataLoader')
  loader = DataLoader(df)
```

그리고 DataLoader 클래스를 이용하여 입력 데이터를 가공합니다.
prepare_data 함수에서 사용자의 텐서플로우 모델 입력에 적합한 데이터로 가공하는 코드를 작성합니다. 모델의 입력 데이터는 self.inputs 변수로 저장하고, 모델의 출력 데이터는 self.outputs 변수로 저장합니다.
```python
class DataLoader:
  def __init__(self, data):
    """
    DataLoader class.

    Args:
        data (pandas.DataFrame): Data in Pandas DataFrame format.
    """

    self.data = data

    # User-customized data.
    self.inputs = None
    self.outputs = None
    self.prepare_data()

  def prepare_data(self):
    """
    Prepare data for training a graph.

    Returns:
        Input/output data in numpy array.
    """

    # Convert the first column to datetime type.
    self.data.ix[:, 0] = pd.to_datetime(self.data.ix[:, 0])

    # Sort by the date.
    self.data = self.data.sort_values(0)

    # Get 8 rows as input, and set the row three after as output.
    data_np = self.data.values[::, 1:]
    inputs = []
    outputs = []
    for i in range(len(data_np) - 8 - 3):
      input = data_np[i:i + 8].flatten()
      output = data_np[i + 8 + 3].flatten()

      inputs.append(input)
      outputs.append(output)

    inputs = np.array(inputs)
    outputs = np.array(outputs)
    inputs = inputs.reshape(-1, 8, 170)

    self.inputs = inputs
    self.outputs = outputs

  def get_batch(self, batch_size=64, shuffle=True):
    """
    Get batch samples of the shuffled dataset.

    Args:
        batch_size (int): Batch size.
        shuffle (boolean): If shuffle or not.

    Returns:
        Sample of batch size.
    """

    # Shuffle data.
    if shuffle:
      shuffle_index = np.arange(len(self.inputs))
      np.random.shuffle(shuffle_index)

      shuffled_inputs = self.inputs[shuffle_index]
      shuffled_outputs = self.outputs[shuffle_index]
    else:
      shuffled_inputs = self.inputs
      shuffled_outputs = self.outputs

    # Enumerate.
    n_iter = int(shuffled_inputs.shape[0] / batch_size)
    for i in range(n_iter):
      yield shuffled_inputs[i * batch_size: (i + 1) * batch_size], \
            shuffled_outputs[i * batch_size: (i + 1) * batch_size]
```

## 텐서플로우 모델을 학습하고, 학습된 모델을 서빙용으로 export 하는 부분 작성
텐서플로우 모델을 정의하고 학습한 후, 학습된 모델을 텐서플로우 서빙용으로 export 하는 코드를 작성합니다. TensorFlowModel 클래스의 각 함수 부분에 필요한 코드를 작성합니다.

```python
logging.info('Train a model')
model = TensorFlowModel(FLAGS.checkpoint_dir, FLAGS.model_dir,
                        FLAGS.model_version)
model.build_graph()
model.train_graph(loader)
```

```python
class TensorFlowModel():
  def __init__(self, checkpoint_dir, model_dir, model_version):
    """
    Build graph and export a model.
    """
    self.X = None
    self.Y = None
    self.loss = None
    self.train = None
    self.output = None
    self.model_dir = model_dir
    self.model_version = model_version
    self.checkpoint_dir = checkpoint_dir
    self.checkpoint_path = os.path.join(checkpoint_dir, model_version)

  def build_graph(self):
    """
    Build a graph
    """

    # Define inputs and outputs
    seq_length = 8
    num_links = 170
    hidden_size = 128
    learning_rate = 0.01
    self.X = tf.placeholder(tf.float32, shape=[None, seq_length, num_links],
                            name='X')
    self.Y = tf.placeholder(tf.float32, shape=[None, num_links])

    # build a LSTM network
    cell = tf.contrib.rnn.BasicLSTMCell(
      num_units=hidden_size, state_is_tuple=True, activation=tf.tanh)
    outputs, _states = tf.nn.dynamic_rnn(cell, self.X, dtype=tf.float32)
    # We use the last cell's output
    Y_pred = tf.contrib.layers.fully_connected(outputs[:, -1], num_links,
                                               activation_fn=None)

    # cost/loss
    self.loss = tf.reduce_sum(
      tf.square(Y_pred - self.Y))  # sum of the squares

    # optimizer
    optimizer = tf.train.AdamOptimizer(learning_rate)
    self.train = optimizer.minimize(self.loss)

    self.output = Y_pred

  def train_graph(self, loader):
    """
    Train a graph
    """

    saver = tf.train.Saver()

    visible_device_list = "0"
    gpu_options = tf.GPUOptions(visible_device_list=visible_device_list)
    log_device_placement = True
    config = tf.ConfigProto(gpu_options=gpu_options,
                            log_device_placement=log_device_placement)

    with tf.Session(config=config) as sess:
      init = tf.global_variables_initializer()
      sess.run(init)

      # restore from checkpoint file
      ckpt_state = tf.train.get_checkpoint_state(self.checkpoint_dir)
      if ckpt_state is not None:
        latest_ckpt_path = ckpt_state.model_checkpoint_path
        logging.info('Restore model from {}'.format(latest_ckpt_path))
        saver.restore(sess, latest_ckpt_path)

      num_epochs = FLAGS.num_epochs
      num_batch = 10
      for i in range(num_epochs):
        batch_loader = loader.get_batch(batch_size=num_batch, shuffle=False)
        for inputs, outputs in batch_loader:
          _, loss = sess.run([self.train, self.loss],
                             feed_dict={self.X: inputs, self.Y: outputs})
        logging.info('Train lose: {}'.format(loss))

      # checkpoint save
      saver.save(sess, self.checkpoint_path)
      # tensorflow serving model export
      self.export(sess)
      sess.close()

    tf.reset_default_graph()

  def export(self, session):
    """
    Export a model.
    """

    builder = tf.compat.v1.saved_model.builder.SavedModelBuilder(
      self.model_dir)
    signature = tf.compat.v1.saved_model.predict_signature_def(
      inputs={'in1': self.X},
      outputs={'out1': self.output}
    )
    builder.add_meta_graph_and_variables(
      session,
      tags=[tf.compat.v1.saved_model.tag_constants.SERVING],
      signature_def_map={'predict_speed': signature})
    builder.save()
    logging.info('Save model to {}'.format(self.model_dir))
    logging.info('  in1: {}'.format(self.X.name))
    logging.info('  out1: {}'.format(self.output.name))
```

위에서 할당받은 checkpoint_path 위치에 모델 checkpoint 파일을 저장합니다.
```python
saver.save(sess, self.local_checkpoint_file)
```

위에서 할당받은 model_dir 위치에 TensorFlow serving용 모델을 저장합니다. 텐서플로우 그래프의 입력 tensor 이름, 출력 tensor 이름, signature 이름 등을 정의합니다.

```python
# tensorflow serving model export
self.export(sess)
```

```python
def export(self, session):
  """
  Export a model.
  """

  builder = tf.compat.v1.saved_model.builder.SavedModelBuilder(
    self.model_dir)
  signature = tf.compat.v1.saved_model.predict_signature_def(
    inputs={'in1': self.X},
    outputs={'out1': self.output}
  )
  builder.add_meta_graph_and_variables(
    session,
    tags=[tf.compat.v1.saved_model.tag_constants.SERVING],
    signature_def_map={'predict_speed': signature})
  builder.save()
  logging.info('Save model to {}'.format(self.model_dir))
  logging.info('  in1: {}'.format(self.X.name))
  logging.info('  out1: {}'.format(self.output.name))
```

## 학습용 GPU 디바이스 지정하기
사용자의 TensorFlow 파이썬 학습코드가 수행될 때, 특정 GPU 만을 이용해서 학습하도록 하려면 TensorFlow 의 session 을 생성 할 때, GPU를 지정하는 코드를 작성해줘야합니다. 코드의 내용은 다음과 같습니다.

```python
# 0번 GPU를 이용하도록 gpu_options 생성
visible_device_list = "0"
gpu_options = tf.GPUOptions(visible_device_list=visible_device_list)
# Tensor와 Operation이 어떤 디바이스에 배치되었는지 확인하고 싶은 경우 True로 설정
log_device_placement = True
# 위의 옵션을 사용하여 Tensorflow session의 설정을 준비
config = tf.ConfigProto(gpu_options=gpu_options,
                        log_device_placement=log_device_placement)

# 위의 설정을 사용해서 TensorFlow 의 session 을 생성
with tf.Session(config=config) as sess:
```
![2.6.2_03](https://i.imgur.com/oPNs3yG.png)



Atelier Storage 의 trainingcode/traffic_speed 폴더에 GPU 0번을 이용하는 텐서플로우 학습코드가 준비되어 있으니 특정 GPU 를 이용한 워크플로우를 편집하고자 하는 경우 참고하도록 합니다.


## 전체 코드 예제
15분 뒤 교통속도를 예측하는 텐서플로우 모델을 구현한 예제 입니다. 기본적인 LSTM 을 사용하여 구현하였습니다. 프레임워크에서 코드를 연동하여 실행하는 방법은 <a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.6.2.TrafficTraining.md">교통속도예측 텐서플로우 모델 학습하기</a> 를 참고합니다.
main 함수를 포함한 사용자 코드에서 사용하는 라이브러리 파일 등은 동일한 폴더에 위치 시키거나 1 depth의 하위 폴더까지 작성 가능합니다.

```python
import os

import tensorflow as tf
import numpy as np
import pandas as pd

from absl import app, flags, logging

FLAGS = flags.FLAGS
flags.DEFINE_string('dataset', '/tmp/pts/data/dataset.csv', 'Dataset path')
flags.DEFINE_string('checkpoint_dir', '/tmp/pts/ckpts', 'Checkpoint directory')
flags.DEFINE_string('model_version', '0000', 'Model version')
flags.DEFINE_string('model_dir', '/tmp/pts/model/0000', 'Model directory')
flags.DEFINE_bool('is_train', True, 'Training mode')
flags.DEFINE_integer('num_epochs', 3, 'Number of epoch')


class DataLoader:
  def __init__(self, data):
    """
    DataLoader class.

    Args:
        data (pandas.DataFrame): Data in Pandas DataFrame format.
    """

    self.data = data

    # User-customized data.
    self.inputs = None
    self.outputs = None
    self.prepare_data()

  def prepare_data(self):
    """
    Prepare data for training a graph.

    Returns:
        Input/output data in numpy array.
    """

    # Convert the first column to datetime type.
    self.data.ix[:, 0] = pd.to_datetime(self.data.ix[:, 0])

    # Sort by the date.
    self.data = self.data.sort_values(0)

    # Get 8 rows as input, and set the row three after as output.
    data_np = self.data.values[::, 1:]
    inputs = []
    outputs = []
    for i in range(len(data_np) - 8 - 3):
      input = data_np[i:i + 8].flatten()
      output = data_np[i + 8 + 3].flatten()

      inputs.append(input)
      outputs.append(output)

    inputs = np.array(inputs)
    outputs = np.array(outputs)
    inputs = inputs.reshape(-1, 8, 170)

    self.inputs = inputs
    self.outputs = outputs

  def get_batch(self, batch_size=64, shuffle=True):
    """
    Get batch samples of the shuffled dataset.

    Args:
        batch_size (int): Batch size.
        shuffle (boolean): If shuffle or not.

    Returns:
        Sample of batch size.
    """

    # Shuffle data.
    if shuffle:
      shuffle_index = np.arange(len(self.inputs))
      np.random.shuffle(shuffle_index)

      shuffled_inputs = self.inputs[shuffle_index]
      shuffled_outputs = self.outputs[shuffle_index]
    else:
      shuffled_inputs = self.inputs
      shuffled_outputs = self.outputs

    # Enumerate.
    n_iter = int(shuffled_inputs.shape[0] / batch_size)
    for i in range(n_iter):
      yield shuffled_inputs[i * batch_size: (i + 1) * batch_size], \
            shuffled_outputs[i * batch_size: (i + 1) * batch_size]


class TensorFlowModel():
  def __init__(self, checkpoint_dir, model_dir, model_version):
    """
    Build graph and export a model.
    """
    self.X = None
    self.Y = None
    self.loss = None
    self.train = None
    self.output = None
    self.model_dir = model_dir
    self.model_version = model_version
    self.checkpoint_dir = checkpoint_dir
    self.checkpoint_path = os.path.join(checkpoint_dir, model_version)

  def build_graph(self):
    """
    Build a graph
    """

    # Define inputs and outputs
    seq_length = 8
    num_links = 170
    hidden_size = 128
    learning_rate = 0.01
    self.X = tf.placeholder(tf.float32, shape=[None, seq_length, num_links],
                            name='X')
    self.Y = tf.placeholder(tf.float32, shape=[None, num_links])

    # build a LSTM network
    cell = tf.contrib.rnn.BasicLSTMCell(
      num_units=hidden_size, state_is_tuple=True, activation=tf.tanh)
    outputs, _states = tf.nn.dynamic_rnn(cell, self.X, dtype=tf.float32)
    # We use the last cell's output
    Y_pred = tf.contrib.layers.fully_connected(outputs[:, -1], num_links,
                                               activation_fn=None)

    # cost/loss
    self.loss = tf.reduce_sum(
      tf.square(Y_pred - self.Y))  # sum of the squares

    # optimizer
    optimizer = tf.train.AdamOptimizer(learning_rate)
    self.train = optimizer.minimize(self.loss)

    self.output = Y_pred

  def train_graph(self, loader):
    """
    Train a graph
    """

    saver = tf.train.Saver()

    with tf.Session() as sess:
      init = tf.global_variables_initializer()
      sess.run(init)

      # restore from checkpoint file
      ckpt_state = tf.train.get_checkpoint_state(self.checkpoint_dir)
      if ckpt_state is not None:
        latest_ckpt_path = ckpt_state.model_checkpoint_path
        logging.info('Restore model from {}'.format(latest_ckpt_path))
        saver.restore(sess, latest_ckpt_path)

      num_epochs = FLAGS.num_epochs
      num_batch = 10
      for i in range(num_epochs):
        batch_loader = loader.get_batch(batch_size=num_batch, shuffle=False)
        for inputs, outputs in batch_loader:
          _, loss = sess.run([self.train, self.loss],
                             feed_dict={self.X: inputs, self.Y: outputs})
        logging.info('Train lose: {}'.format(loss))

      # checkpoint save
      saver.save(sess, self.checkpoint_path)
      # tensorflow serving model export
      self.export(sess)
      sess.close()

    tf.reset_default_graph()

  def export(self, session):
    """
    Export a model.
    """

    builder = tf.compat.v1.saved_model.builder.SavedModelBuilder(
      self.model_dir)
    signature = tf.compat.v1.saved_model.predict_signature_def(
      inputs={'in1': self.X},
      outputs={'out1': self.output}
    )
    builder.add_meta_graph_and_variables(
      session,
      tags=[tf.compat.v1.saved_model.tag_constants.SERVING],
      signature_def_map={'predict_speed': signature})
    builder.save()
    logging.info('Save model to {}'.format(self.model_dir))
    logging.info('  in1: {}'.format(self.X.name))
    logging.info('  out1: {}'.format(self.output.name))


def main(argv):
  logging.info('Load dataset')
  df = pd.read_csv(FLAGS.dataset, header=None)

  logging.info('Initialize a DataLoader')
  loader = DataLoader(df)

  if FLAGS.is_train:
    logging.info('Train a model')
    model = TensorFlowModel(FLAGS.checkpoint_dir, FLAGS.model_dir,
                            FLAGS.model_version)
    model.build_graph()
    model.train_graph(loader)
  else:
    pass


if __name__ == '__main__':
  app.run(main)
```

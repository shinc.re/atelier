
# 텐서플로우 모델 주기적으로 학습하기 (HourlyTensorflowTraining)

매분, 매시간, 매일, 매주, 매월, 매년 등 주기적으로 파일로부터 데이터셋을 읽어 딥러닝 학습을 수행하고, 그 결과로 새롭게 학습된 모델의 신규버전이 담긴 디렉토리와 파일을 생성하는 예제입니다.

## 입력 데이터 준비하기
본 예제에서는 Atelier WebToolKit 화면에서 **Storage** 탭을 클릭하여 dataset/input/ 디렉토리를 선택합니다. 해당 디렉토리에 traffic_processing.csv 파일이 있는지 확인합니다.
만약 없다면 /home/csle/ksb-csle/examples/input 폴더에 존재하는 traffic_processing.csv 파일을 사용자 HDFS 의 dataset/input/traffic 폴더에 등록하여 사용합니다.

![2.5.10_DataRepository](https://i.imgur.com/e3e6IJS.png)

## 사용자 파이썬 코드 준비하기
텐서플로우 기반 딥러닝 모델이 구현된 사용자 파이썬 코드가 있어야 합니다. 사용자 파이썬 코드는 프레임워크와 연동하기 위해 프레임워크에서 전달하는 파라미터를 받는 부분, 학습된 모델을 텐서플로우 서빙용으로 export 하는 부분 등이 구현되어야 합니다. 자세한 내용은 Atelier Framework와 연동을 위한 tensorflow 학습코드 작성하기 를 참고합니다.
main 함수를 포함한 사용자 파이썬 코드에서 사용하는 라이브러리 파일 등은 동일한 폴더에 위치 시키거나 1 depth의 하위 폴더까지 작성 가능합니다.


## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | HourlyTensorflowTraining | 워크플로우 이름     
description  | 텐서플로우 기반 딥러닝 모델을 주기적으로 학습하는 예제  | 워크플로우를 설명하는 글
isBatch  | true | 배치 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 반복실행 |   
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름

딥러닝 모델을 주기적으로 생성하기 위해 **RUNTYPE** 을 **반복실행** 으로 설정합니다. **주기** 는 한번, 매분, 매시간, 매일, 매주, 매월, 매년 중에 하나로 설정합니다. 본 예제에서는 테스트를 위해 **매분** 으로 설정합니다. 매분은 테스트용으로 5분 단위로 실행됩니다.

### 엔진 선택
본 예제는 텐서플로우를 이용하여 배치 형태로 딥러닝 모델을 생성하는 워크플로우이므로 **External** 엔진을 선택합니다.


- 엔진 속성

순번  | 엔진 Type | NickName  | 설명
--|---|---|--
1  | External  | TrainEngine    | 텐서플로우 모델 학습

#### Reader
**FileBatchReader** 를 선택하고 아래표와 같은 속성을 지정합니다. path 에 입력 데이터셋 파일의 경로를 지정합니다. 이 값은 사용자 파이썬 코드(텐서플로우 모델을 정의하고 학습하는 코드)의 `--dataset` 파라미터로 전달됩니다.

field  |value   | 설명
--|---|--
path |  dataset/input/traffic/traffic_processing.csv |  데이터셋 파일 경로
fileType  |  CSV |  파일 타입
delimiter  |  , |  구분자
header  |  false |  header 포함 유무
saveMode  |   | 사용 하지 않음  


path 는 경로 입력창 옆의 "File" 버튼을 클릭하여 아래 그림과 같이 Stroage 탐색창을 통해 파일을 선택하여 입력할 수 있습니다.


#### Writer
**FileBatchWriter** 를 선택하고 아래표와 같은 속성을 지정합니다. path 에 학습한 딥러닝 모델이 최종 저장될 폴더를 지정합니다. 해당 폴더에 자동으로 최신버전의 폴더를 생성한 후 모델이 저장됩니다. 모델의 버전은 0001 부터 시작하여 1씩 증가합니다. path 에 입력한 값은 사용자 파이썬 코드(텐서플로우 모델을 정의하고 학습하는 코드)의 `--model_dir` 파라미터로 전달됩니다.

본 예제에서는 다른 엔진에서 학습한 딥러닝 모델을 사용하도록 하기 위해 HDFS 절대경로를 지정합니다. 상대경로를 지정할 경우, 동적으로 생성한 **사용자 기본 파일경로** 가 앞에 추가되므로 다른 엔진에서 정확한 파일 경로를 지정하는 것이 어렵습니다.   

field  |value   | 설명
--|---|--
path  |  model/periodic/ |  모델 저장 경로
fileType  |  CSV |  파일 타입
delimeter  |  , |  구분자  
header  |  false |  header 포함 여부
saveMode  |   | 사용하지 않음

#### Controller
텐서플로우와 같은 외부시스템를 제어하기 위한 **TensorflowTrainController** 를 선택합니다.

#### Runner
**TensorflowRunner** 를 선택하고 아래표와 같은 속성을 지정합니다. path 에 main 함수를 포함하는 사용자 파이썬 코드(텐서플로우 모델을 정의하고 학습하는 코드)의 경로 및 파일명을 지정합니다.

field  |value   | 설명
--|---|--
path  |  trainingcode/traffic_speed/train.py |  텐서플로우 모델 학습을 위한 파이썬 코드 경로
cluster  |  false |  cluster 환경 실행 여부  
inJson  |  false |  Json 형태 파라미터 전달 여부
tfVersion  |  r1.6 |  텐서플로우의 버전


#### Operator
**DLTrainOperator** 를 선택하고 아래표와 같은 속성을 지정합니다. path 에 모델의 체크포인트를 저장할 경로를 지정합니다. 이 값은 사용자 파이썬 코드(텐서플로우 모델을 정의하고 학습하는 코드)의 `--checkpoint_dir` 파라미터로 전달됩니다. 로컬 파일시스템 절대경로로 입력합니다.

field  |value   | 설명
--|---|--
path  | file:///tmp/periodic/ckpts |  모델의 체크포인트를 저장할 경로 <br> 반드시 로컬 시스템 경로만 지정
addtionalParams  | 아래 설정 참고  |

additionalParams 설정은 [+] 버튼을 두 번 클릭하여 다음과 같이 합니다. 사용자가 필요한 파라미터의 이름과 값을 세팅하여 사용자 파이썬 코드로 전달합니다.

field  |value   | 설명
--|---|--
name  | is_train  | 새롭게 학습할 경우에는 True, 그 외의 경우에는 False.
value  | True  |  

field  |value   | 설명
--|---|--
name  | num_epochs  |  학습 반복 횟수
value  | 2  |


![2.5.10_01](https://i.imgur.com/aUJyYGU.png)


<br>
<br>

![2.5.10_02](https://i.imgur.com/SeCiUAt.png)


## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.


### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.





### 워크플로우 모니터링 하기
웹툴킷 상단 메뉴의 Monitoring 탭을 클릭하면 Workflow 탭이 선택되어있습니다. Workflow 탭에서는 실행한 워크플로우들의 목록 및 동작 상태를 확인하고 제어할 수 있습니다. 위에서 실행한 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.

![2.5.10_03](https://i.imgur.com/eJ4lOGe.png)

주기적으로 (5분 마다) 실행되므로, 동작 상태(status) 가 완료(Completed)였다가 실행 중일 때는 실행 중(Running) 으로 바뀌는 것을 확인할 수 있습니다.


WorkFlow History 탭을 선택하면, 프레임워크에서 워크플로우가 동작하며 발생시킨 로그 정보를 확인 할 수 있습니다. 본 예제에서는 엔진이 5분 마다 동작한 내역을 확인할 수 있습니다.

![2.5.10_04](https://i.imgur.com/bP6Eedg.png)


### 학습한 후 export 된 모델 확인하기  
엔진의 FilebatchWriter 에서 지정한 위치 (model/periodic/) 에 학습된 모델이 생성된 것을 확인합니다. Storage 메뉴에서 model/periodic/ 위치에 엔진이 5분 마다 실행되어 모델을 주기적으로 생성한 것을 확인할 수 있습니다. 모델의 버전은 0000 부터 자동으로 1씩 증가합니다.

![2.5.10_05](https://i.imgur.com/kMq3iEw.png)


## 워크플로우 종료하기
Monitoring 탭에서 동작중(Inprogress)인 상태의 HourlyTensorflowTraining 워크플로우의 정지(<span style="color:red">&#9724;</span>) 버튼을 눌러 종료시킵니다.  

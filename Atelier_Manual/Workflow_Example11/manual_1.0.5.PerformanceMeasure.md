
# 모델 성능 측정하기 (PerformanceMeasureExample)

본 예제에서는 학습 모델의 성능을 측정하는 방법을 보여줍니다. 일반적으로 Binary Classification 작업인 경우에는 areaUnderPR, areadUnderROC metric을, Multi-class Clasisfication 작업에는 fmeasure, weightPrecision, weightedRecall, accuracy metric을, Regression 작업인 경우에는 rmse, mse, r2, mae metric을 사용합니다.

- PR: Precision Recall curve
- ROC: Receiver Operating Characteristics curve
- Accuracy (정확도): 정답을 맞춘 비율로 예측된 값이 정답과 얼마나 같은지를 나타내는 비율입니다.
- Precision (정밀도): true positive / total positive, 양성이라고 판단한 전체 중에 진짜 양성의 비율로, 예측한 것 중에 정답의 비율이 얼마나 되는지 나타냅니다.
- Recall (재현율): true positive / real positive, 진짜 양성 중에 양성이라고 올바르게 판단내린 비율로, 찾아야할 것 중에 실제로 찾은 비율을 나타냅니다.  
- FMeasure: 2 * (precision * recall / precision + recall), 정밀도와 재현율의 조화 평균 정도로 생각하면 됩니다.
- RMSE (Root Mean Square Error): 예측값과 실제값의 평균 제곱근 편차입니다.
- MSE (Mean Square Error): 예측값과 실제값의 평균 제곱 오차입니다.
- R2 (r-square): 1 - (추정 모형의 MSE / 평균 관측 값의 MSE)
- MAE (Mean Absolute Error): 예측값과 실제값 차이의 평균값입니다.

다양한 성능 측정 metric을 확인하기 위해 본 예제에서는 다음의 3가지 학습 모델을 사용하여 엔진을 구성합니다.
- GeneralizedLinearRegression 오퍼레이터를 이용하여 regression 모델을 만듭니다.
- LogisticRegressor 오퍼레이터를 이용하여 다중 분류 모델을 만듭니다.
- LinearSvcClassifier 오퍼레이터를 이용하여 이진 분류 모델을 만듭니다.

## 입력 데이터 준비하기

### 데이터 로딩하기

실행을 위해서 Host PC의 /home/csle/ksb-csle/examples/input 폴더에 존재하는 `boston.csv` 파일을 웹툴킷을 이용하여 사용자 Storage dataset/input 폴더에 등록합니다.

### 데이터 셋 설명하기

보스턴 주택 가격 데이터 셋은 주택의 가격에 영향을 주는 여러가지 요건들과 가격 정보가 포함되어 있습니다.

속성 | 설명
--|---
CRIM | 자치시(town) 별 1인당 범죄율
ZN | 25,000 평방피트를 초과하는 거주지역의 비율
INDUS | 비소매상업지역이 점유하고 있는 토지의 비율  
CHAS | 찰스강에 대한 더미변수(강의 경계에 위치한 경우는 1, 아니면 0)  
NOX | 10ppm 당 농축 일산화질소
RM | 주택 1가구당 평균 방의 개수
AGE | 1940년 이전에 건축된 소유주택의 비율
DIS | 5개의 보스턴 직업센터까지의 접근성 지수
RAD | 방사형 도로까지의 접근성 지수
TAX | 10,000 달러 당 재산세율
PTRATIO | 자치시(town)별 학생/교사 비율  
B | 1000(Bk-0.63)^2, 여기서 Bk는 자치시별 흑인의 비율을 말함.  
LSTAT | 모집단의 하위계층의 비율(%)
MEDV | 본인 소유의 주택가격(중앙값) (단위: $1,000)  

본 예제에서는 주택 가격에 영향을 미치는 다양한 요소를 입력 받아 주택 가격 'MEDV'를 예측 및 분류하는 3개의 학습 모델을 만들고 각각의 모델에 대해 성능을 측정합니다.

## 워크플로우 생성하기

워크플로우 편집화면을 이용하여 학습 모델 성능을 측정하는 워크플로우를 아래와 같이 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | 모델 성능 측정 예제 | 워크플로우 이름     
description  | 학습 모델의 성능을 측정하는 예제입니다.  | 워크플로우를 설명
isBatch | true | 배치 처리를 하는 워크플로우로 true로 지정
verbose | true | 디버깅을 위해 로그정보를 보고자할 경우 true로 지정
RunType | 즉시실행 | 실행 타입
Project | Spark ML 기계학습 | 워크플로우의 프로젝트 이름

### 엔진 선택

Batch 엔진, MiniBatch 엔진을 사용할 수 있으며, 여기서는 Batch 엔진을 사용하겠습니다.

- 엔진 속성

순번  | 엔진 Type | NickName  
--|---|---
1  | Batch  | PerformanceMeasureEngine  

엔진에는 어떤 데이터를 읽어와 (reader) 어떤 방법으로 처리하여 (operator) 그 결과를 어디에 저장할 것인지 (writer), 그리고 위의 전체적인 작업을 어떤 식으로 제어하며 (controller) 어떻게 구동할 것인지 (runner) 등의 정보를 입력해야 합니다.

#### Reader

Atelier **Stroage** 에 등록된 주택 가격에 영향을 미치는 다양한 정보가 담겨있는 `boston.csv` 파일을 읽어들입니다. FileBatchReader를 드래그앤드랍한 후 아래와 같이 속성을 지정해줍니다.

field  |value   | 설명
--|---|--
filePath  | dataset/input/boston.csv |  입력 파일 경로를 입력합니다.
fileType  |  CSV |  읽을 파일의 타입을 입력합니다.
delimiter  |  , |  컬럼간 구분자를 지정합니다.  
header  |  true |  파일의 header 포함 유무를 입력합니다.
field  | 입력없음  | 컬럼의 정보를 입력합니다 (데이터 내 헤더 정보가 포함되어 있기에 여기선 입력을 안합니다).  
saveMode  | OVERWRITE |  Writer에서 사용되는 옵션으로 Reader에서는 사용되지 않습니다.

#### Writer

모델에 대한 성능 측정 결과를 저장하기 위해 FileBatchWriter를 선택하고 아래와 같이 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  | output/ml/regression.csv |  결과 파일의 저장경로를 입력합니다.
fileType  |  CSV |  저장파일의 타입을 입력합니다.
delimiter  |  , |  컬럼간 구분자를 지정합니다.  
header  |  true |  header 정보를 포함해서 저장할건지 그 유무를 입력합니다.
saveMode  | OVERWRITE  |  파일 저장 방식을 선택합니다.

#### Controller

Spark기반 데이터 처리 기법 및 Spark ML을 활용하여 모델을 생성합니다. Spark 환경에서 사용되는 SparkSessionController를 선택합니다.

#### Runner

SimpleSparkRunner를 선택합니다.

field  |value   | 설명
--|---|--
master |  local [ * ] |  Spark local 모드를 입력합니다.
numExecutors  | 2 |  Spark의 executor 갯수를 입력합니다.
executorCores  | 2  |  Spark의 현재 할당된 코어수를 입력합니다.
executerMemory  |  1g |  Spark의 executor 메모리 정보를 입력합니다.
driverMemory  |  1g |  Spark 드라이버에 할당할 메모리 정보를 입력합니다.
totalCores  |  2 |  Spark의전체 할당 가능한 코어수를 입력합니다.
sparkVersion  | 2.4.2 |  Spark 버전 정보를 입력합니다.

#### Operator

본 예제에서는 주택 가격에 영향을 미치는 다양한 요소를 입력 받아 주택 가격 'MEDV'를 예측 및 분류하는 3개의 학습 모델을 만들고 각각의 모델에 대해 성능을 측정합니다. 다음의 역할을 수행하는 5개의 오퍼레이터가 붙습니다.

- 주택 가격 'MEDV'를 예측하는 회귀 분석 모델을 만듭니다 (GeneralizedLinearRegression 오퍼레이터)
- 'MEDV' 컬럼의 continuous 데이터를 3개의 구분된 값을 갖는 categorical 데이터로 변경하여 newMedv이라는 컬럼으로 추가합니다 ( AddConditionalColumn 오퍼레이터)
- 주택 가격 'MEDV'를 분류하는 다중 분류 모델을 만듭니다 (LogisticRegressor 오퍼레이터)
- 'MEDV' 컬럼의 continuous 데이터를 2개의 구분된 값을 갖는 categorical 데이터로 변경하여 binMedv이라는 컬럼으로 추가합니다 ( AddConditionalColumn 오퍼레이터)
- 주택 가격 'MEDV'를 분류하는 이진 분류 모델을 만듭니다 (LinearSvcClassifier 오퍼레이터)

각각의 오퍼레이터를 아래와 같이 설정합니다.

1. **GeneralizedLinearRegressorOperator**

GeneralizedLinearRegressorOperator 끌어놓은 후 세부 설정 화면에서 trainInfo 버튼을 클릭합니다. 학습을 수행하기 위한 기본적인 정보를 아래와 같이 입력합니다.

field  |value   | 설명
--|---|--
labelColumnName |  medv |  주택 가격 값인 'medv'를 예측하는 학습 모델입니다.
featureColumnNames  | * |  'medv'를 제외한 모든 컬럼을 Feature로 사용합니다.
\_path  | /user/biuser/model/boston  |  개인 계정의 model/boston/ 밑에 학습된 모델을 저장합니다.
numFolds  |  3 |  3-fold cross-validation을 수행합니다.
testSplitRatio  |  0.2 |  test 데이터 셋은 전체 데이터 셋의 20%만 사용합니다.
metric  |  rmse |  모델의 성능은 rmse (Root Mean Square Error) 메트릭을 사용합니다.
mlType  | REGRESSION |  만들고자 하는 학습 모델은 Regression 작업을 수행하기 위한 모델입니다.

Generalized Linear Regressor모델의 세부 파라미터를 아래의 그림을 참조하여 설정합니다 (파라미터 튜닝에 대한 자세한 설명은 Atelier Spark ML 이해하기를 참조바랍니다).

![1.0.5_glr](https://i.imgur.com/ThGGBmS.png)

이 오퍼레이터가 실행되면 base path인 /user/biuser/model/boston 밑에 생성된 Generalized Linear Regression 모델이 저장되게 됩니다.

2. **AddConditionalColumnOperator**

다음으로 LogisticRegressor를 이용한 다중 분류 모델을 만들고자 합니다. 다중 분류 모델을 만들기 위해서는 Label 정보가 continuous 데이터가 아닌 categorical 형태의 데이터로 변형되어야 합니다. 웹 툴킷에서 AddConditionalColumnOperator를 엔진에 끌어놓은 후 newColumnName에 'newMedv'를 입력하고 'rules'버튼 클릭 후 아래와 같이 설정합니다.
- 'medv' 컬럼 값이 10.0보다 작은 경우 그 값을 0으로 변경합니다.
- 'medv' 컬럼 값이 10.0보다 크고 20.0보다 작은 경우 그 값을 10으로 변경합니다.
- 'medv' 컬럼 값이 20.0보다 크고 30.0보다 작은 경우 그 값을 20으로 변경합니다.
- 'medv' 컬럼 값이 30.0보다 큰 경우 그 값을 30으로 변경합니다.

![1.0.5_operator1](https://i.imgur.com/vx4Ut60.png)

위와 같이 설정하면 0, 10, 20, 30의 값을 갖는 'newMedv'라는 새로운 컬럼이 추가됩니다.

3. **LogisticRegressionClassifierOperator**

LogisticRegression 모델은 'medv'가 아닌 'newMedv' 컬럼을 Label 컬럼으로 사용합니다. 웹 툴킷에서 LogisticRegressionClassifierOperator 끌어놓은 후 세부 설정 화면에서 trainInfo 버튼을 클릭합니다. 학습을 수행하기 위한 기본적인 정보를 아래와 같이 입력합니다.

field  |value   | 설명
--|---|--
labelColumnName |  newMedv |  3개의 category로 분류된 'newMedv'컬럼을 레이블 정보로 사용합니다.
featureColumnNames  | crim,zn,indus,chas,nox,rm,age,<br>dis,rad,tax,ptratio,b,lstat |  'medv', 'newMedv'를 제외한 모든 컬럼을 Feature로 사용합니다.
\_path  | /user/biuser/model/boston  |  개인 계정의 model/boston/rf 밑에 학습된 모델을 저장합니다.
numFolds  |  5 |  5-fold cross-validation을 수행합니다.
testSplitRatio  |  0.2 |  test 데이터 셋은 전체 데이터 셋의 20%만 사용합니다.
metric  |  accuracy |  모델의 성능은 정확도 메트릭을 사용합니다.
mlType  | MULTICLASS_CLASSIFICATION |  다중 분류 작업 모델입니다.

Logistic Regression모델의 세부 파라미터를 아래의 그림을 참조하여 설정합니다 (파라미터 튜닝에 대한 자세한 설명은 Atelier Spark ML 이해하기를 참조바랍니다).

![1.0.5_lr](https://i.imgur.com/1TE2uAl.png)

이 오퍼레이터가 실행되면 base path인 /user/biuser/model/boston 밑에 생성된 Logistic Regression 모델이 저장되게 됩니다.

4. **AddConditionalColumnOperator**

다음으로 Linear SVC 알고리즘을 이용한 이진 분류 모델을 만들고자 합니다. 이진 분류 모델을 만들기 위해서 Label 정보를 이진화된 형태의 값으로 변형해 줍니다. 웹 툴킷에서 AddConditionalColumnOperator를 엔진에 끌어놓은 후 newColumnName에 'binMedv'를 입력하고 'rules'버튼 클릭 후 아래와 같이 설정합니다.
- 'medv' 컬럼 값이 20.0보다 작은 경우 그 값을 0으로 변경합니다.
- 'medv' 컬럼 값이 20.0보다 큰 경우 그 값을 20으로 변경합니다.

![1.0.5_operator2](https://i.imgur.com/yZkEXY5.png)

위와 같이 설정하면 0, 20의 이진화된 값을 갖는 'binMedv'라는 새로운 컬럼이 추가됩니다.

5. **LinearSVCClassifierOperator**

LinearSVC 모델은 'medv'가 아닌 이진화된 'binMedv' 컬럼을 Label 컬럼으로 사용합니다. 웹 툴킷에서 LinearSVCClassifierOperator 끌어놓은 후 세부 설정 화면에서 trainInfo 버튼을 클릭합니다. 학습을 수행하기 위한 기본적인 정보를 아래와 같이 입력합니다.

field  |value   | 설명
--|---|--
labelColumnName |  binMedv |  이진화된 'binMedv'컬럼을 레이블 정보로 사용합니다.
featureColumnNames  | crim,zn,indus,chas,nox,rm,age,<br>dis,rad,tax,ptratio,b,lstat |  'medv', 'newMedv', 'binMedv'를 제외한 모든 컬럼을 Feature로 사용합니다.
\_path  | /user/biuser/model/boston  |  개인 계정의 model/boston/rf 밑에 학습된 모델을 저장합니다.
numFolds  |  5 |  5-fold cross-validation을 수행합니다.
testSplitRatio  |  0.2 |  test 데이터 셋은 전체 데이터 셋의 20%만 사용합니다.
metric  |  areaUnderPR |  모델의 성능은 PR (Precision Recall) curve 메트릭을 사용합니다.
mlType  | BINARY_CLASSIFICATION |  이진 분류 작업 모델입니다.

Linear Support Vector Machine 모델의 세부 파라미터를 아래의 그림을 참조하여 설정합니다 (파라미터 튜닝에 대한 자세한 설명은 Atelier Spark ML 이해하기를 참조바랍니다).

![1.0.5_svc](https://i.imgur.com/VGwiu5O.png)

이 오퍼레이터가 실행되면 base path인 /user/biuser/model/boston 밑에 생성된 Linear SVC 모델이 저장되게 됩니다.

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![1.0.5_build](https://i.imgur.com/hk11TeT.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

### 워크플로우 모니터링 하기
Atelier WebToolKit 상단 "Monitoring" 메뉴의 "Workflow" 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다. 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.

### 결과 파일 확인하기
본 예제에서는 GeneralizedLinearRegression, LogisticRegressor, LinearSvcClassifier 이렇게 3가지 학습 모델을 만들었습니다. "Repository" 탭에서 3개의 학습 모델이 생성되었는지 확인합니다. 적용된 학습 모델 알고리즘과 그 때 성능 정보는 history.csv 파일을 열어 확인할 수 있습니다.

![1.0.5_repository](https://i.imgur.com/rRUCGl4.png)

아래는 history.csv 파일 정보를 보여줍니다.
- 첫 번째 모델은 GeneralizedLinearRegression이 적용된 회귀 분석 모델로 RMSE, MSE, R2, MAE 성능 메트릭에 따른 성능 결과를 확인할 수 있습니다.
- 두 번째 모델은 LogisticRegression이 적용된 다중 분류 모델로 accuracy, fmeasure, weightedPrecision, weightedRecall 성능 메트릭에 따른 모델 성능 결과를 확인할 수 있습니다.
- 세 번째 모델은 LinearSvcClassifier이 적용된 이진 분류 모델로 areaUnderPR curve, areadUnderROC curve 성능 메트릭에 따른 모델 성능 결과를 확인할 수 있습니다.

![1.0.5_result2](https://i.imgur.com/300qhUw.png)

최종적으로 out/ml/measure.csv 파일을 확인합니다. 이 파일에는 feature에 따른 각 모델들의 추론 결과가 함께 출력되어 있습니다.
- GeneralizedLinearRegression 알고리즘을 적용한 회귀 분석 결과가 'glm_prediction'이라는 새로운 컬럼으로 추가되어 있음을 확인할 수 있습니다.
- LogisticRegression 알고리즘을 적용한 다중 분류 결과가 'lr_prediction(newMedv)'라는 새로운 컬럼으로 추가되어 있음을 확인할 수 있습니다.
- Linear support vector machine 알고리즘을 적용한 이진 분류 결과가 'svc_prediction(binMedv)'라는 새로운 컬럼으로 추가되어 있음을 확인할 수 있습니다.

![1.0.5_result1](https://i.imgur.com/LbPWR1S.png)

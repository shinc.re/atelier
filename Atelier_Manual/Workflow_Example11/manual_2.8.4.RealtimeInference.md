
# 실시간 추론하기

## 전체 시나리오 개요 (Revisit)

실시간 시계열 데이터 기반의 모델 검증 및 평가 시나리오는 실시간 수집되는 교통 속도를 활용하여 향후 15분 후의 교통 속도를 예측하는 시나리오로, 데이터 수집부터 모델 학습 및 재학습, 15분 후의 교통 속도 추론, 그리고 재학습된 모델의 성능 검증 및 실시간 모델 평가 과정 이렇게 총 5개의 workflow로 구성됩니다.

- 워크플로우 1: 시계열 데이터를 수신하여 실시간으로 정제 및 처리합니다. 처리된 데이터는 학습 데이터 DB에 실시간으로 저장되는 한편 실시간 추론을 위해 워크플로우 3으로 전송됩니다.
- 워크플로우 2: 모델을 생성하기 위한 학습 데이터를 학습 데이터 DB에서 가져와 구성하고 RNN 딥러닝 모델을 생성합니다. 또한 설정된 주기로 재학습 과정을 수행합니다.  
- 워크플로우 3: RNN 딥러닝 모델로 워크플로우 1에 의해 전송받은 데이터에 대해 추론 과정을 수행합니다. 추론 결과는 사용자에게 kafka로 전송되는 한편 향후 모델의 실시간 성능 평가를 위해 평가 데이터 DB에 실시간 저장됩니다.
- 워크플로우 4: 재학습된 RNN 딥러닝 모델을 사용하기에 앞서 모델의 성능이 좋은지 여부를 실제 데이터를 활용하여 검증합니다. 검증에 사용하는 데이터는 학습 데이터 DB에서 가져와 검증용 데이터로 재구성합니다.
- 워크플로우 5: 평가 데이터 DB에서 평가 데이터를 구성하여 현재 서비스에 활용되고 있는 RNN 딥러닝 모델의 추론 성능 결과를 실시간으로 감시합니다.

## 워크플로우 생성하기

본 워크플로우는 실시간 추론 결과를 kafka로 전송하는 한편 모델 성능 평가를 위해 '평가데이터 DB'에 저장하는 워크플로우입니다. 아래의 그림에서 보듯 총 2개의 엔진으로 구성되어 있습니다.
- 첫 번째 엔진은 워크플로우 1에서 전송된 데이터로 실시간 모델 추론 작업을 수행하고 그 결과를 사용자에게 kafka로 전송하는 엔진입니다.
- 두 번째 엔진은 사용자에게 전송되는 kafka 데이터를 같이 subscribe하여 '평가 데이터 DB'에 저장하는 엔진입니다. 이 데이터 DB는 현재 서비스에 활용 중인 학습 모델의 추론 성능을 평가하는 데 사용됩니다.

![2.8.1_rtInference](https://i.imgur.com/eXkbeTX.png)

여기서 중요한 점은 실시간 추론 과정 수행시, 워크플로우 1에서 넘어온 데이터를 변경없이 그대로 사용한다라는 것입니다 (간단히 컬럼 이름을 바꾼다거나 vector포맷의 문자열을 다시 vector 타입으로 변경하는 작업만 수행합니다). 워크플로우 1에서 넘어온 데이터가 추론 뿐만 아니라 학습 데이터로도 사용된다는 점을 고려할 때, 생성된 학습 모델을 바로 서비스로 deploy해도 추론 성능은 학습 정확도 성능과 거의 유사함을 쉽게 유추할 수 있습니다.

편집화면에서 워크플로우를 작성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | 2.8.4.실시간 추론하기 |   워크플로우 이름
description  | 강남 교통 예측 데이터 실시간 Inference 워크플로우 |  워크플로우를 설명하는 글
isBatch  | false | 스트림 처리를 하는 워크플로우 이므로, false 로 지정
verbose  | true | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |   
Project | 시계열 예측서비스 자동 평가 시나리오 |   워크플로우가 소속될 프로젝트 이름

###  첫 번째 엔진 생성하기

첫 번째 엔진은 워크플로우 1에서 전송된 데이터로 추론 작업을 수행하고 그 결과를 kafka로 전송하는 엔진입니다. 이를 위해 **MiniBatch** 엔진을 선택합니다.

- 엔진 속성

순번  | 엔진 Type | NickName  | 설명
--|---|---|---
1  | MiniBatch  | Inference  | 실시간 추론과정을 수행합니다.

#### Reader

워크플로우 1에서 실시간 전송되는 데이터를 받을 수 있게 **KafkaReader** 를 선택하고 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
bootStrapServers  | csle1:9092 |  Kafka 접속 주소(IP, 포트번호)를 입력합니다.
zooKeeperConnect  | csle1:2181 |  zookeeper의 접속주소(IP, 포트번호)를 입력합니다.
groupId  | kangnam1  |  Topic 그룹 ID를 입력합니다.
topic  | kangnam_engineering1  |  Kafka Topic 이름을 설정합니다.

#### Writer

모델 추론 결과는 kafka를 통해 사용자에게 보내집니다.  **KafkaWriter** 를 선택하고 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
bootStrapServers  | csle1:9092 |  Kafka 접속 주소(IP, 포트번호)를 입력합니다.
zooKeeperConnect  | csle1:2181 |  zookeeper의 접속주소(IP, 포트번호)를 입력합니다.
groupId  | kangnam1  |  Topic 그룹 ID를 입력합니다.
topic  | kangnam_inference1  |  Kafka Topic 이름을 입력합니다.

#### Controller

기본 **SparkStreamController** 를 선택합니다.

#### Runner

기본 **SimpleSparkRunner** 를 사용합니다.

#### Operator

실시간 추론을 위해 총 5개의 Operator를 사용합니다. 데이터를 조작하는 오퍼레이터가 아닌 컬럼 이름을 바꾼다거나 vector포맷의 문자열을 다시 vector 타입으로 변경하는 간단한 작업만 수행하는 오퍼레이터들입니다.

1. **ColumnSelectOperator**

field  |value   | 설명
--|---|--
selectedColumnName  | PRCS_DATE, input, input_shifted+00001, <br> input_shifted+00002, input_shifted+00003, <br> input_shifted+00004, input_shifted+00005, <br> input_shifted+00006, input_shifted+00007 |  컬럼명을 입력합니다.

2. **VectoredStringToVectorOperator**

교통예측을 위한 RNN기반 텐서플로우 모델은 과거 35분 동안의 170개 도로에 대한 교통속도를 입력받아 향후 15분 후의 170개 도로에 대한 교통 속도를 예측하는 모델입니다. 즉, 170개 도로에 대한 35분 간의 교통속도 내역 데이터가 하나의 행으로 변형되어 모델의 입력으로 들어가야 합니다.

input 컬럼에는 과거 35분전의 170개 도로에 대한 교통 속도가 벡터 포맷의 문자열 형태로 저장되어 있습니다. 그리고 input_shifted+00001에는 과거 30분전의 170개 도로에 대한 교통 속도가, input_shifted+00002에는 과거 25분전의 교통 속도가 저장되어 있습니다. 추론 데이터 구성을 위해 과거 35분 전부터 현재까지의 교통 속도 정보들을 하나의 행에 다 넣어줘야 합니다. 이를 위해 VectoredStringToVectorOperator을 끌어놓습니다.

field  |value   | 설명
--|---|--
columnName  | input | 컬럼 이름이 'input'으로 시작하는 모든 컬럼을 선택합니다.
dropOriginal  | false  |  원래 데이터는 삭제하지 않고 그대로 둡니다.
newName  | in1  | 선택된 컬럼들을 하나의 vector로 변경한 후 'in1'이라는 컬럼으로 추가합니다.

텐서플로우 모델은 여기서 새롭게 생성된 in1 컬럼의 데이터만을 활용하여 추론 과정을 수행하게 됩니다.

3. **TensorflowPredictOperator**

추론 과정을 수행하기 위해 TensorflowPredictOperator를 끌어다 놓은 후 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
path  | hdfs://csle1:9000/user/biuser/<br>model/serving/kangnam1 | 서비스에 활용할 학습 모델이 저장되어 있는 서빙 모델 저장소 위치를 입력합니다.
name  | kangnam_traffic  |  학습 모델의 이름을 입력합니다.
signatureName  | predict_speed  | 학습 모델의 signature 정보를 입력합니다.

options 버튼을 클릭하고 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
key  | model.auto.load | 신규 모델 자동 로딩 기능을 사용할지 여부를 결정합니다.
value  | true  |  'true'로 설정해 자동 로딩 기능을 사용하게 합니다.

위와 같이 설정하면, 학습 모델은 in1 컬럼의 데이터를 입력데이터로 하여 추론 서비스를 제공해주게 됩니다. 이 때 추론 결과는 데이터 프레임의 맨 뒤에 '모델 위치-out'라는 컬럼으로 추가됩니다.

4. **ColumnSelectOperator**

field  |value   | 설명
--|---|--
selectedColumnName | PRCS_DATE <br> input_shifted+00007 |  시간 및 현재 교통속도 (input_shifted+00007) 데이터를 담고 있는 컬럼들을 선택합니다.
selectedPattern  | out  |  모델 추론 결과인 '모델 위치-out'컬럼을 선택합니다.

5. **RenameColumnOperator**

'input_shifted+00007' 컬럼은 현재 교통 속도 데이터를 담고 있는 컬럼입니다. 'current_speed'이름으로 변경합니다.

field  |value   | 설명
--|---|--
existingName  | input_shifted+00007 | 이름을 변경할 컬럼을 선택합니다.
newName  | current_speed | 'current_speed'라는 이름으로 변경합니다.

첫 번째 엔진 실행 결과 'PRCS_DATE, current_speed, ~/modelID-out1'이라는 데이터프레임이 kafka를 통해 사용자에게 전송되게 됩니다. 시간과 현재 교통 속도, 그리고 모델에서 예측 추론한 15분 후의 교통속도가 포함되어 있어 이를 GUI를 입혀 예쁘게 출력시키면 됩니다.

### 두 번째 엔진 생성하기

kafka를 통해 추론된 결과는 사용자에게 전송되는 동시에 '평가 데이터 DB'라는 곳에도 저장합니다. 이는 추후 현재속도 값과 예측 추론된 값 비교를 통해 현재 서비스의 예측 정확도가 얼마나 되는지 실시간으로 파악하기 위해서입니다. 실시간 예측 정확도 측정 결과 성능이 나빠진다 생각되면 '2.8.4. 주기적 재학습하기'를 통해 재학습을 수행하게 하거나 또는 '2.8.8. 다중 학습 모델 성능 평가하기'를 통해 모델 저장소에 있는 모델 중 하나로 대체하게 해야 할 것입니다.

실시간 추론 결과를 '평가 데이터 DB'에 실시간으로 저장하기 위해 본 예제에서는 Spark Structured Streaming기술을 사용합니다. Stream 엔진을 선택합니다.

- 엔진 속성

순번  | 엔진 Type | NickName | 설명
--|---|---|---
2  | Stream  | validationCsvParam  |  추론 데이터를 실시간으로 파일에 저장합니다.

#### Reader

**KafkaPipeReader** 를 선택하고 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
bootStrapServers  | csle1:9092 |  Kafka 접속 주소(IP, 포트번호)를 입력합니다.
zooKeeperConnect  | csle1:2181 |  zookeeper의 접속주소(IP, 포트번호)를 입력합니다.
topic  | kangnam_inference1  |  Kafka 메시지 큐 이름을 입력합니다.
addTimestamp  | false  |  추론 데이터 내의 시간 정보가 포함되어 있기에 false로 설정합니다.
timestampName  | PRCS_DATE  | 추론 데이터 내 'PRCS_DATE' 컬럼을 시간 정보로 활용합니다.
watermark  | 10 minutes  | 늦게 들어오는 데이터는 최대 10분 동안 기다립니다. 10분 이후 들어온 데이터를 폐기합니다.
sampleJsonPath  | hdfs://csle1:9000/user/biuser/dataset/<br>input/validation/kangnam_inference.json  | 입력데이터 형태 포맷을 지정합니다.
failOnDataLoss  | false  | 손실 발생하더라도 작업은 계속하도록 'false'로 지정합니다.
timeFormat | yyyy-MM-dd'T'HH:mm:ss | 'PRCS_DATE'컬럼의 timestamp 형태를 입력합니다.

Spark Structured Streaming은 다른 streaming기술과 달리 이벤트 시간을 기준으로 데이터를 처리할 수 있다는 특징이 있습니다. 가령 IoE 센서로부터 수집되는 데이터가 있을 때, 데이터를 수신한 시간이 아닌 센서가 데이터를 측정했던 실제 시간을 기준으로 해서 집계 처리를 하거나 결측치를 채우는 등의 데이터 처리를 메모리 상에서 수행할 수 있습니다. 이를 위해서는 이벤트가 발생한 시간 정보가 필요합니다. 만일 데이터 내 이벤트 시간 정보가 담겨 있다면 이 정보를 그냥 쓰면 되고 아닌 경우에는 현재 시간을 이벤트 시간으로 해서 사용할 수 있을 것입니다.
이런 사항을 지정하는 것이 'addTimestamp' 필드입니다. 데이터 내 이벤트 시간 정보가 포함되어 있다면 false로 설정해서 데이터 내 포함된 시간을 활용하게 하고, 시간 정보가 없다면 true로 설정해 현재 시간 정보를 이벤트 시간으로 추가하게 한 후 그 시간 정보를 활용하게 합니다.

Spark Structured Streaming의 또 하나의 중요한 특징은 들어올 데이터의 형태 (schema)를 미리 안다는 것입니다. 데이터가 들어오고 나서 이 데이터를 어떻게 처리할지 결정하는 게 아니라, 서비스 시작 전부터 들어오는 데이터에 이런이런 방법으로 데이터를 처리할 거라 미리 다 정해놓습니다. 실제 데이터가 들어오면 schema 정보 검사없이 이미 정해진 방법으로 바로바로 처리하기만 할 뿐입니다. 이를 위해 'sampleJsonPath'에 들어올 데이터의 샘플 형태를 입력하여 엔진이 그 형태를 미리 알 수 있게 해 줘야 합니다.

#### Writer

**FilePipeWriter** 를 선택하고 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
mode | append | 새로 들어온 입력데이터는 'append'합니다
trigger  | 5 seconds  |  5초 간격 mini-batch로 처리합니다.
checkpointLocation  | hdfs://csle1:9000/user/biuser/applications/<br>validation/kangnam1/ckp/file_validation  |  checkpoint를 저장할 폴더를 지정합니다.
timeColumn  | PRCS_DATE |  파일 저장시 참조할 시간 정보가 담겨있는 컬럼을 선택합니다.
saveTimeBasis  | HOUR |  데이터는 HOUR 단위로 저장됩니다.
timeFormat  | yyyy-MM-dd'T'HH:mm:ss  |  시간 컬럼의 timestamp 형태를 입력합니다.

Spark Structured Streaming의 또 하나의 중요한 특징은 'fault-tolerant'하다는 것입니다. 처리된 결과를 WAL (Write Ahead Log)라는 폴더에 저장해 놓기에 오류가 발생해도 쉽게 복구할 수 있습니다. 이를 위해서 처리 결과를 저장해 놓을 위치인 checkpoint를 설정해줘야 합니다.

![2.8.4_checkpoint](https://i.imgur.com/fhUc6ET.png)

설정한 checkpoint위치에 commits, offsets 등 여러 정보가 생성됩니다. 이 때 파일 이름은 0, 1, 2, 이런 식으로 순차적으로 저장되며, 오류 발생시 이 파일들을 접근하여 오류를 복구하게 됩니다.

알아서 복구를 해주기에 장점이 있지만, 단점도 존재합니다. 테스트 종료 후 서비스를 시작하면, 설정된 checkpoint위치에 파일 이름을 0번 부터 시작해서 다시 만들기 시작합니다. 그런데 기존 테스트로 인해 checkpoint에는 이미 0번부터 해서 기존에 처리했던 결과 파일들이 저장되어 있기에 시스템이 헷갈려하기 시작합니다. 정상 동작할 때도 있지만 높은 확률로 데이터를 받았다가 안받았다가 하는 기현상이 발생하기도 합니다. 따라서 테스트할 때는 항상 checkpoint를 지운 후 해 주시는게 낫습니다.

FileInfo를 클릭하여 평가 데이터 DB로 사용할 디렉토리 위치를 입력합니다.

field  |value   | 설명
--|---|--
fileType | CSV | CSV 타입으로 저장합니다.
delimiter  | 5 seconds  |  컬럼 간 구분자는 ','로 지정합니다..
header  | true |  헤더 정보를 포함해서 저장합니다.
saveMode  | OVERWRITE |  파일이 이미 존재하는 경우 덮어씁니다.
\_path  | hdfs://csle1:9000/user/biuser/applications/<br>validation/kangnam1/validation/ |  지정된 디렉토리에 실시간으로 데이터를 저장합니다.

#### Controller
기본 Controller인 **StreamingGenericController** 를 선택합니다.

#### Runner
기본 Runner인 **SimpleSparkRunner** 를 선택합니다.

#### Operator

1. **SelectColumnsPipeOperator**

field  |value   | 설명
--|---|--
colName  | PRCS_DATE, current_speed |  현재 시간 정보와, 교통 속도 정보를 선택합니다.
colPattern  | out  |  모델 추론 결과인 '~/modelID-out1'컬럼을 선택합니다.

2. **ChangeColumnDataTypePipeOperator**

field  |value   | 설명
--|---|--
columnName  | out |  모델 추론 결과인 '~/model위치-out1'컬럼을 선택합니다.
dataType  | STRING  |  문자열 타입으로 변경합니다.

모델 추론 결과인 '~/model위치-out1'컬럼은 vector 타입입니다. vector 타입의 컬럼을 파일에 저장하기 위해서는 vector 내 sub 컬럼들을 다 split해서 따로따로 저장해야 하는데 이 경우 cost가 상당히 높다라는 단점이 있습니다. 이런 이유로 Atelier에서는 vector 형태의 컬럼은 string 형태로 변경한 후 파일로 저장하도록 권고 하고 있습니다.

### 워크플로우 완성화면

다음과 같이 2개의 엔진으로 구성된 워크플로우가 만들어집니다.

![2.8.4_workflow](https://i.imgur.com/CgTr9n2.png)

## 워크플로우 실행 및 결과 확인

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

### 워크플로우 모니터링 하기

웹툴킷 상단 메뉴의 Monitoring 탭을 클릭하면 Workflow 탭이 선택되어있습니다. Workflow 탭에서는 실행한 워크플로우들의 목록 및 동작 상태를 확인하고 제어할 수 있습니다. 위에서 실행한 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.

WorkFlow History 탭을 선택하면, 프레임워크에서 워크플로우가 동작하며 발생시킨 로그 정보를 확인 할 수 있습니다.

## 결과 확인하기

이 워크플로우는 '2.5.3. 실시간 데이터 처리하기' 워크플로우의 3번째 엔진에서 전송되는 데이터를 이용해서 수행되기에 관련 워크플로우가 이미 실행되어 있어야 합니다. 본 예제 내 두 개의 엔진 실행 결과는 다음과 같습니다.

- 첫 번째 엔진 실행 결과 모델 추론 결과가 kafka로 전송됩니다.
- 두 번째 엔진 실행 결과 모델 추론 결과가 평가 데이터 DB 에 저장됩니다.

### 실시간 추론 결과 확인하기

학습 모델 추론 결과가 kafka로 전송되고 있는지 확인하기 위해 콘솔 창에 아래 명령어를 입력해 확인합니다.

```
kafka-console-consumer.sh --zookeeper csle1:2181 --bootstrap-server csle1:9092 --topic kangnam_inference1
```

아래 화면은 그 결과를 캡쳐한 것으로 '현재 시간, 170개 도로 현재 교통속도, 실시간 추론 결과'로 이루어진 데이터가 전송되고 있음을 확인할 수 있습니다.

![2.8.4_inference](https://i.imgur.com/ZXdptWy.jpg)

여기서 '실시간 추론 결과' 컬럼은 vector 형태의 문자열 컬럼인 '170개 도로 현재 교통속도' 컬럼과는 달리 순수 vector 형태입니다. 그래서 두 번째 엔진에서 vector를 문자열로 변경하는 오퍼레이터가 붙은 것입니다 (vector 형태는 파일로 저장할 수 없습니다).

### 실시간 파일 저장 확인하기

![2.8.4_filesave](https://i.imgur.com/LQ0zI1z.png)

실시간으로 저장되고 있는 파일들을 확인합니다.


# 자동 라벨링 데이터 생성하기

모델 검증 및 평가에 있어서 중요한 건 Ground-truth로 활용할 수 있는 Label 데이터를 확보하는 것입니다. 일반적인 학습 모델의 경우 Label 데이터를 관리자가 매뉴얼하게 입력해야 한다는 단점이 있지만, 시계열 데이터 기반 예측 서비스의 경우 자동으로 Label 데이터를 생성하게 할 수 있습니다. 본 튜토리얼에서는 이런 시계열 데이터 기반 예측 서비스에 적용 가능한 자동 라벨링 기능에 대해 소개합니다.

## 자동 라벨링 기능 개요

### 자동 라벨링이 가능한 모델 종류

아래는 자동 라벨링 기능이 적용 가능한 모델을 개념적으로 표현한 것입니다. 자동 라벨링 기능은 RNN과 같이 과거의 정보를 기반으로 미래의 정보를 예측하는 학습 모델에 적용 가능한데 그 중에서도 예측하고자 하는 정보가 input feature에 포함된 경우에만 가능합니다. 가령 과거 일주일 동안의 에너지 사용량을 기반으로 내일 에너지 사용량을 예측하는 딥러닝 모델이나, 과거 40분간의 교통 속도 내역을 기반으로 15분 후의 교통 속도를 예측하는 교통 속도 예측 모델이 이에 해당합니다.

![2.8.8_targetServices](https://i.imgur.com/LksqIpZ.png)

### 자동 라벨링 기능 개념

기본 개념은 간단합니다. Feature로 사용 중인 예측 컬럼의 데이터를 특정 시간만큼 연기시켜 Ground-truth 데이터로 사용하는 것입니다.

과거 일정 기간의 Feature 데이터를 활용하여 특정 시간 후의 Label 컬럼 값을 예측하는 모델의 경우 특정 시간이 지나면 Feature로 사용했던 Label 컬럼 값을 실제 Ground-truth 값으로 사용할 수 있게 됩니다. 가령 15분 후의 교통 속도를 예측하는 딥러닝 모델의 경우, 추론 후 15분이 지나면 교통 속도를 측정하는 택시에 의해 현재의 실측된 교통 속도 값이 올라오게 됩니다. 현재 측정된 속도 정보와 과거 15분 전에 모델을 통해 예측했던 교통 속도 정보를 비교하면 이 모델이 교통 속도를 얼마나 정확하게 예측할 수 있는지 쉽게 확인할 수 있습니다. 24시간 동안의 에너지 사용량을 예측하는 모델의 경우에도 현재 측정된 에너지 사용량 값과 24시간 전 예측했던 에너지 사용량 값을 비교하면 모델의 예측 정확도를 쉽게 확인할 수 있을 것입니다. 즉, Feature로 사용 중인 예측 컬럼의 데이터를 특정 시간만큼 연기시키면, 그 데이터가 바로 모델의 성능을 평가할 수 있는 Label 데이터가 된다라는 것입니다.

Atelier TensorflowPredict 오퍼레이터는 입력데이터에 대한 모델 추론 결과를 '모델위치-out'이라는 새로운 컬럼 (여기서는 predict 컬럼이라고 하겠습니다)으로 만들어 데이터프레임 맨뒤에 추가해 줍니다. 아래의 왼쪽 그림이 이를 나타내고 있습니다. 'input' 컬럼은 feature로 사용되는 입력값이고, predict는 TensorflowPredict 오퍼레이터에 의해 추가된 추론 결과 컬럼으로 15분 후의 예측값을 나타냅니다.

![2.8.8_shifting](https://i.imgur.com/6akNXzd.png)

Atelier에서는 시간 정보를 활용해서 특정 컬럼을 이동시키는 DataShiftingOperator라는 데이터 처리 기능을 제공합니다. 위의 오른쪽 그림이 DataShiftingOperator를 이용하여 'input'컬럼을 5분 단위로해서 15분간 지연시킨 데이터프레임의 모습을 보여주고 있습니다.

이 중 'PRCS_DATE' 컬럼, 모델이 추론한 결과인 'predict'컬럼, 그리고 15분 지연시킨 'input+3' 컬럼만 선택해서 보겠습니다. 아래 그림이 이를 나타냅니다. 'predict'컬럼 값과 'input+3'의 컬럼 값을 간단히 비교하는 것만으로도 모델의 추론 성능을 알 수 있습니다.

![2.8.8_autoLabeling](https://i.imgur.com/DQkNQRZ.png)

## 워크플로우 생성하기

워크플로우 편집화면을 이용하여 자동 라벨링 기능을 수행하는 워크플로우를 아래와 같이 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | 2.8.8.자동라벨 생성하기 | 워크플로우 이름을 입력합니다.   
description  | 실시간 시계열 데이터에 한해 자동적으로 라벨링을 수행해주는 예제입니다.  | 워크플로우를 설명
isBatch | true | 배치 처리를 하는 워크플로우로 true로 설정합니다.
verbose | true | 디버깅을 위해 로그정보를 보고자할 경우 true로 설정합니다.
RunType | 즉시실행 | 실행 타입
Project | Atelier 모델 검증 및 평가 | 워크플로우의 프로젝트 이름을 입력합니다.

### 엔진 선택

자동 라벨링 기능은 파일 기반의 배치 형태로 읽어들일 수도 있고, kafka 등의 스트림 형태로도 읽어들일 수도 있습니다. BatchEngine 또는 MiniBatchEngine 둘 다 사용가능하며, 본 예제에서는 Batch 엔진을 사용하여 학습 모델을 생성하도록 하겠습니다.

- 엔진 속성

순번  | 엔진 Type | NickName  
--|---|---
1  | Batch  | AutoLabelingEngine  

#### Reader

Atelier **Stroage** 에 등록된 `energy_sample.csv` 파일을 읽어들입니다. FileBatchReader를 끌어놓은 후 아래와 같이 속성을 지정해줍니다.

실제 센서 스트림 데이터를

field  |value   | 설명
--|---|--
filePath  | dataset/input/energy_sample.csv |  입력 파일 경로를 입력합니다.
fileType  |  CSV |  읽을 파일의 타입을 입력합니다.
delimiter  |  , |  컬럼간 구분자는 ','입니다.  
header  |  true |  파일의 header 포함 유무를 입력합니다.
field  | 입력없음  | 데이터 내 헤더 정보가 포함되어 있기에 여기선 입력을 안합니다.  
saveMode  | OVERWRITE |  Writer에서 사용되는 옵션으로 Reader에서는 사용되지 않습니다.

#### Writer

FileBatchWriter를 선택하고 아래와 같이 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  | hdfs://csle1:9000/user/biuser/applications/<br>validation/kangnam1/data/autoLabeling.csv |  결과 파일의 저장경로를 입력합니다.
fileType  |  CSV |  저장파일의 타입을 입력합니다.
delimiter  |  , |  컬럼간 구분자를 지정합니다.  
header  |  true |  header 정보를 포함해서 저장할건지 그 유무를 입력합니다.
saveMode  | OVERWRITE  |  파일 저장 방식을 선택합니다.

#### Controller

Spark 환경에서 사용되는 SparkSessionController를 선택합니다.

#### Runner

SimpleSparkRunner를 선택합니다.

#### Operator

1. **ColumnSelectOperator**

field  |value   | 설명
--|---|--
selectedColumnName  | TIMESTAMP |  'TIMESTAMP'라는 컬럼을 선택합니다.
selectedPattern  | METER/W  |  컬럼 이름에 'METER/W'라는 문자열이 포함된 컬럼을 선택합니다.

2. **ColumnArithmeticOperator**

field  |value   | 설명
--|---|--
allPattern  | METER/W  |  컬럼 이름에 'METER/W'라는 문자열이 포함된 모든 컬럼을 선택합니다.
operator  | COL_SUM  |  선택된 모든 컬럼 간 사칙연산 (여기서는 더하기)을 수행합니다.
madeColumnName  | Plug_power  |  'Plug_power'라는 컬럼을 새로 만들어 사칙 연산 결과를 저장합니다.

3. **AggregateDFTimeOperator**

field  |value   | 설명
--|---|--
timeColumnName  |  TIMESTAMP |  시간 축약에 사용할 시간 정보는 'TIMESTAMP'라는 컬럼에 있습니다.
aggrType  | AGGR_AVERAGE  |  평균 값으로 축약합니다.
duration  | 1 minutes  |  축약할 시간은 '1 minutes' 입니다.

위와 같이 설정하면 'TIMETSAMP' 컬럼의 시간 값을 기준으로 1분 단위 평균 집계 처리하게 됩니다.

4. **DataShiftingOperator**

새롭게 추가된 'Plug_power'라는 컬럼을 shifting합니다.

field  |value   | 설명
--|---|--
selectColumnName  | Plug_power  |  현재 교통 속도 정보입니다.
timeColumnName  | TIMESTAMP |  기준 시간 정보를 담고 있는 컬럼명을 입력합니다.
direction  | FORWARD_DIRECTION  |  현재 교통 속도를 15분 앞 당깁니다.
reductionMethod  | DELETE | 데이터 이동으로 공백이 생긴 데이터는 삭제합니다.

shiftInfo를 클릭하여 이동할 시간 정보에 관해 입력합니다.

field  |value   | 설명
--|---|--
time  | 1  |  이동할 시간은 1분입니다.
timeUnit  | MINUTE |  이동할 시간 단위는 minute입니다.
duration  | 15  |  총 15분을 이동시킵니다.
durationUnit  | MINUTE | 이동할 시간 단위는 minute입니다.

위와 같이 설정하면
현재 교통 속도 정보를 5분, 10분, 15분 뒤로 미룬 새로운 컬럼이 현재 데이터 프레임에 append됩니다.

5. **ColumnSelectOperator**

field  |value   | 설명
--|---|--
selectedColumnName  | TIMESTAMP, Plug_power, Plug_power_shifted+00015 |  컬럼명을 입력합니다.

확인하고자 하는 컬럼 명들을 입력합니다.

### 워크플로우 완성화면

다음과 같은 워크플로우가 만들어집니다.

![2.8.8_workflow](https://i.imgur.com/qaPEn7a.png)

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기

위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.  

### 워크플로우 배포하기

위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

### 워크플로우 모니터링 하기

Atelier WebToolKit 상단 "Monitoring" 메뉴의 "Workflow" 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다. 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.

### 결과 파일 확인하기
"Repository" 탭을 클릭하여, path 프로퍼티로 지정된 경로(applications/validation/kangnam1/data)에 autoLabeling.csv 파일이 생성되었음을 확인합니다.

![2.8.8_result_file](https://i.imgur.com/T71F4NN.png)

autoLabeling.csv 파일을 열어 ‘Plug_power’값을 15분 지연시킨 새로운 컬럼이 추가되었는지 확인한다.

![2.8.8_result](https://i.imgur.com/ThkhLVj.png)

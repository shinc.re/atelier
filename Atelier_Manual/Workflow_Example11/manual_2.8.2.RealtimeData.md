
# 실시간 시계열 데이터 처리하기

## 전체 시나리오 개요 (Revisit)

실시간 시계열 데이터 기반의 모델 검증 및 평가 시나리오는 실시간 수집되는 교통 속도를 활용하여 향후 15분 후의 교통 속도를 예측하는 시나리오로, 데이터 수집부터 모델 학습 및 재학습, 15분 후의 교통 속도 추론, 그리고 재학습된 모델의 성능 검증 및 실시간 모델 평가 과정 이렇게 총 5개의 workflow로 구성됩니다.

- 워크플로우 1: 시계열 데이터를 수신하여 실시간으로 정제 및 처리합니다. 처리된 데이터는 학습 데이터 DB에 실시간으로 저장되는 한편 실시간 추론을 위해 워크플로우 3으로 전송됩니다.
- 워크플로우 2: 모델을 생성하기 위한 학습 데이터를 학습 데이터 DB에서 가져와 구성하고 RNN 딥러닝 모델을 생성합니다. 또한 설정된 주기로 재학습 과정을 수행합니다.  
- 워크플로우 3: RNN 딥러닝 모델로 워크플로우 1에 의해 전송받은 데이터에 대해 추론 과정을 수행합니다. 추론 결과는 사용자에게 kafka로 전송되는 한편 향후 모델의 실시간 성능 평가를 위해 평가 데이터 DB에 실시간 저장됩니다.
- 워크플로우 4: 재학습된 RNN 딥러닝 모델을 사용하기에 앞서 모델의 성능이 좋은지 여부를 실제 데이터를 활용하여 검증합니다. 검증에 사용하는 데이터는 학습 데이터 DB에서 가져와 검증용 데이터로 재구성합니다.
- 워크플로우 5: 평가 데이터 DB에서 평가 데이터를 구성하여 현재 서비스에 활용되고 있는 RNN 딥러닝 모델의 추론 성능 결과를 실시간으로 감시합니다.

### 워크플로우 구성 엔진 소개

본 예제는 이 중 첫번째 워크플로우에 해당하는 것으로 교통속도 데이터를 실시간으로 처리하여 학습 데이터 형태로 바꾸는 워크플로우입니다. 아래의 그림과 같이 총 4개의 엔진으로 구성되어 있습니다.

![2.8.1_rtData](https://i.imgur.com/5A2Qxh0.png)

첫번째, 두번째 엔진은 임의적으로 수집되는 raw 데이터를 1분 단위, 5분 단위로 집계하여 결측치 및 이상치를 완화하는 엔진입니다. 튜토리얼 2.6.5 TrafficTimeSeriesPreprocessing과 거의 동일하기에 여기서는 세 번째, 네 번째 엔진에 대한 설정만 다루겠습니다.

#### 입력 데이터 준비하기

본 예제에서는 교통센서로부터 시계열 속도 데이터(LINK_ID, 속도, 날짜)를 Kafka로 입력 받는 것을 가정합니다. 초 단위로 측정된 센서 데이터가 무작위로 들어온다고 가정합니다. 튜토리얼 '2.6.5 실시간 시계열 교통속도 센서스트림 처리 하기'와 동일한 방법으로 스트림 데이터를 생성합니다.

본 예제의 첫 번째, 두 번째, 네 번째 엔진은 Spark Structured Streaming 방식의 Stream 엔진을 사용합니다. Stream 엔진의 경우 앞으로 들어올 데이터의 형태, 즉 스키마 정보를 실행 이전에 미리 설정해줘야 한다는 특징이 있습니다. Host PC의 /home/csle/ksb-csle/examples/input/validation 폴더에 있는 아래의 파일들을 웹툴킷을 이용하여 **Storage** dataset/input/validation 위치에 업로드 합니다.
- kangnam_raw.json (첫 번째, 두 번째 KafkaPipeReader가 수신하는 데이터 형태를 저장하고 있습니다)
- kangnam_engineering.json (네 번째 KafkaPipeReader가 수신하는 데이터 형태를 저장하고 있습니다)

## 워크플로우 생성하기

편집화면에서 워크플로우를 작성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | 01. 실시간 데이터처리 |   워크플로우 이름
description  | 강남 교통 예측 데이터 실시간 처리 Workflow  |  워크플로우를 설명하는 글
isBatch  | false | 스트림 처리를 하는 워크플로우 이므로, false 로 지정
verbose  | true | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |   
Project | 시계열 예측서비스 자동 평가 시나리오 |   워크플로우가 소속될 프로젝트 이름

본 예제에서는 세 번째, 네 번째 엔진 설정 방법에 대해서만 설명합니다.

###  세 번째 엔진 생성하기

첫 번째, 두 번째 엔진을 통해 아래와 같이 5분 단위로 집계된 도로별 교통 속도가 세 번째 엔진으로 입력되게 됩니다.

PRCS_DATE  | LINK_ID  | PRCS_SPD
--|---|--
2016-01-01 00:05:00  | 1220034700  | 32.8
2016-01-01 00:05:00  | 1220019900  | 15.9
2016-01-01 00:05:00  | 1220025800  | 21.1   
...  | ...  | ...  

세 번째 엔진은 스트림 형태로 입력되는 교통 데이터를 다양한 방식으로 처리하여 학습 데이터 형태로 변경하는 작업을 수행합니다. 이를 위해 **MiniBatch** 엔진을 선택합니다.

- 엔진 속성

순번  | 엔진 Type | NickName  | 설명
--|---|---|---
3  | MiniBatch  | Traffic Engineering  | 학습 데이터 처리

#### Reader

두 번째 엔진을 통해 전송되는 데이터를 받을 수 있게 **KafkaReader** 를 선택하여 엔진에 드래그 앤 드롭하고 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
bootStrapServers  | csle1:9092 |  Kafka 접속 주소(IP, 포트번호)를 입력합니다.
zooKeeperConnect  | csle1:2181 |  zookeeper의 접속주소(IP, 포트번호)를 입력합니다.
groupId  | kangnam1  |  Topic 그룹 ID를 입력합니다.
topic  | kangnam_preprocess1  |  Kafka Topic 이름을 입력합니다.

#### Writer

학습 데이터 형태로 최종 처리된 데이터는 실시간 추론 또는 주기적 재학습을 위해 Kafka를 통해 다음 엔진으로 전송됩니다. **KafkaWriter** 를 선택하고 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
bootStrapServers  | csle1:9092 |  Kafka 접속 주소(IP, 포트번호)를 입력합니다.
zooKeeperConnect  | csle1:2181 |  zookeeper의 접속주소(IP, 포트번호)를 입력합니다.
groupId  | kangnam1  |  Topic 그룹 ID를 입력합니다.
topic  | kangnam_engineering1  |  Kafka Topic 이름을 입력합니다.

#### Controller

**SparkStreamController** 를 선택하고 아래와 같이 설정합니다. **SparkStreamController** 는 스트림으로 입력되는 데이터를 버퍼에 저장하고, 일정 주기 마다 정해진 윈도우 크기로 잘라서 operator 에게 전달합니다.

field  |value   | 설명
--|---|--
operationPeriod  |  10 | Reader는 10초마다 데이터를 읽어옵니다.
windowSize  | 1530  | 버퍼에서 윈도우 크기는 170개 도로 x 9 = 1530개입니다.
slidingSize  |  170 | 윈도우를 170개씩 (170개 도로) sliding 하면서 데이터를 가져옵니다.

RNN기반 교통속도 예측 모델은 과거 35분 동안의 교통 속도 내역을 토대로 향후 15분 후의 교통 속도를 예측하는 모델입니다. 즉 추론 작업을 수행하기 위해서는 현재 측정된 교통 속도 데이터와 과거 5분, 10분, ..., 35분 전까지의 교통 속도내역까지 함께 넣어줘야 향후 15분 후의 교통 속도가 예측됩니다.

앞서 언급했듯이, **SparkStreamController** 는 두 번째 엔진에서 올라오는 5분 간격으로 집계된 170개 도로의 교통 속도 데이터를 버퍼에 저장합니다. 그리고 과거 35분 전부터 현재까지의 교통 속도를 고려해야 하기에 버퍼에서 총 8주기의 속도 데이터를 가져와 하나의 input 데이터를 만들게 됩니다. 여기서는 첫 시작 데이터가 어디일지 가늠하기 어려워 총 9주기 (9 * 170 = 1530)를 가져오도록 설정했습니다. 그 후 5분이 지나고 나면 다시 9주기의 데이터를 가져와 input 데이터를 만들어야 하기에 슬라이딩 크기는 1주기 (1 * 170 = 170)으로 설정했습니다.

#### Runner

기본 **SimpleSparkRunner** 를 사용합니다.

#### Operator

세 번째 엔진에서는 총 10개의 Operator를 사용하여 입력데이터를 처리합니다.

1. **ColumnSelectOperator**

field  |value   | 설명
--|---|--
selectedColumnName | PRCS_DATE <br> LINK_ID <br> PRCS_SPD |  선택할 컬럼 이름을 입력합니다.

2. **GroupByFilterOperator**

field  |value   | 설명
--|---|--
keyColName  | PRCS_DATE  |  총계처리를 할 key 컬럼 이름을 입력합니다.
groupby  | COUNT  |  총계처리 방법을 선택합니다.
condition  | EQUAL  |  총계처리 후 적용할 Filter 방법을 선택합니다.
value  | 170  | Filter 값을 입력합니다.

1530개 데이터에서 LINK_ID가 170 개가 되지 않는 데이터를 버리기 위해서, PRCS_DATE 칼럼을 COUNT 하여 170이 되는 조건의 데이터만 남깁니다. 그러면 170 x 8 = 1360 개의 데이터만 남습니다.

3. **OrderByFilterOperator**

field  |value   | 설명
--|---|--
keyColName  | PRCS_DATE  |  정렬을 할 key 컬럼 이름을 입력합니다.
method  | ASC  |  오름차순으로 정렬합니다.
value  | 1360  | 선택할 Row 의 개수를 입력합니다.

PRCS_DATE 컬럼을 정렬하여 8 주기의 데이터 (170 x 8 = 1360 개)만 남깁니다.

4. **MinMaxScalingOperator**

field  |value   | 설명
--|---|--
selectedColumnId |  2 |  선택할 컬럼 ID를 입력합니다 (컬럼 ID는 0부터 시작합니다).
max  | 0.5  |  데이터가 scaling 될 때 최대값을 입력합니다.
min  |  -0.5 |  데이터가 scaling 될 때 최소값을 입력합니다.
withMinMaxRange  |  true | 위에서 설정한 max, min 값 사용 여부를 나타냅니다.
maxRealValue  | 100  |  데이터에서 정상 범위로 사용할 최대값을 입력합니다. 이 이상인 데이터는 버립니다.
minRealValue  |  0 |   데이터에서 정상 범위로 사용할 최소값을 입력합니다. 이 이하인 데이터는 버립니다.

5. **PivotOerator**

field  |value   | 설명
--|---|--
selectedColumnId  | 1  | Pivot 할 컬럼 ID를 입력합니다.
groupByColumn  | 0  |  같은 그룹으로 묶을 컬럼 ID를 입력합니다.
valueColumn  | 2  |  총계처리를 할 컬럼 ID를 입력합니다.
method  | AVG  |  그룹 내 여러 개의 데이터는 평균값으로 대체합니다.

'시간, 도로ID, 교통속도'였던 3개의 컬럼이, 시간에 따른 각 도로의 평균속도 데이터로 변경됩니다.

![2.6_pivotResult](https://i.imgur.com/rFwXQFK.png)

6. **ColumnSelectWithFileOperator**

field  |value   | 설명
--|---|--
columnIdPath  | dataset/input/validation/<br>traffic_kangnam_cols.txt  |  컬럼 이름이 정의된 텍스트파일 경로를 입력합니다.

텐서플로우 기반 속도 예측 모델의 입력 순서에 맞도록 컬럼을 선택합니다. 본 예제에서는 컬럼의 개수가 많으므로 ColumnSelectOperator 대신 ColumnSelectWithFileOperator를 사용합니다. 파일로부터 텍스트를 읽어들인 후 쉼표(,)로 구분하여 구분된 문자열 순서대로 컬럼을 선택합니다.  

7. **VectorAssembleColumnAddOperator**

앞서 pivoting한 결과에서 보듯 170개 도로의 ID는 '121~''라는 형식으로 이루어져 있습니다. 170개 도로 전체를 vector화해 input이라는 새로운 컬럼으로 추가하기 위해 **VectorAssembleColumnAddOperator** 을 추가합니다.

field  |value   | 설명
--|---|--
vectorAssembleColumnName  | input  | assemble된 컬럼명
pattenNames  | 12  |  '12'로 시작하는 컬럼들을 모두 assemble하여 'input'이라는 컬럼으로 새롭게 추가합니다.
keepSrcColumn  | true  |  vector화에 사용한 컬럼을 유지할 경우 true로 설정합니다.

8. **ChangeColumnDataTypeOperator**

input 컬럼에는 170개 도로에 대한 교통 속도가 vector 포맷으로 저장되어 있습니다. 만들고자 하는 RNN기반 딥러닝 모델은 과거 35분 동안의 교통 속도 내역을 기반으로 향후 15분 후의 교통 속도를 예측하는 것이기에 **DataShiftingOperator** 를 이용하여 과거 35분을 고려하도록 input 컬럼을 이동 및 복사시키게 됩니다. 이를 위해 PRCS_DATE 컬럼의 데이터를 timestamp 포맷으로 변경해줍니다 (Spark은 기본적으로 타입 추론을 수행하기에 timestamp포맷으로 읽어들입니다. 그렇지만 컬럼 내 null값과 같은 이상한 값이 있는 경우 가끔씩 문자열로 추론할 때도 있습니다).

field  |value   | 설명
--|---|--
columnName  | PRCS_DATE  | 'PRCS_DATE'컬럼의 타입을 변경하고자 합니다.
dataType  | TIMESTAMP  |  TIMESTAMP 포맷으로 변경합니다.

9. **DataShiftingOperator**

한 행에 35분 동안의 교통 속도 정보가 포함될 수 있게 데이터를 shifting 시킵니다. **DataShiftingOperator** 선택 후 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
patternNames  | input  |  'input'컬럼을 shifting합니다.
timeColumnName  | PRCS_DATE |  기준 시간 정보를 담고 있는 컬럼명을 입력합니다.
direction  | FORWARD_DIRECTION  |  현재 교통 속도를 15분 앞 당깁니다.
reductionMethod  | DELETE | 데이터 이동으로 공백이 생긴 데이터는 삭제합니다.

shiftInfo를 클릭하여 이동할 시간 정보에 관해 입력합니다.

field  |value   | 설명
--|---|--
time  | 5  |  이동할 시간은 5분입니다.
timeUnit  | MINUTE |  이동할 시간 단위는 minute입니다.
duration  | 35  |  총 35분을 이동시킵니다.
durationUnit  | MINUTE | 이동할 시간 단위는 minute입니다.

위와 같이 설정하면 아래의 데이터로 변경됩니다.

![2.8.2_shifting](https://i.imgur.com/vQszk0i.png)

10. **ChangeColumnDataTypeOperator**

현재 'input'으로 시작하는 모든 컬럼들은 vector 타입입니다. vector 타입의 컬럼을 파일에 저장하기 위해서는 vector 내 sub 컬럼들을 다 split해서 따로따로 저장해야 하는데 이 경우 cost가 상당히 높다는 단점이 있습니다. 파일이 아닌 kafka로 전송할 경우에도 key, value 형태의 JSON 포맷으로 변경되어 전송되기에, 데이터 뿐만 아니라 컬럼 명까지 계속해서 전송된다는 단점이 있습니다. 이런 이유로 Atelier에서는 vector 형태의 컬럼은 string 형태로 변경한 후 파일로 저장하거나 kafka로 전송하도록 권고 하고 있습니다. 또한 vector 형태의 string 컬럼으로부터 vector형태의 컬럼을 재변환하는 오퍼레이터도 제공하고 있습니다.

field  |value   | 설명
--|---|--
columnName  | input  | 'input'으로 시작하는 컬럼들의 타입을 변경합니다.
dataType  | STRING  |  STRING 포맷으로 변경합니다.

### 네 번째 엔진 생성하기

위의 10개의 오퍼레이터들을 통해, 실시간으로 인입되는 raw 데이터의 기본적인 정제 작업에서부터 텐서플로우 모델이 활용할 Feature Engineering 작업까지 수행되게 됩니다. 처리된 데이터는 두 번째 워크플로우로 들어가 실시간 추론에 그대로 활용되며, 마찬가지로 모델 재학습에도 그대로 사용되게 됩니다 (즉 학습 데이터와 추론 데이터로 활용합니다).

네 번째 엔진은 위의 처리된 데이터를 파일 형태로 실시간 저장하는 엔진입니다. 실시간으로 처리하기 때문에 Spark Structured Streaming기반의 Stream 엔진을 사용합니다.

- 엔진 속성

순번  | 엔진 Type | NickName | 설명
--|---|---|---
4  | Stream  | Saving for Train  |  처리된 데이터를 실시간으로 파일에 저장합니다.

#### Reader

**KafkaPipeReader** 를 선택하고 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
bootStrapServers  | csle1:9092 |  Kafka 접속 주소(IP, 포트번호)를 입력합니다.
zooKeeperConnect  | csle1:2181 |  zookeeper의 접속주소(IP, 포트번호)를 입력합니다.
topic  | kangnam_engineering1  |  Kafka 메시지 큐 이름을 입력합니다.
addTimestamp  | false  |  데이터 수신 시간을 DataFrame 에 추가하고 싶을 경우, true 로 지정합니다.
timestampName  | PRCS_DATE  | 윈도우, watermark 등을 설정할 시간 정보가 들어있는 컬럼 이름을 입력합니다.
watermark  | 10 minutes  | 늦게 들어오는 입력데이터를 얼마동안 기다릴 것인지 설정합니다.
sampleJsonPath  | hdfs://csle1:9000/user/biuser/dataset/<br>input/validation/kangnam_engineering.json  | 입력데이터 형태를 설정해줍니다.
failOnDataLoss  | false  | 손실 발생시 작업을 그만둘건지의 여부를 설정합니다.
timeFormat | yyyy-MM-dd'T'HH:mm:ss | 시간 컬럼의 timestamp 형태를 입력합니다.

Spark Structured Streaming은 IoE 센서로부터 들어오는 Raw 데이터를 실시간으로 정제하거나 특정 시간 단위로 집계 처리하는 기능을 제공합니다. 이 때 raw 데이터가 들어온 시간이 필요하게 됩니다. 만일 센서 장치가 측정된 값을 보낼 때 시간 정보까지 같이 보낸다면 이 정보를 그냥 쓰면 되고 아닌 경우에는 현재 시간을 시간 정보로 설정해서 사용할 수 있을 것입니다. 'addTimestamp' 필드가 이런 기능을 담당하는 것으로, 센서가 데이터를 수집할 때 시간 정보까지 같이 수집한다면 false로 설정해서 수집된 시간을 활용하게 하고, 시간 정보를 수집하지 않는다면 true로 설정해 현재 시간 정보를 추가하도록 해야 합니다.

Spark Structured Streaming의 또 하나의 중요한 특징은 들어올 데이터의 형태를 미리 안다는 것입니다 (그래서 Structured라는 이름이 붙었습니다). 데이터가 들어오고 나서 이 데이터를 어떻게 처리할지 결정하는 게 아니라, 이미 엔진 시작 전부터 들어오는 데이터에 이런이런 방법으로 데이터를 처리할 거라 미리 다 정해놓습니다. 실제 데이터가 들어오면 schema 정보 검사없이 정해진 방법으로 그냥 바로바로 처리할 뿐입니다. 이를 위해 'sampleJsonPath'에 들어올 데이터의 샘플 형태를 입력하여 엔진이 그 형태를 미리 알 수 있게 해 줘야 합니다.

#### Writer

**FilePipeWriter** 를 선택하고 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
mode | append | 파일에 append합니다.
trigger  | 5 seconds  |  5초 간격의 mini-batch 형태로 데이터를 처리합니다.
checkpointLocation  | hdfs://csle1:9000/user/biuser/applications/<br>validation/kangnam1/ckp/file_engineering  |  checkpoint 파일을 저장할 폴더를 지정합니다.
timeColumn  | PRCS_DATE |  파일 저장시 참조할 시간 정보가 담겨있는 컬럼을 입력합니다.
saveTimeBasis  | HOUR |  파일을 시간 단위로 저장하기 위해 HOUR를 선택합니다.
timeFormat  | yyyy-MM-dd'T'HH:mm:ss  |  시간 컬럼의 timestamp 형태를 입력합니다.

FileInfo를 클릭합니다.

field  |value   | 설명
--|---|--
fileType | CSV | CSV 타입으로 저장합니다.
delimiter  | 5 seconds  |  컬럼 간 구분자는 ','입니다.
header  | true |  헤더 정도를 포함하여 저장합니다.
saveMode  | OVERWRITE |  파일이 이미 존재하는 경우 덮어씁니다.
\_path  | hdfs://csle1:9000/user/biuser/applications/<br>validation/kangnam1/train/ |  지정된 디렉토리에 실시간으로 데이터를 저장합니다.

#### Controller
기본 Controller인 **StreamingGenericController** 를 선택합니다.

#### Runner
기본 Runner인 **SimpleSparkRunner** 를 선택합니다.

#### Operator

1. **SelectColumnsPipeOperator**

field  |value   | 설명
--|---|--
selectedColumnName  | PRCS_DATE, input, input_shifted+00001, <br> input_shifted+00002, input_shifted+00003, <br> input_shifted+00004, input_shifted+00005, <br> input_shifted+00006, input_shifted+00007, |  선택할 컬럼명을 입력합니다.

### 워크플로우 완성화면

다음과 같이 4개의 엔진으로 구성된 워크플로우가 만들어집니다.

![2.8.2_01](https://i.imgur.com/raO7g1X.png)

## 워크플로우 실행 및 결과 확인

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

### 워크플로우 모니터링 하기

웹툴킷 상단 메뉴의 Monitoring 탭을 클릭하면 Workflow 탭이 선택되어있습니다. Workflow 탭에서는 실행한 워크플로우들의 목록 및 동작 상태를 확인하고 제어할 수 있습니다. 위에서 실행한 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.

WorkFlow History 탭을 선택하면, 프레임워크에서 워크플로우가 동작하며 발생시킨 로그 정보를 확인 할 수 있습니다.

## 결과 확인하기

### 실시간 트래픽 전송하기

170개 도로에서 교통속도가 실시간으로 측정되는 환경을 에뮬레이션하기 위해 Host PC의 /home/csle/ksb-csle/examples/input 폴더에 존재하는 ``201601_kangnam_organized_new.csv`` 파일을 사용합니다. 이 파일에는 택시 위에 센서를 부착하여 강남의 170개 도로에서 실제로 측정한 교통 속도 정보가 아래와 같은 형식으로 담겨 있습니다. 실제 환경에서 측정된 것이기에 시간 정보도 뒤섞여 있고, 결측치 값도 상당수 있습니다.

PRCS_DATE  | LINK_ID  | PRCS_SPD
--|---|--
2016-01-01 00:00:27  | 1220034700  | 32.8
2016-01-01 00:00:12  | 1220019900  | 15.9
2016-01-01 00:00:08  | 1220025800  | 21.1  
2016-01-01 00:00:12  | 1220030100  | 28.2
...  | ...  | ...  

Host PC의 /home/csle/ksb-csle/examples/input/validation 폴더에 존재하는 kangnam_producer.py 파이썬 프로그램을 활용하여 실제 환경을 에뮬레이션합니다. 콘솔 창에서 아래와 같이 입력합니다.

```
cd /home/csle/ksb-csle/examples/input/validation
python kangnam_producer.py ../input/201601_kangnam_organized_new.csv csle1:9092 kangnam_raw1 0.01

첫번째 arg: 보내려는 csv 파일 경로
두번째 arg: 카프카 서버 접속 주소
세번째 arg: 토픽명
네번째 arg: 각 row 를 전송하는 주기 (초 단위)
```

파일로 부터 데이터를 한줄씩 읽어 Kafka로 보내는 것을 확인할 수 있습니다.

### 실시간 처리 결과 확인하기

각 엔진 별 처리 결과를 확인합니다.

- 첫 번째 엔진을 통해, 1분 단위로 실시간 집계 처리된 데이터가 kafka로 전송됩니다.
- 두 번째 엔진을 통해, 5분 단위로 실시간 집계 처리된 데이터가 kafka로 전송됩니다. 동시에 추후 오류 발생시 데이터 확인을 위해 '전처리 데이터 DB'에도 저장합니다.
- 세 번째 엔진을 통해, 최종 학습 데이터가 만들어져 kafka로 전송됩니다.
- 네 번째 엔진을 통해, 만들어진 학습 데이터가 학습 데이터 DB에 실시간으로 저장됩니다.

첫 번째 엔진 처리 결과를 확인하기 위해 kafka로 1분 단위 집계 처리된 데이터가 전송되고 있는지 콘솔 창에 아래 명령어를 입력해 확인합니다.

```
kafka-console-consumer.sh --zookeeper csle1:2181 --bootstrap-server csle1:9092 --topic kangnam_preprocess1min1
```

아래 화면은 그 결과를 캡쳐한 것으로 1분 단위로 집계 처리되서 전송되고 있음을 확인할 수 있습니다.

![2.8.2_1min](https://i.imgur.com/JXktlBJ.png)

1분 단위 집계 처리된 데이터는 두 번째 엔진을 통해 5분 단위로 집계 처리됩니다. 콘솔 창에 아래 명령어를 통해 확인합니다.

```
kafka-console-consumer.sh --zookeeper csle1:2181 --bootstrap-server csle1:9092 --topic kangnam_preprocess1
```

아래 화면은 그 결과를 캡쳐한 것으로 5분 단위로 집계 처리되서 전송되고 있음을 확인할 수 있습니다.

![2.8.2_5min](https://i.imgur.com/fc7btzj.png)

5분 단위 처리된 데이터는 '전처리 데이터 DB'에도 저장됩니다. Storage에 접근하여 파일이 실시간으로 생성되고 있는지 확인합니다.

![2.8.2_preprocessDB](https://i.imgur.com/AIhf1u9.png)

세 번째 엔진은 5분 단위 집계 처리된 데이터를 학습 데이터로 변경한 후 kafka로 전송하는 작업을 수행합니다. 콘솔 창에 아래 명령어를 통해 학습 데이터가 전송되고 있는지 확인합니다.

```
kafka-console-consumer.sh --zookeeper csle1:2181 --bootstrap-server csle1:9092 --topic kangnam_engineering1
```

3개 컬럼으로 이루어졌던 데이터가 pivoting등 다양한 데이터 처리 작업을 통해 복잡하게 변경되어 있음을 확인할 수 있습니다.

![2.8.2_trainData](https://i.imgur.com/BasqRzI.jpg)

이 데이터는 '2.8.4. 주기적 재학습하기'와 '2.8.5 실시간 추론하기' 워크플로우에서 그대로 사용되게 됩니다. 학습 데이터 DB에 실시간으로 저장되고 있는 지 Storage에 접근하여 확인합니다.

![2.8.2_trainDB](https://i.imgur.com/5TEDw0L.png)

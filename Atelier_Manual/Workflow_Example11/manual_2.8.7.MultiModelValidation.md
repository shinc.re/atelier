
# 다중 학습 모델 성능 평가하기

## 전체 시나리오 개요 (Revisit)

실시간 시계열 데이터 기반의 모델 검증 및 평가 시나리오는 실시간 수집되는 교통 속도를 활용하여 향후 15분 후의 교통 속도를 예측하는 시나리오로, 데이터 수집부터 모델 학습 및 재학습, 15분 후의 교통 속도 추론, 그리고 재학습된 모델의 성능 검증 및 실시간 모델 평가 과정 이렇게 총 5개의 workflow로 구성됩니다.

- 워크플로우 1: 시계열 데이터를 수신하여 실시간으로 정제 및 처리합니다. 처리된 데이터는 학습 데이터 DB에 실시간으로 저장되는 한편 실시간 추론을 위해 워크플로우 3으로 전송됩니다.
- 워크플로우 2: 모델을 생성하기 위한 학습 데이터를 학습 데이터 DB에서 가져와 구성하고 RNN 딥러닝 모델을 생성합니다. 또한 설정된 주기로 재학습 과정을 수행합니다.  
- 워크플로우 3: RNN 딥러닝 모델로 워크플로우 1에 의해 전송받은 데이터에 대해 추론 과정을 수행합니다. 추론 결과는 사용자에게 kafka로 전송되는 한편 향후 모델의 실시간 성능 평가를 위해 평가 데이터 DB에 실시간 저장됩니다.
- 워크플로우 4: 재학습된 RNN 딥러닝 모델을 사용하기에 앞서 실제 데이터를 활용하여 모델 성능을 검증합니다. 검증에 사용하는 데이터는 학습 데이터 DB에서 가져와 검증용 데이터로 재구성합니다.
- 워크플로우 5: 평가 데이터 DB에서 평가 데이터를 구성하여 현재 서비스에 활용되고 있는 RNN 딥러닝 모델의 추론 성능 결과를 실시간으로 감시합니다.

![2.8.1_overview](https://i.imgur.com/L2mW9Pq.png)

실시간 성능 평가 결과 서비스에 활용되고 있는 RNN 딥러닝 모델 성능이 목표치보다 낮을 수 있습니다. 이런 경우 재학습을 수행하거나 또는 기존 학습된 모델에서 모델 성능 목표치를 만족할 수 있는 모델을 찾을 필요가 있습니다. 기존 모델에서 찾는 경우, 모델 저장소에서 여러개의 RNN 딥러닝 모델을 가져와 평가 데이터로 각 모델들의 성능을 측정하는 다음의 워크플로우가 추가될 수도 있습니다.

- 워크플로우 6: 모델 저장소에서 n (n=3)개의 RNN 딥러닝 모델을 가져와 워크플로우 3에서 구성한 평가 데이터로 각 모델의 성능을 검증합니다.

본 예제는 이 중 여섯 번째 워크플로우에 해당되는 것으로 모델 저장소에 있는 최근 n개의 RNN 딥러닝 모델을 가져와 검증 데이터로 각 모델들의 성능을 평가하는 방법을 다룹니다.

## 워크플로우 생성하기

모델을 평가하기 위한 데이터는 학습 모델을 만들 때 사용했던 데이터가 아닌 실제 필드에서 수집되는 깨끗하지 않는 실시간 데이터여야 합니다. 이 데이터는 '2.8.3 실시간 시계열 데이터 처리하기'워크플로우를 통해 '~applications/validation/kangnam1/train/' 폴더에 시간 순으로 저장되고 있습니다.

이 데이터 중 검증 데이터로 활용할 데이터를 선택해야 합니다. 첫번째 엔진이 이를 담당하는 엔진으로 '학습 데이터 DB'에서 특정 기간에 해당하는 파일만을 골라 임시 폴더로 복사하는 역할을 수행합니다. 두 번째 엔진은 임시 폴더내 잘개 쪼개진 데이터를 검증 데이터로 만든 후 이를 이용하여 모델 저장소에 있는 여러 개의 RNN 딥러닝 모델의 성능을 검증하는 역할을 수행합니다. 세 번째 엔진은 임시 폴더 내 복사된 파일들을 삭제하는 엔진입니다.

### 첫 번째 엔진 구성하기

학습 데이터 DB에 접근해서 검증 데이터로 사용할 최근 데이터만을 임시 폴더로 복사하기 위해 **FileSystem** 엔진을 선택합니다. **FileSystem** 엔진은 특정 디렉토리 내 파일 리스트를 검색한 후 특정 조건에 해당하는 파일만을 필터링하여 다른 폴더로 복사하거나 또는 특정 폴더 내 파일들을 삭제할 때 사용하는 파일 시스템 관련 처리 엔진입니다.

- 엔진 속성

순번  | 엔진 Type | NickName  | 설명
--|---|---|---
1  | FileSystem  | copyDirParam  | 검증 데이터 구성을 위한 관련 csv 파일들 임시 폴더로 복사합니다.

#### Reader

특정 디렉토리 내 파일 리스트를 읽어오기 위해 **LocalFilesCopyReader** 를 선택하여 엔진에 드래그 앤 드롭하고 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
readDir  | hdfs://csle1:9000/user/biuser/<br>applications/validation/kangnam1/train |  복사하고자 하는 파일의 디렉토리 정보를 입력합니다.
enableRecurisve  | true |  디렉토리 내 세부 디렉토리의 파일도 recursive하게 읽어들일 수 있게 true로 설정합니다.

**LocalFilesCopyReader** 는 readDiR 내의 파일 리스트를 읽어 'fileName, absolutePath, modification time, access token' 형태의 데이터 프레임을 구성해서 다음 오퍼레이터로 전송해 줍니다.

~/train 디렉토리에는 '2.8.3. 실시간 시계열 데이터 처리하기' 워크플로우에 의해 처리된 실시간 학습 데이터가 파일 형태로 저장되어 있습니다. 이 중 검증 데이터로 구성할 파일들만 골라서 임시 디렉토리로 복사해야 하기에, 아래 2개의 Operator를 사용하여 파일들을 필터링합니다.

#### Operator

1. **TimeBasedFileFilterOperator**

파일들의 수정 시간을 기반으로 복사할 파일들을 필터링합니다. 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
columnId | 2 |  필터링 할 때 사용할 칼럼 ID, 2번 컬럼은 파일 수정 시간 정보를 가지고 있습니다.
criteriaTime  |  CURRENT |  기준이 되는 시간 정보를 입력합니다. 현재 시간을 기점으로 할 것이기에 'CURRENT'를 입력합니다. 다른 시간을 사용할 경우 'yyyy-MM-dd HH:mm:ss'의 형태로 입력합니다.
consideredTime  |  10 |   기준 시간을 기점으로 어느 정도 시간까지를 고려할 것인지 결정합니다. 현재 시점을 기준으로 10일 이내에 수정된 파일들만 고려할 것이기에 10을 입력합니다.
timeUnit  |  DAY |  10일을 고려할 것이기에 DAY를 입력합니다.

위와 같이 입력하면 현재 시간을 기준으로 10일 이내에 수정된 모든 파일들만을 필터링하게 됩니다.

2. **FsFilterOperator**

Structured Streaming으로 저장된 경우 'part-00~'이런 식의 이름으로 파일이 생성됩니다. 파일 리스트 중에서 'part'로 시작되는 파일만들만 필터링해야 하기에 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
colName | fileName | 필터링 할 때 fileName 컬럼을 사용할 것입니다.
condition  |  START_WITH | 파일 이름이 'part-'로 시작되는 파일만 필터링할 것이기에 'START_WITH'를 선택합니다.
value  |   |  
pattern  | part |  'part'로 시작되는 파일만 필터링합니다.

#### Writer

파일들을 어디로 복사할 건지 설정합니다. **ModelCopyWriter** 를 선택하고 아래와 같이 지정합니다.

field  |value   | 설명
--|---|--
dfs  | hdfs://csle1:9000 |  authority 정보
copyDirPath  | hdfs://csle1:9000/user/biuser/applications/<br>validation/kangnam1/multi_valid_csv |  복사하고자 하는 디렉토리 위치
renamed  | 사용안함 |  파일의 이름을 변경하고자 할 때 사용합니다.
columnSavingPathInfo  | 1  |  파일의 absolutePath 정보를 담고 있는 컬럼 ID
autoIncreased  | false |  복사하고자 하는 디렉토리 내 이미 동일한 파일이 존재하는 경우 자동으로 숫자 1을 증가시켜서 복사합니다. 주로 학습 모델을 복사할 때 사용합니다.
saveMode  | OVERWRITE  |  덮어씁니다.

Controller와 Runner는 기본 setting되어 있는 그대로 사용합니다.

위와 같이 첫 번째 엔진을 구성하면 학습 데이터 DB 내 파일들 중 10일 이내에 수정된 파일들만 평가 데이터로 활용하게 됩니다.

###  두 번째 엔진 구성하기

두 번째 엔진은 임시 폴더내 잘개 쪼개진 데이터를 하나의 데이터프레임으로 만든 후 이를 이용하여 모델의 성능을 검증하는 엔진입니다. 개략적으로 다음의 절차로 동작합니다.

- 임시 폴더 내 파일들을 모아 검증 데이터를 만듭니다.
- 학습 모델 저장소에서 n개의 모델을 로딩합니다.
- 검증 데이터에 대한 각 모델들의 추론 결과를 append합니다.
- 자동 라벨링 기능을 이용하여 시계열 데이터의 Ground-truth 정보를 만들어냅니다.
- 모델 추론 결과 정보와 ground-truth 정보를 이용하여 각 모델들의 성능을 측정한 후 그 결과를 파일에 저장합니다.

#### Reader

임시 디렉토리 내 복사된 모든 파일들을 모아 하나의 데이터프레임을 만들어 검증 데이터를 구성합니다. FileBatchReader 선택 후 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
filePath  | hdfs://csle1:9000/user/biuser/applications/<br>validation/kangnam1/multi_valid_csv |  임시 디렉토리 위치를 입력합니다.
fileType  |  CSV |  읽을 파일의 타입을 입력합니다.
delimiter  |  , |  컬럼간 구분자를 지정합니다.  
header  |  true |  파일의 header 포함 유무를 입력합니다.
field  | 입력없음  | 컬럼의 정보를 입력합니다 (데이터 내 헤더 정보가 포함되어 있기에 여기선 입력을 안합니다).  
saveMode  | OVERWRITE |  Writer에서 사용되는 옵션으로 Reader에서는 사용되지 않습니다.

#### Writer

모델 성능 검증 결과를 저장하기 위해 FileBatchWriter를 선택하고 아래와 같이 속성을 지정합니다.

field  |value   | 설명
--|---|--
filePath  | hdfs://csle1:9000/user/biuser/applications/<br>validation/kangnam1/multiValidation/measure.csv |  결과 파일의 저장경로를 입력합니다.
fileType  |  CSV |  저장파일의 타입을 입력합니다.
delimiter  |  , |  컬럼간 구분자를 지정합니다.  
header  |  true |  header 정보를 포함해서 저장할건지 그 유무를 입력합니다.
saveMode  | OVERWRITE  |  파일 저장 방식을 선택합니다.

#### Operator

Reader를 통해 읽어들인 검증 데이터는 '시간-35분전 교통속도-30분전 교통속도-25분전 교통속도-...-현재 교통속도'이런 형태로 구성되어 있습니다.

먼저 시간 정보를 기준으로 정렬 과정을 수행합니다.

1. **OrderByOperator**

field  |value   | 설명
--|---|--
selectedColumnID | 0 |  첫번째 컬럼이 시간 정보를 담고 있습니다. index 0을 입력합니다.
method  | ASC  |  오름차순으로 정렬합니다.

2. **DataShiftingOperator**

정렬이 완료된 후 Ground-truth 정보를 생성하기 위해 DataShiftingOperator를 선택한 후 아래와 같이 설정합니다 (자동 라벨링 관련 설명은 '2.8.9 자동 라벨 데이터 생성하기' 참조바랍니다).

field  |value   | 설명
--|---|--
patternNames  | input_shifted+00007  |  현재 교통 속도 정보입니다.
timeColumnName  | PRCS_DATE |  기준 시간 정보를 담고 있는 컬럼명을 입력합니다.
direction  | FORWARD_DIRECTION  |  현재 교통 속도를 15분 앞 당깁니다.
reductionMethod  | DELETE | 데이터 이동으로 공백이 생긴 데이터는 삭제합니다.

shiftInfo를 클릭하여 이동할 시간 정보에 관해 입력합니다.

field  |value   | 설명
--|---|--
time  | 5  |  이동할 시간은 5분입니다.
timeUnit  | MINUTE |  이동할 시간 단위는 minute입니다.
duration  | 15  |  총 15분을 이동시킵니다.
durationUnit  | MINUTE | 이동할 시간 단위는 minute입니다.

위와 같이 설정하면 현재 교통 속도 정보를 5분, 10분, 15분 뒤로 미룬 새로운 컬럼이 현재 데이터 프레임에 append됩니다.

3. **ColumnSelectOperator**

field  |value   | 설명
--|---|--
selectedColumnName  | PRCS_DATE, input, input_shifted+00001, <br> input_shifted+00002, input_shifted+00003, <br> input_shifted+00004, input_shifted+00005, <br> input_shifted+00006, input_shifted+00007, input_shifted+00007_shifted+00003 |  컬럼명을 입력합니다.

Feature 데이터로 사용할 컬럼 명들을 입력합니다.

4. **RenameColumnOperator**

'input_shifted+00007_shifted+00003' 컬럼은 현재 교통 속도 정보를 15분 뒤로 미룬 것으로, 추후 Ground-truth 컬럼으로 사용될 것이기에 컬럼명을 변경해 줍니다.

field  |value   | 설명
--|---|--
existingName  | input_shifted+00007_shifted+00003 | 이름 변경할 컬럼
newName  | ground | 변경할 새로운 컬럼명

5. **VectoredStringToVectorOperator**

교통 예측을 위한 RNN기반 텐서플로우 모델은 과거 40분 동안의 170개 도로에 대한 교통속도를 입력받아 향후 15분 후의 170개 도로에 대한 교통 속도를 예측하는 모델입니다. 즉, 170개 도로에 대한 40분 간의 교통속도 내역 데이터가 하나의 행으로 변형되어 모델의 입력으로 들어가야 합니다.

input 컬럼에는 과거 35분전의 170개 도로에 대한 교통 속도가 벡터 포맷의 문자열 형태로 저장되어 있습니다. 그리고 input_shifted+00001에는 과거 30분전의 170개 도로에 대한 교통 속도가, input_shifted+00002에는 과거 25분전의 교통 속도가 저장되어 있습니다. 과거 35분 전부터 현재까지의 교통 속도 정보들을 하나로 모아 vector 형태로 변경하기 위해 VectoredStringToVectorOperator을 끌어놓습니다.

field  |value   | 설명
--|---|--
columnName  | input | input이라는 단어로 시작되는 컬럼들을 하나로 모읍니다.
dropOriginal  | false  |  원래 데이터는 삭제하지 않고 그대로 둡니다.
newName  | in1  | 하나로 모은 데이터의 컬럼명을 in1이라고 설정합니다.

텐서플로우 모델은 여기서 새롭게 생성된 in1 컬럼의 데이터만을 활용하여 추론 과정을 수행하게 됩니다.

6. **TFModelPerformanceInspector**

모델 검증을 위해 **TFModelPerformanceInspector** 끌어놓은 후 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
modelRepository  | hdfs://csle1:9000/user/<br>biuser/model/kangnam1 | 검증하고자 하는 모델이 저장되어 있는 모델 저장소 위치를 입력합니다.
numModelsToConsider  | 3 |  모델 저장소에서 가져올 모델의 갯수를 입력합니다. 최근 3개의 학습 모델의 성능을 검증할 것이기에 3을 입력합니다.
modelName  | kangnam_traffic | 모델의 이름을 입력합니다.
signatureName  | predict_speed | 모델의 signature명을 입력합니다.
modelIDs  | 입력없음  | ID 정보를 이용해서 모델을 가져오고자 할 때 입력합니다.

텐서플로우 모델 추론 결과 '모델 위치 정보-out'이라는 컬럼이 새롭게 append됩니다.

7. **ColumnSelectOperator**

검증에 활용할 컬럼만을 선택합니다.

field  |value   | 설명
--|---|--
selectColumnName  | PRCS_DATE, ground, <br> input_shifted+00007 | 선택하고자 하는 컬럼명을 입력합니다.
selectedPattern  | out | 'out'이라는 문자열이 포함된 컬럼명을 선택합니다.

8. **RenameColumnOperator**

여기서 input_shifted+00007은 170개 도로의 현재 교통속도 정보를 담고 있는 컬럼입니다. 이름을 변경합니다.

field  |value   | 설명
--|---|--
existingName  | input_shifted+00007 | 이름 변경할 컬럼
newName  | current_speed | 변경할 새로운 컬럼명

9. **ChangeColumnDataTypeOperator**

텐서플로우 모델을 통해 추론된 '모델 위치 정보-out' 컬럼은 vector 타입입니다. 성능 계산을 위해 문자열 형태로 변경합니다.

field  |value   | 설명
--|---|--
columnName  | out | 'out'이라는 문자열을 포함한 컬럼의 타입을 변경합니다.
dataType  | STRING | 변경할 타입은 String입니다.

vector 타입의 컬럼을 disassemble해서 다수 개의 sub 컬럼들로 분리할 수 있습니다. 그 후 Spark ML에서 제공하는 성능 측정 방법을 활용한 PerformanceMasure 오퍼레이터를 이용해서 성능을 계산할 수도 있습니다. Spark ML에서 제공하는 성능 측정 방법을 사용하기에 안정적이다는 장점이 있지만 vector 내 sub 컬럼이 많은 경우 성능 이슈가 크게 발생합니다.

가령 vector 내 sub 컬럼이 5개 미만이면 속도 차이를 체감할 수 없지만, 본 예제와 같이 vector 내 sub 컬럼이 170개(170개 도로)인 경우 성능 차이가 확연합니다 (컬럼 내 sub 컬럼을 분리하는데만 대략 2분 정도가 소요됩니다). 이런 이유로 여기서는 String 타입으로 변경 후 PerformanceMeasure 오퍼레이터가 아닌 NativePerformanceMeasure를 사용하여 성능을 측정합니다.

10. **NaivePerformanceMeasurer**

모델을 통해 추론된 컬럼과 ground 컬럼의 각 값들을 비교하여 성능을 측정합니다.

field  |value   | 설명
--|---|--
ground  | ground | ground-truth값은 'ground'이라는 문자열을 포함한 컬럼입니다.
predict  | out | 추론 결과는 'out'이라는 문자열을 포함한 컬럼입니다.
modelType  | REGRESSION | 교통속도 예측 모델은 회귀 분석 모델입니다.
measureType  | RMSE | RMSE를 이용해서 성능을 측정합니다.
seperator  | , | 컬럼 내 sub 컬럼은 ','로 구분되어 있습니다.
kfold  | 1 |  구성된 검증 데이터 전체로 모델을 검증합니다.

학습 모델 검증 방법에서 소개한 검증 데이터 분리 후 검증 반복횟수를 입력합니다.

#### Controller

기본 Controller를 사용합니다.

#### Runner

기본 Runner를 사용합니다.


### 세 번째 엔진 생성하기

세 번째 엔진은 임시 폴더 내 복사된 파일들을 삭제하는 엔진입니다.

- 엔진 속성

순번  | 엔진 Type | NickName | 설명
--|---|---|---
3  | FileSystem  | deleteDirParam  | 임시 폴더 내 복사된 파일들 삭제

#### Reader

**LocalFilesCopyReader** 를 선택하여 엔진에 드래그 앤 드롭하고 아래와 같이 설정합니다.

field  |value   | 설명
--|---|--
readDir  | hdfs://csle1:9000/user/biuser/applications/<br>validation/kangnam1/multi_valid_csv |  삭제하고자 하는 파일의 디렉토리 정보
enableRecurisve  | true |  디렉토리 내 세부 디렉토리의 파일들도 recursive하게 읽어들일지 결정

특정 파일을 필터링할 필요 없이 ~/multi_valid_csv 디렉토리 내 모든 파일을 삭제합니다.

#### Writer

field  |value   | 설명
--|---|--
columnSavingPathInfo  | 1 |  삭제하고자 하는 파일들의 path 정보를 담고 있는 컬럼 ID를 입력합니다.
enableRecurisve  | true |  디렉토리 내 세부 디렉토리의 파일들도 recursive하게 삭제합니다.

#### Controller

기본 Controller를 사용합니다.

#### Runner

기본 Runner를 사용합니다.

<br>

### 워크플로우 완성화면

다음과 같이 3개의 엔진으로 구성된 워크플로우가 만들어집니다.

![2.8.7_workflow](https://i.imgur.com/ilLimSc.png)

## 워크플로우 실행 및 결과 확인

### 워크플로우 실행 및 모니터링 하기

#### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

#### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

#### 워크플로우 모니터링 하기

웹툴킷 상단 메뉴의 Monitoring 탭을 클릭하면 Workflow 탭이 선택되어있습니다. Workflow 탭에서는 실행한 워크플로우들의 목록 및 동작 상태를 확인하고 제어할 수 있습니다. 위에서 실행한 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.

WorkFlow History 탭을 선택하면, 프레임워크에서 워크플로우가 동작하며 발생시킨 로그 정보를 확인 할 수 있습니다.

### 결과 확인하기

- 첫 번째 엔진 실행 결과 검증 데이터를 구성하기 위한 파일들이 학습 데이터 DB에서 임시 폴더로 복사됩니다.
- 두 번째 엔진 실행 결과 모델 저장소 내 최근 3개의 학습 모델을 가져와 그 성능을 측정한 결과 파일이 생성됩니다.
- 세 번째 엔진 실행 결과 임시 폴더 내 복사된 파일들이 삭제됩니다.

#### 임시 파일 Copy 확인

먼저 검증 데이터를 구성할 파일들이 임시 디렉토리 내로 성공적으로 복사되었는지 확인합니다.

![2.8.7_copy](https://i.imgur.com/4VTm0yZ.png)

#### 성능 측정 결과 파일 생성 확인

먼저 모델 저장소 내 저장되어 있는 모델 정보를 확인합니다. 총 5개의 모델이 저장되어 있음을 확인할 수 있습니다.

![2.8.7_repository](https://i.imgur.com/YvhZ2px.png)

워크플로우가 실행되면 모델 저장소 내 3개의 모델을 가져와 Ground-truth값과 모델에 의해 추론된 값 비교를 통해 모델의 성능 검증이 이루어집니다. 그 결과는 아래와 같이 measure.csv파일에 저장됩니다.

![2.8.7_result_file](https://i.imgur.com/yW9I1ta.png)

#### 성능 측정 결과 확인

measure.csv 파일을 열어 성능 결과 내용을 확인합니다.

![2.8.7_result](https://i.imgur.com/tUmWDzg.png)

'numModelsToConsider' 필드값을 3으로 설정했기에 모델 저장소에서 가장 최근에 생성된 3개의 모델을 가져와 성능 검증을 수행하게 됩니다. ~/0004-out-1(21)은 0004번 모델의 성능 측정 결과를, ~/0003-out-1(21)은 0004번 모델의 성능 측정 결과를 나타냅니다. 여기서 21은 검증 데이터 내 레코드의 갯수를 나타냅니다.

#### 임시 파일 삭제 확인

![2.8.7_delete](https://i.imgur.com/7SQGHYH.png)

학습 모델 검증 후 임시 폴더에 복사되어 있던 파일들이 성공적으로 삭제되었음을 확인할 수 있습니다.

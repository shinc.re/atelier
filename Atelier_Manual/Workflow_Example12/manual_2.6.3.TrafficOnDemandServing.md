
# 온디맨드 방식으로 교통속도예측 모델 서빙하기 (TrafficOnDemandServing)

텐서플로우 기반 딥러닝 모델을 이용한 교통 속도 예측 RESTful 서비스 예제를 설명합니다.

## 입력 데이터 준비하기
**교통속도예측 텐서플로우 모델 학습하기** 매뉴얼에서 학습한 후 export 된 모델이 ``/model/kangnam`` 위치에 저장되어 있다고 가정합니다.



## 워크플로우 생성하기
워크플로우 편집화면에서 워크플로우를 작성합니다. 하나의 엔진을 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | TrafficOnDemandServing |  워크플로우 이름    
description  | 강남 교통 RESTful 서비스 예제  |  워크플로우를 설명하는 글
isBatch  | false | RESTful 서비스를 하는 워크플로우 이므로, false 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType  |  즉시실행  |  
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름

### 엔진 생성하기
텐서플로우를 이용하여 온 디맨드 형태로 교통속도 예측 서비스를 생성하는 워크플로우이므로, **OnDemandServing** 엔진을 선택합니다. OnDemandServing 엔진에서는 데이터를 입출력하기 위한 Reader 와 Writer를 사용하지 않습니다. 데이터는 request 에 실어서 보냅니다.


- 엔진 속성

순번  | 엔진 Type | NickName   | 설명
--|---|---|---
1  | OnDemandServing  | ServingEngine   | RESTful 서비스 제공


#### Controller
**TensorflowServingController** 를 선택합니다.


#### Runner
**TensorflowServingRunner** 를 선택하고 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|---|--
port  | 8001  |   RESTful 서비스 포트번호  
modelName  | kangnam_traffic  |  모델의 이름  
modelBasePath  | model/kangnam  | 모델 경로




#### Operator
**TensorflowServingOperator** 를 선택합니다.


<br>



![2.6.3_workflow](https://i.imgur.com/mWHq1EB.png)


## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.


### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.




### 워크플로우 모니터링 하기

Monitoring 탭을 클릭하면 Workflow 탭이 선택되어있습니다. Workflow 탭에서는 실행한 워크플로우들의 목록 및 동작 상태를 확인하고 제어할 수 있습니다. 위에서 실행한 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.

![2.6.3_monitoring](https://i.imgur.com/0z9Js0t.png)


WorkFlow History 탭을 선택하면, 프레임워크에서 워크플로우가 동작하며 발생시킨 로그 정보를 확인 할 수 있습니다.

![2.6.3_monitoring_history](https://i.imgur.com/LpELzxd.png)

### 서빙 서비스 상태 보기
Atelier WebToolKit Dashboard 화면을 선택하면, 화면 하단의 Services 테이블에서 본 예제에서 실행한 서빙 서비스에 대한 정보를 확인할 수 있습니다.

![2.6.3_services](https://i.imgur.com/Vvewunq.png)

### 서빙 서비스 확인하기
Postman 프로그램을 활용하여 서빙 엔진이 제공하는 RESTful 서비스를 테스트할 수 있습니다. Postman을 실행하여 아래와 같이 설정합니다.
- 좌측 상단의 콤보 박스에서 **POST** 를 선택하고, 질의 선택할 URL을 입력합니다. 본 예제에서의 URL은 서빙 엔진이 제공하는 URL (``http://0.0.0.0:8001/model/predict_speed/in1``)을 사용합니다. "predict_speed" 는 모델 export 시 정의한 signature 이름이고, "in1" 은 입력 tensor 로 정의한 이름입니다.
- **Headers** 메뉴에서 Key 필드에 "Content-Type"을, Value 필드에 "text/csv" 을 입력합니다.

![2.6.3_postmanSetting](https://i.imgur.com/sWybhUv.png)

- **Body** 메뉴에서 **binary** 라디오 버튼을 선택한 후, **파일 선택** 버튼을 클릭하여 입력 파일을 선택합니다. 입력파일은 Host PC의 /home/csle/ksb-csle/examples/input 폴더에 있는 ``traffic_serving_input1.csv`` 또는 ``traffic_serving_input2.csv`` 를 사용합니다.
- 우측 상단의 **Send** 버튼을 클릭하면 질의 요청이 제출되고, 결과가 화면 하단에 표시됩니다. 입력 데이터에 대한 15분 뒤 예측된 교통 속도가 리턴됩니다.

![2.6.3_servingResult1](https://i.imgur.com/9CjnzkZ.png)

## 워크플로우 종료하기
"Monitoring" 메뉴의 "Workflow" 탭에서 실행 중인 TrafficOnDemandServing 워크플로우를 종료(<span style="color:red">&#9724;</span>)할 수 있습니다.

## 워크플로우 저장하기
워크플로우 편집 화면에서 작성한 워크플로우를 "Save Workflow" 메뉴를 이용하여 저장합니다.

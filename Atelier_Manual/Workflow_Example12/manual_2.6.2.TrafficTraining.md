
# 교통속도예측 텐서플로우 모델 학습하기 (TrafficTraining)

일정 기간 수집하여 5분 단위 평균 속도 시계열 데이터를 이용하여 텐서플로우 기반 딥러닝 모델을 학습하고 서빙용 모델을 만드는 예제를 설명합니다.

![2.6.2.TrafficTraining_intro](https://i.imgur.com/IpGTXC8.png)

## 입력 데이터 준비하기
<a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.6.1.TrafficPreprocessing.md">교통속도 스트림 데이터 전처리하기</a> 매뉴얼에서 만든 결과 파일(``traffic_processing.csv``) 이 있다고 가정합니다. 또한 텐서플로우 기반의 15분 뒤 속도를 예측하는 모델이 파이썬 코드로 구현되어 있다고 가정합니다.


### 입력파일 업로드
``traffic_processing.csv`` 파일을 **Storage** 에 웹툴킷을 이용하여 dataset/input/traffic 위치에 업로드 합니다.

![2.6.2_fileUpload](https://i.imgur.com/K6xJJ9F.png)

### 사용자 파이썬 코드 업로드
**Storage** 에서 trainingcode/traffic_speed 폴더를 생성합니다.
그리고, Host PC의 /home/csle/ksb-csle/examples/trainingcode/traffic_speed/train.py 파일을 **Storage** 의 trainingcode/traffic_speed 폴더에 업로드 합니다.

![2.6.2_codeUpload](https://i.imgur.com/KUhELCL.png)


`train.py` 파일은 텐서플로우 기반의 15분 뒤 속도를 예측하는 모델을 학습하는 코드가 구현되어 있는 파일입니다. (본 예제에서는 LSTM 알고리즘을 이용하여 15분 뒤 속도를 예측하는 모델을 사용합니다.) 프레임워크와 연동하여 학습이 되도록 파이썬 코드에 input 과 output 경로 등을 argument 로 받는 부분이 코딩되어야 합니다. 자세한 내용은 <a href="https://etrioss.kr/hooyoung/beeaimanual1906/blob/master/manual_1906_gitUpload/2.9.1.KSB_TfPyCoding_Guide.md">Atelier Framework와 연동을 위한 tensorflow 학습코드 작성하기</a> 를 참고합니다.
main 함수를 포함한 사용자 파이썬 코드에서 사용하는 라이브러리 파일 등은 동일한 폴더에 위치 시키거나 1 depth의 하위 폴더까지 작성 가능합니다.

#### 학습용 GPU 디바이스 지정하기
사용자의 TensorFlow 파이썬 학습코드가 수행될 때, 특정 GPU 만을 이용해서 학습하도록 하려면 TensorFlow 의 session 을 생성 할 때, GPU를 지정하는 코드를 작성해줘야합니다. 코드의 내용은 다음과 같습니다.
```python
#0번 GPU를 이용하도록 gpu_options 생성
gpu_options = tf.GPUOptions(visible_device_list="0")
#위의 옵션을 사용해서 TensorFlow 의 session 을 생성
with tf.Session(config=tf.ConfigProto(gpu_options=gpu_options)) as sess:
```

![2.6.2_03](https://i.imgur.com/uiNq416.png)

Atelier Storage 의 trainingcode/traffic_speed 폴더에 GPU 0번을 이용하는 텐서플로우 학습코드가 준비되어 있으니 특정 GPU 를 이용한 워크플로우를 편집하고자 하는 경우에 활용하도록 합니다.


## 워크플로우 생성하기
워크플로우 편집화면에서 워크플로우를 작성합니다. 본 예제에서는 하나의 엔진을 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | TrafficTraining | 워크플로우 이름    
description  | 강남 교통 학습 예제  | 워크플로우를 설명하는 글
isBatch  | true | 배치 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType  |  즉시실행  |  
Project | Atelier Tutorials  |   워크플로우가 소속될 프로젝트 이름



### 엔진 생성하기
텐서플로우를 이용하여 배치 형태로 딥러닝 모델을 생성하기 위해 **External** 엔진을 선택합니다.


- 엔진 속성

순번  | 엔진 Type | NickName  | 설명
--|---|---|---
1 | External  | TrainEngine  | 딥러닝 모델 학습


#### Reader
**FileBatchReader** 를 선택하고 아래표와 같은 속성을 지정합니다. path 에 입력 데이터셋 파일의 경로를 지정합니다. 이 값은 사용자 파이썬 코드(텐서플로우 모델을 정의하고 학습하는 코드)의 `--dataset` 파라미터로 전달됩니다.

field  |value   | 설명
--|---|--
path |  dataset/input/traffic/traffic_processing.csv |  데이터셋 파일 경로
fileType  |  CSV |  파일 타입
delimiter  |  , |  구분자
header  |  false |  header 포함 유무
saveMode  |   | 사용 하지 않음

path 에는 아래와 같이 3가지 경로가 입력 가능합니다.

1) 로컬 파일시스템 절대경로
   file:///home/csle/ksb-csle/examples/input/{선택한 경로 및 파일명}
2) hdfs 절대경로
   hdfs://{host}:{port}/user/{user_id}/dataset/{선택한 경로 및 파일명}
3) Hdfs 상대경로
   dataset/{선택한 경로 및 파일명}

#### Writer
**FileBatchWriter** 를 선택하고 아래표와 같은 속성을 지정합니다. path 에 학습한 딥러닝 모델이 최종 저장될 폴더를 지정합니다. 해당 폴더에 자동으로 최신버전의 폴더를 생성한 후 모델이 저장됩니다. 모델의 버전은 0001 부터 시작하여 1씩 증가합니다. path 에 입력한 값은 사용자 파이썬 코드(텐서플로우 모델을 정의하고 학습하는 코드)의 `--model_dir` 파라미터로 전달됩니다.


field  |value   | 설명
--|---|--
path  | model/traffic_speed/ |  저장 경로  
fileType  |  CSV |  파일 타입
delimiter  |  , |  구분자
header  |  false |  header 포함 유무
saveMode  |   | 사용 하지 않음

path 에는 아래와 같이 3가지 경로가 입력 가능합니다.

1) 로컬 파일시스템 절대경로
   file:///home/csle/ksb-csle/{선택한 경로}
2) hdfs 절대경로
   hdfs://{host}:{port}/user/{user_id}/model/{선택한 경로}
3) hdfs 상대경로
   model/{선택한 경로}

그러나, 본 예제에서는 다른 엔진에서 학습한 딥러닝 모델을 사용하도록 하기 위해 HDFS 상대경로를 지정합니다.

#### Controller
Controller는 외부 Tensorflow 호출하기 위해 **TensorflowTrainController** 를 선택합니다.

#### Runner
**TensorflowRunner** 를 선택합니다. path 에 main 함수를 포함하는 사용자 파이썬 코드(텐서플로우 모델을 정의하고 학습하는 코드)의 경로 및 파일명을 지정합니다.

field  |value   | 설명
--|---|--
path  | trainingcode/traffic_speed/train.py  |  사용자 파이썬 코드 경로
cluster |  false |  cluster 환경 실행 여부
inJson  | false  |  Json 형태 파라미터 전달 유무
tfVersion  |  r1.6 |  Tensorflow 버전 정보

path 에는 아래와 같이 3가지 경로가 입력 가능합니다.   

1) 로컬 파일시스템 절대경로
   file:///home/csle/ksb-csle/{선택한 경로 및 파일명}
2) hdfs 절대경로
   hdfs://{host}:{port}/user/{user_id}/{선택한 경로 및 파일명}
3) hdfs 상대경로
   trainingcode/{선택한 경로 및 파일명}

사용자는 로컬 파일시스템에 원하는 이름(예: kangnam) 폴더를 생성하고 해당 폴더 내에 main 함수를 포함하는 사용자 python 파일(예:train.py)을 작성합니다.
그리고, 관련된 라이브러리 파일 등은 동일한 폴더에 위치 시키거나 1 depth의 하위 폴더까지 작성 가능합니다.

#### Operator
**DLTrainOperator** 를 선택하고 아래표와 같은 속성을 지정합니다. path 에 모델의 체크포인트를 저장할 경로를 지정합니다. 이 값은 사용자 파이썬 코드(텐서플로우 모델을 정의하고 학습하는 코드)의 `--checkpoint_dir` 파라미터로 전달됩니다. 로컬 파일시스템 절대경로로 입력합니다.

field  |value   | 설명
--|---|--
path  | file:///tmp/traffic_speed_local/ckpts |  모델의 체크포인트를 저장할 경로 <br> 반드시 로컬 시스템 경로만 지정
additionalParams  |   |  아래의 표 참고

additionalParams 설정은 [+] 버튼을 두 번 클릭하여 다음과 같이 합니다. 사용자가 필요한 파라미터의 이름과 값을 세팅하여 사용자 파이썬 코드로 전달합니다.

field  |value   | 설명
--|---|--
paramName  | is_train  |  학습 유무
paramValue  | True  |  

field  |value   | 설명
--|---|--
paramName  | num_epochs  | 학습 반복 횟수
paramValue  | 2  |   

<br>

![2.6.2_workflow](https://i.imgur.com/TPhPsMf.png)

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.


### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.


TensorFlowRunner 에서 지정한 train.py 학습코드가 실행되며 모델의 학습이 실행되는 것을 Engine실행로그에서 확인 할 수 있습니다.

![2.6.2_workflowLog](https://i.imgur.com/vwpilYb.png)

### 워크플로우 모니터링 하기

Atelier WebToolKit 상단 메뉴의 Monitoring 탭을 클릭하면 Workflow 탭이 선택되어있습니다. Workflow 탭에서는 실행한 워크플로우들의 목록 및 동작 상태를 확인하고 제어할 수 있습니다. 위에서 실행한 워크플로우 이름을 클릭하면 워크플로우의 각 엔진들의 동작 상태 (status) 를 확인할 수 있습니다.

![2.6.2_monitoring](https://i.imgur.com/NN813Su.png)


#### 모델 학습을 위한 GPU 디바이스 사용 현황 보기
아래의 명령을 이용하여 모델 학습에 사용중인 GPU 0번의 사용 현황을 확인한다.
```
watch nvidia-smi -i 0
```
![2.6.4.TrainGPUCheck](https://i.imgur.com/rePh9pJ.png)



WorkFlow History 탭을 선택하면, 워크플로우가 동작하며 발생시킨 로그 정보를 확인 할 수 있습니다.

![2.6.2_monitoring_history](https://i.imgur.com/Ro0nbaa.png)

### 학습한 후 export 된 모델 확인하기  
FileBatchWriter 에서 지정한 위치 (model/traffic_speed/) 에 학습된 모델이 생성된 것을 확인합니다.
Atelier WebToolKit 상단의 Repository 메뉴에서 model/traffic_speed 위치에 최신버전인 0000 폴더를 만들고 학습된 모델(saved_model.pb 파일 및 variables 폴더)이 저장된 것을 확인할 수 있습니다. 만약 0000 폴더가 이미 있었다면, 0001 폴더를 만들고 학습된 모델이 저장됩니다.  

![2.6.2_modelSave](https://i.imgur.com/vxHw5GV.png)

**<span style="font-size: 15pt;">&#10070; 참고</span>**
학습 후 export 한 모델을 프레임워크에서 REST API 서비스가 가능하도록 띄우고, 클라이언트는 쿼리를 통해 예측 결과를 받을 수 있습니다. 사용자는 온디맨드 방식으로 서비스를 구성하거나 (온디맨드 방식으로 교통속도예측 모델 서빙하기 참고) 스트림 방식으로 서비스를 구성할 수 있습니다 (스트림 방식으로 교통속도예측 모델 서빙하기 참고).

## 워크플로우 종료하기
Atelier WebToolKit 상단 "Monitoring" 메뉴의 "Workflow" 탭에서 실행 중인 TrafficTraining 워크플로우를 종료(<span style="color:red">&#9724;</span>)할 수 있습니다.

## 워크플로우 저장하기
워크플로우 편집 화면에서 작성한 워크플로우를 "Save Workflow" 메뉴를 이용하여 저장합니다.


# Kafka 스트림으로 가공된 데이터를 Spark ML을 통해 5분 후 온도 예측하기 (TempPredictResult)

![13.01.05.00](https://i.imgur.com/X0AnPHQ.png)

HTTP RESTful 인터페이스를 통해서 스트림으로 입력되는 스트림데이터를 Kafka 스트림을 통해 전처리하고 SparkML을 통해 5분 후 온도를 예측하는 예제 입니다.

<br>

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | TempPredictResult | 워크플로우 이름     
description  | 5분 후 온도 예측하는 예제  | 워크플로우를 설명하는 글
isBatch  | false | 스트림 처리를 하는 워크플로우 이므로, false 로 지정
verbose  | true | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
runtype  | 즉시실행 |
Project | Hackathon 2021  |   워크플로우가 소속될 프로젝트 이름

<br>

### 엔진 선택

본 예제에서는 5개의 Engine을 사용하며 3개의 Engine은 13.01.03.Kafka_Write/Read_1Minute 예제를 활용하고, 2개의 Engine을 새로 추가합니다.

[13.01.03.Kafka_Write/Read_1Minute 예제 바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/manual_13.01.03.Kafka_WriteRead_1Minute.md)

![13.01.05_01](https://i.imgur.com/Z2ludwv.png)

<br>

#### 2번 Engine Writer 추가하기

![13.01.05_02](https://i.imgur.com/HGJ16RH.png)

**KafkaPipeWriter** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
mode  |  append |  새로 들어온 입력데이터에 대해서 처리함
trigger  |  5 seconds |  5초 동안 들어온 입력데이터에 대해서 처리함
bootStrapServers  | csle1:9092  |  Kafka 접속 주소 및 포트
zookeeperConnect  |  csle1:2181 |   Zookeeper 접속 주소 및 포트
topic  |  temp_output2_1 |  Kafka 큐의 이름
checkpointPath  |  dataset/output/temp/checkpoint/kafka1/  | 체크포인트 경로
failOnDataLoss  |  true  |  -

<br>

#### 네 번째 Engine

네 번째 Engine은  **MiniBatch** 엔진을 드래그&드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName
--|---|---
4  | MiniBatch  |   |

#### Reader

**KafkaReader** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
bootStrapServers  | csle1:9092  |  Kafka 접속 주소 및 포트
zookeeperConnect  |  csle1:2181 |   Zookeeper 접속 주소 및 포트
groupId  |  temp_gId_1 |  그룹 ID
topic  |  temp_output2_1 |  Kafka 큐의 이름

<br>

#### Writer

**KafkaWriter** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
bootStrapServers  | csle1:9092  |  Kafka 접속 주소 및 포트
zookeeperConnect  |  csle1:2181 |   Zookeeper 접속 주소 및 포트
groupId  |  temp_gId_1 |  그룹 ID
topic  |  tempPredict_1 |  Kafka 큐의 이름

<br>

#### Controller

**SparkStreamController** 를 드래그&드롭한 후 아래 표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
operationPeriod  |  2 |  Reader로 부터 데이터를 읽어올 주기
windowsSize  |  10  |  큐에서 사용할 윈도우의 크기
slidingSize  |  1  |  입력큐의 크기

<br>

#### Runner

**SimpleSparkRunner** 를 드래그&드롭합니다. 디폴트 속성값을 사용합니다.

<br>

#### Operator

네 번째 엔진에서는 6개의 Operator를 사용하여 입력데이터를 가공 및 온도를 예측합니다.

1. ColumnSelectOperator

field  |value   | 설명
--|:---:|--
selectColumnId  |  2  |  선택할 칼럼 ID

<br>

2. MinMaxScalingOperator

field  |value   | 설명
--|:---:|--
selectedColumnId  |  0  | 선택할 칼럼 ID
max  |  0.5  |  입력되는 값이 scaling 될 때 최대값
min  |  -0.5  |  입력되는 값이 scaling 될 때 최소값
withMinMaxRange  |  true  |  위에서 설정한 max, min 값 사용 여부
minRealValue  |  -20  |  온도 값에서 정상 범위로 사용할 최소값
maxRealValue  |  50  |  온도 값에서 정상 범위로 사용할 최대값

<br>

3. TransposeOperator

field  |value   | 설명
--|:---:|--
selectedColumnName  |  TEMP  |  transpose 연산을 적용할 컬럼의 이름

<br>

4. SparkMLPredictOperator

field  |value   | 설명
--|:---:|--
clsNameForModel  |  org.apache.spark.ml.PipelineModel  |  학습 모델의 형식
path  |  res://biuser/model/20200205-134939/automl_test.zip  |  학습 모델의 경로

<br>

5. MinMaxScalingOperator

field  |value   | 설명
|---|:---:|---|
selectedColumnId  |  13  |  선택할 칼럼 ID
max  |  50  |  입력되는 값이 scaling 될 때 최대값
min  | -20  |  입력되는 값이 scaling 될 때 최소값
withMinMaxRange  |  true  |  위에서 설정한 max, min 값 사용 여부
minRealValue  |  -0.5  |  온도 값에서 정상 범위로 사용할 최소값
maxRealValue  |  0.5  |  온도 값에서 정상 범위로 사용할 최대값

<br>

6. ColumnSelectOperator

|field|value|설명|
|---|:---:|---|
|selectedColumnId  |  13  | 선택할 칼럼 ID|

<br>

#### 다섯 번째 Engine

다섯 번째 Engine은  **MiniBatch** 엔진을 드래그&드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName
--|---|---
5  | MiniBatch  |   |

<br>

#### Reader

**KafkaReader** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
bootStrapServers  | csle1:9092  |  Kafka 접속 주소 및 포트
zookeeperConnect  |  csle1:2181 |   Zookeeper 접속 주소 및 포트
groupId  |  temp_gId_1 |  그룹 ID
topic  |  temp1_1 |  Kafka 큐의 이름

<br>

#### Writer

**FileBatchWriter** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
path  | dataset/simulation/output/temp_1Minute_1.csv  |  저장 경로
fileType  | CSV |   파일 형태
delimiter  | , |  구분자
saveMode  |  APPEND  |  저장 방식

<br>

#### Controller

**SparkStreamController** 를 드래그&드롭한 후 아래 표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
operationPeriod  |  1 |  Reader로 부터 데이터를 읽어올 주기
windowsSize  |  -1  |  큐에서 사용할 윈도우의 크기
slidingSize  |  -1  |  입력큐의 크기

<br>

#### Runner

**SimpleSparkRunner** 를 드래그&드롭합니다. 디폴트 속성값을 사용합니다.

<br>

#### Operator

다섯 번째 엔진에서는 3개의 Operator를 사용하여 4번째 Engine에서 5분 후 예측 온도 데이터를 가공하여 csv파일로 저장합니다.

1. AddValueColumnOperator

field  |value   | 설명
--|:---:|--
infoType  |  COLUMN_TIME  |  추가할 컬럼의 형태
columnName  |  time  |  추가할 컬럼 명

2. ChangeColumnDataTypeOperator

field  |value   | 설명
--|:---:|--
columName  |  time  |  데이터 타입을 변결할 컬럼
dataType  |  LONG  |  변경할 데이터 타입

3. ColumnSelectOperator

field  |value   | 설명
--|:---:|--
selectedColumnId  |  1, 0  |  |



## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![13.01.05_03](https://i.imgur.com/NTsjF7D.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![13.01.05_04](https://i.imgur.com/3LGuzWP.png)

<br>

### 워크플로우 모니터링 하기

<br>

#### 워크플로우 상태 확인
Atelier WebToolkit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다.

![13.01.05_05](https://i.imgur.com/AqYK1FD.png)

<br>


#### 워크플로우 로그 보기
**WorkFlow History** 탭을 선택하면, Atelier 프레임워크에서 워크플로우가 동작하며 발생시킨 로그 정보를 확인 할 수 있습니다.

첫 번째 Engine에서 HttpServerReaderWriter를 통해 입력받은 데이터를 KafkaWriter로 두 번째 Engine으로 전송하여 두 번째 Engine에서는 첫 번째 Operator에서 설정한 Window 단위인 1분, 오버랩 단위인 30초 즉, 1분 30초 동안 취득한 데이터를 평균 계산(AVG)하여 네 번째 Engine으로 전송합니다.  

네 번째 Engine의 실행 로그를 확인하면 두 번째 Engine으로부터 30초마다 데이터를 받아 버퍼에 저장하며 카운트 합니다.30초 단위의 10개의 데이터 즉, 5분의 데이터가 수신되면 5번 Engine으로 예측 결과를 전송합니다.

![13.01.05_06](https://i.imgur.com/VRwsj8H.png)

<br>

#### Storage에서 결과 확인하기

![13.01.05_07](https://i.imgur.com/REfpqEN.png)

![13.01.05_08](https://i.imgur.com/YrG9uzp.png)

<br>

## 워크플로우 종료하기
본 예제는 지속적으로 스트림데이터가 입력되므로 사용자가 워크플로우를 종료해야 합니다.  
Atelier WebToolkit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)합니다.

<br><br>
<br><br>

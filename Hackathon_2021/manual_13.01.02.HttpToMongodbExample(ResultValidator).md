# 스트림 데이터 적재하기 확인하기 (HttpToMongodbExample)

![13.02.01.000](https://i.imgur.com/XC9CuFi.png)

HTTP RESTful 인터페이스를 통해 MongoDB에 저장된 데이터를 확인하는 예제를 설명합니다.

<br>

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | HttpToMongodbExample(ResultValidator) | 워크플로우 이름     
description  | MongoDB에 적재된 데이터를 확인하는 예제  | 워크플로우를 설명하는 글
isBatch  | true | 스트림 처리를 하는 워크플로우 이므로, false 로 지정
verbose  | true | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
runtype  | 즉시실행 |
Project | Hackathon 2021  |   워크플로우가 소속될 프로젝트 이름

<br>

### 엔진 선택

본 예제에서는 **Batch** 엔진을 드래그앤 드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName
--|---|---
1  | Batch  | ProcessingEngine  |

<br>

#### Reader

HTTP 서버로부터 전달되어 오는 데이터를 입력 받기 위해 **HttpServerReader** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
serverAddress  | csle1:27018  |  MongoDB 서버 주소 및 포트  
dbName  |  mongodbexamples_1 |   DB 명  
collectionName  |  data_from_http |  테이블 명
cleaningDataInconsistency  |  true |  기존 테이블 삭제 및 재생성 여부

<br>

#### Writer

**FileBatchWriter** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
path  | dataset/simulation/output/mongoDBTable_1  |  저장 경로
fileType  | CSV |   파일 형태
delimiter  | , |  구분자
saveMode  |  OVERWRITE |  저장 방식

<br>

#### Controller

**SparkSessionController** 를 드래그&드롭한 후 아래 표와 같은 속성을 지정합니다.

<br>

#### Runner

**SimpleSparkRunner** 를 드래그&드롭합니다. 디폴트 속성값을 사용합니다.

<br>

#### Operator

본 예제에서는  Operator를 사용하지 않습니다.

<br><br>

## 워크플로우 실행 및 모니터링하기

<br>

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![13.01.02_01](https://i.imgur.com/bbJvQ0i.png)

<br>

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![13.01.02_02](https://i.imgur.com/isENyTB.png)

<br>

### 워크플로우 모니터링 하기

<br>

#### 워크플로우 상태 확인
Atelier WebToolKit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 작성한 워크플로우가 완료 (<span style="color:#6698FF">Finished</span>)인 것을 확인할 수 있습니다. 이 화면에서 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다.

![13.01.02_03](https://i.imgur.com/4Y6WEi5.png)

<br>


#### Storage에서 결과 확인하기
**Atelier WebToolKit의 Storage** 메뉴를 선택하면, MongoDB에 적재된 데이터를 csv파일로 저장한 정보를 확인 할 수 있습니다.

![13.01.02_04](https://i.imgur.com/XCrrjfd.png)

![13.01.02_05](https://i.imgur.com/pyfszRu.png)

<br><br>

## 워크플로우 종료하기
본 예제는 지속적으로 스트림데이터가 입력되므로 사용자가 워크플로우를 종료해야 합니다.
Atelier WebToolkit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)합니다.

<br><br>
<br><br>

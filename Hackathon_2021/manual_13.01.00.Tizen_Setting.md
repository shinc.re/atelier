
# Tizen 추가 설정

- 준비물
  1. Visual Studio 및 Tizen Studio
  2. Raspberry PI 3 or 4
  3. Micro SD Card 8GB (or More)
  4. Micro SD Card Reader
  5. Atelier Sample App Code

- 개발환경 준비
  - Visual Studio 및 Tizen Studio : https://tizenschool.org/tutorial/194/contents/1


## Atelier Sample App 프로젝트 설명

![sampleappproject](https://i.imgur.com/G6L6TU6.png)

|항목|설명|
|:---:|---|
|SampleNUIApp.cs|App UI 및 쓰레드 정의|
|testTopic1.cs|Topic1의 동작 함수 정의|
|testTopic2.cs|Topic2의 동작 함수 정의|
|tizen-manifest.xml|앱이 정의하는 메타파일 정보가 정의된 파일|
|shared/res/|Topic 1, 2에 필요한 센서 데이터 및 이미지 데이터
|^|sensor_data.txt : 온도, 습도, 미세먼지, VOC 4종 센서 데이터|
|^|sensor_data_temp.txt : 온도 센서 데이터|


<br>

## Atelier Sample App 프로젝트 추가 설정

Atelier는 **REST API**를 제공합니다. 그러므로 Atelier Sample App에서도 Wi-Fi를 사용하여 Atelier로 데이터를 전송합니다.
Atelier Sample App에서 **Internet Privileges**을 추가 설정하여 Raspberry PI와 Atelier간에 통신이 가능하도록 설정합니다.



1. Visual Studio 우측 솔루션 탐색기에서 **tizen-manifest.xml** 파일을 더블 클릭합니다.

![tizen-manifest](https://i.imgur.com/54RKdCH.png)

2. tizen-manifext.xml을 화면이 나타나면 좌측 메뉴에서 **Privileges** 메뉴를 선택하고 **Add** 버튼을 클릭합니다.

![privileges add](https://i.imgur.com/IP6bIeq.png)

3. Privileges 탭이 나타나면 **Search** 탭에서 ‘**internet**’ 을 검색합니다.
4. 검색 결과인 <a> <span style="color:black">**http://tizen.org/privilege/internet** </span></a>을 선택합니다.
5. 우측 하단에 ‘**OK**’버튼을 클릭 후 ‘**Ctrl + s**’ 입력하여 tizen-manifest.xml 수정 정보를 저장합니다.

![privileges save](https://i.imgur.com/YAAbpKd.png)

<br>

## Atelier Sample App Debugging

Atelier Sample App의 Code에서는 **tlog.Debug** 함수를 이용하여 Visual Studio의 SDB Command Prompt 통해 디버깅이 가능합니다.  
다음은 Visual Studio에서 SDB Command Prompt를 사용하기 위한 설명입니다.


1. Visual Studio의 상단 메뉴에서 도구 → Tizen → Tizen Sdb Command Prompt 클릭

![sdb_01](https://i.imgur.com/evbj7gc.png)

2. Tizen Sdb Command Prompt 클릭 후    다음과 같이 CMD Prompt 화면이 나타납니다.

![sdb_02](https://i.imgur.com/blERIpG.png)

3. Prompt 화면에 ‘sdb shell’ 입력  
     → dlogutil ATELIER & 입력  
     → Atelier Sample App 실행  

![sdb_03](https://i.imgur.com/d9upYQK.png)

4. Start Atelier Test Sample App!! 문구 확인.

<br><br>

# Topic - 1

Topic – 1은 온도 센서로부터 입력되는 데이터를 HTTP로 수집 후 Kafka를 통해 전처리를 수행하고, Spark ML을 통해 5분 후의 온도를 예측하는 예제입니다.

![topic1](https://i.imgur.com/AggFlIa.png)

## 예제 실행 방법 및 Source Code

![topic1_1](https://i.imgur.com/htNdq0T.png)

![topic1_2](https://i.imgur.com/Dv3AbBz.png)

![topic1_3](https://i.imgur.com/zCBYfTQ.png)


<br><br>

# Topic - 2

Atelier는 **REST API**을 제공하여, 파이썬 기반 사용자의 TensorFlow 코드를 파라미터를 전달하고, 서빙용으로 export된 TensorFlow 모델을 하둡 파일 시스템(HDFS)으로 복사해오는 방식을 통해 TensorFlow 모델을 연동할 수 있습니다.

이번 예제는 Sample App의 Topic2 Test를 통해 이미지를 전송하여 이미지의 추론 결과를 다시 Raspberry PI로 Serving하는 예제입니다

![topic2](https://i.imgur.com/YGqTlWM.png)

<br>

텐스플로(TensorFlow)는 구글(Google)에서 만든 오픈소스 소프트웨어 라이브러리로
딥러닝 프로그램을 쉽게 구현할 수 있도록 다양한 기능을 제공해주는 라이브러리 입니다.

TensorFlow Homepage : https://www.tensorflow.org/

![topic2_1](https://i.imgur.com/5rQj0jY.png)

<br>

이번 예제에서 사용하는 인셉션 모델(Inception Model)은 합성곱 신경망(CNN : Convolutional Neural Network) 모델 중 하나로 Google TensorFlow에서 무료로 배포하고 있는 모델입니다.

CNN 모델은 이미지 인식에서 주로 사용되고 있는 신경망이며, 다음과 같은 특징이 있습니다.

1. 이미지의 공간정보를 유지하면서 특징을 효과적으로 인식 및 분류
2. 학습 파라미터가 적음(재활용성 높음).

![topic2_2](https://i.imgur.com/da9V3gN.png)

### 예제 실행 방법 및 Source Code

![topic2_3](https://i.imgur.com/Pkwk2Ou.png)

![topic2_4](https://i.imgur.com/GaZNgQB.png)

![topic2_5](https://i.imgur.com/mRpOZ9H.png)


<br><br>
<br><br>

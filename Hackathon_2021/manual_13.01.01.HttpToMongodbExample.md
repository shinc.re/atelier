
# 스트림 데이터 적재하기 (HttpToMongodbExample)

![13.01.01_00](https://i.imgur.com/RKfSC86.png)

HTTP RESTful 인터페이스를 통해서 스트림으로 입력되는 스트림데이터를 MongoDB에 저장하는 예제를 설명합니다.

이 예제에서 MongoDB는 따로 스키마를 갖고 있지 않으며 Atelier Sample App에서 전송하는 JSON 데이터의 키-값에 따라 자동으로 으로 저장됩니다.


<br>

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | HttpToMongodbExample | 워크플로우 이름     
description  | 스트림 데이터를 MongoDB에 적재하는 예제  | 워크플로우를 설명하는 글
isBatch  | false | 스트림 처리를 하는 워크플로우 이므로, false 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
runtype  | 즉시실행 |
Project | Hackathon 2021  |   워크플로우가 소속될 프로젝트 이름

<br>

### 엔진 선택

본 예제에서는 스트림 데이터를 입력받아 처리한 뒤 MongoDB에 적재하므로 **MiniBatch** 엔진을 드래그&드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName
--|---|---
1  | MiniBatch  | ProcessingEngine  |

<br>

#### Reader

HTTP 서버로부터 전달되어 오는 데이터를 입력 받기 위해 **HttpServerReader** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
port  |  53001 |  HTTP 서버의 포트번호를 입력합니다.
ip  |  0.0.0.0 |  HTTP 서버의 주소를 입력합니다.

<br>

#### Writer

**MongodbMiniBatchWriter** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
serverAddress  | csle1:27018  |  MongoDB 서버 주소 및 포트  
dbName  |  mongodbexamples_1 |   DB 명  
collectionName  |  data_from_http |  생성할 테이블 명
cleaningDataInconsistency  |  true |  기존 테이블 삭제 및 재생성 여부

<br>

#### Controller

**SparkStreamController** 를 드래그&드롭한 후 아래 표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
operationPeriod  |  3 |  스트리밍 처리 주기 (초 단위)

<br>

#### Runner

**SimpleSparkRunner** 를 드래그&드롭합니다. 디폴트 속성값을 사용합니다.

<br>

#### Operator

본 예제에서는 스트림 입력을 그대로 MongoDB에 적재하므로 Operator를 사용하지 않습니다. 만약 스트림 입력 데이터를 가공해야 할 경우, 필요한 Operator를 드래그앤 드롭하여 속성값을 지정한 후 사용할 수 있습니다.

<br>

![13.01.01_01](https://i.imgur.com/tZTTaIP.png)

atelier 계정으로 접속하면 예제 워크플로우가 만들어져있습니다. **HttpToMongodbExample** 을 불러오기하여 사용할 수 있습니다.

![13.01.01_02](https://i.imgur.com/CabRF4X.png)

<br><br>

## 워크플로우 실행 및 모니터링하기

<br>

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![13.01.01_03](https://i.imgur.com/PVBZDnZ.png)

<br>

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![13.01.01_04](https://i.imgur.com/oohGfnz.png)

<br>

### 워크플로우 모니터링 하기

<br>

#### 워크플로우 상태 확인
Atelier WebToolkit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다.

![13.01.01_05](https://i.imgur.com/cHArCR1.png)

<br>


#### 워크플로우 로그 보기
**WorkFlow History** 탭을 선택하면, Atelier 프레임워크에서 워크플로우가 동작하며 발생시킨 로그 정보를 확인 할 수 있습니다.

![13.01.01_06](https://i.imgur.com/VSVXol0.png)

<br><br>

## 결과 확인하기

Visual Studio에서 연결한 Tizen Sdb Command Prompt를 통해 Data의 송수신 결과를 확인할 수 있습니다.  
OK! I got it. 라는 문구가 출력 되었다면 정상적으로 동작한 것입니다.

![13.01.01_07](https://i.imgur.com/LOLFWnk.png)




## 워크플로우 종료하기
본 예제는 지속적으로 스트림데이터가 입력되므로 사용자가 워크플로우를 종료해야 합니다.
Atelier WebToolkit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)합니다.

<br><br>
<br><br>

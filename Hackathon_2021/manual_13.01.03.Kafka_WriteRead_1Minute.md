
# Kafka 스트림을 통한 데이터 전처리 (Kafka_WriteRead_1Minute)

![13.01.03.00](https://i.imgur.com/xfk5Uu9.png)

HTTP RESTful 인터페이스를 통해서 스트림으로 입력되는 스트림데이터를 Kafka 스트림을 통해 전처리하는 예제 입니다.

<br>

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | Kafka_Write/Read_1Minute | 워크플로우 이름     
description  | 스트림 데이터를 MongoDB에 적재하는 예제  | 워크플로우를 설명하는 글
isBatch  | false | 스트림 처리를 하는 워크플로우 이므로, false 로 지정
verbose  | true | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
runtype  | 즉시실행 |
Project | Hackathon 2021  |   워크플로우가 소속될 프로젝트 이름

<br>

### 엔진 선택

본 예제에서는 3개의 Engine을 사용합니다.

#### 첫 번째 Engine

첫 번째 Engine은  **MiniBatch** 엔진을 드래그앤 드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName
--|---|---
1  | MiniBatch  |   |

<br>

#### Reader

HTTP 서버로부터 전달되어 오는 데이터를 입력 받기 위해 **HttpServerReader** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
port  |  53001 |  HTTP 서버의 포트번호를 입력합니다.
ip  |  0.0.0.0 |  HTTP 서버의 주소를 입력합니다.

<br>

#### Writer

**KafkaWriter** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
bootStrapServers  | csle1:9092  |  Kafka 접속 주소 및 포트
zookeeperConnect  |  csle1:2181 |   Zookeeper 접속 주소 및 포트
groupId  |  temp_gId_1 |  그룹 ID
topic  |  temp1_1 |  Kafka 큐의 이름

<br>

#### Controller

**SparkStreamController** 를 드래그&드롭한 후 아래 표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
operationPeriod  |  3 |  스트리밍 처리 주기 (초 단위)

<br>

#### Runner

**SimpleSparkRunner** 를 드래그&드롭합니다. 디폴트 속성값을 사용합니다.

<br>

#### Operator

첫 번째 Engine은 Operator를 사용하지 않습니다.


<br><br>

#### 두 번째 Engine

두 번째 Engine은  **Stream** 엔진을 드래그&드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName
--|---|---
2  | Stream  |   |

#### Reader

**KafkaPipeReader** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
bootStrapServers  | csle1:9092  |  Kafka 접속 주소 및 포트
zookeeperConnect  |  csle1:2181 |   Zookeeper 접속 주소 및 포트
topic  |  temp1_1 |  Kafka 큐의 이름
addTimestamp  |  true |  입력데이터를 받은 시간을 DataFrame 에 추가하고 싶을 경우, true 로 지정
timestampName  |  SENSOR_INPUT_TIME |  Timestamp 이름
watemark  |  1 minutes |  늦게 들어오는 입력데이터를 얼마동안 기다릴 것인지 입력
sampleJsonPath  |  dataset/input/temp/tempStreamingSplitSample.json |  입력 데이터 형태 지정
failOnDataLoss  |  false |  -
timeFormat | yyyy-MM-dd'T'HH:mm:ss | 시간 포맷


<br>

#### Writer

**KafkaPipeWriter** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
mode  |  append |  새로 들어온 입력데이터에 대해서 처리함
trigger  |  5 seconds |  5초 동안 들어온 입력데이터에 대해서 처리함
bootStrapServers  | csle1:9092  |  Kafka 접속 주소 및 포트
zookeeperConnect  |  csle1:2181 |   Zookeeper 접속 주소 및 포트
topic  |  temp_output1_1 |  Kafka 큐의 이름
checkpointPath  |  dataset/output/temp/checkpoint/kafka1/  | 체크포인트 경로
failOnDataLoss  |  true  |  -

<br>

#### Controller

**StreamingGenericController** 를 드래그&드롭합니다.

<br>

#### Runner

**SimpleSparkRunner** 를 드래그&드롭합니다. 디폴트 속성값을 사용합니다.

<br>

#### Operator

두 번째 엔진에서는 3개의 Operator를 사용하여 입력데이터를 가공합니다.

1. GroupByPipeOperator

field  |value   | 설명
--|:---:|--
timeColName  |  SENSOR_INPUT_TIME  |  시간정보가 들어있는 칼럼 이름
keyColName  |  DEVICE_ID  | Key 칼럼 이름
valColName  |  TEMP  |  계산할 칼럼
groupby  |  AVG  |  평균 계산
window |  세부설정  | 아래 표 참고

<br>

field  |value   | 설명
--|:---:|--
key  |  SENSOR_INPUT_TIME  |  윈도우를 사용할 칼럼 이름
windowsLength  |  1 minutes  |  윈도우 단위 설정
slidingInterval  |  30 seconds  |  오버랩 설정

<br>

2. SelectColumnsPipeOperator

field  |value   | 설명
--|:---:|--
colName  |  DEVICE_ID / windows.start / TEMP | 선택할 칼럼 이름
isRename  |  false  |  -

<br>

3. RenameColumnsPipeOperator

field  |value   | 설명
--|:---:|--
renameColumns  |  세부 설정  |  아래 표 참고

<br>

field  |value   | 설명
--|:---:|--
selectedColIndex  |  1  |  window.start 칼럼 선택 (0번부터 시작함)
newColName  |  SENSOR_INPUT_TIME  |  변경할 칼럼 이름
newFieldType  |  STRING  |  변경할 칼럼 타입

<br>

#### 세 번째 Engine

세 번째 Engine은  **MiniBatch** 엔진을 드래그&드롭합니다.

- 엔진 속성

순번  | 엔진 Type | NickName
--|---|---
3  | MiniBatch  |   |

<br>

#### Reader

**KafkaReader** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
bootStrapServers  | csle1:9092  |  Kafka 접속 주소 및 포트
zookeeperConnect  |  csle1:2181 |   Zookeeper 접속 주소 및 포트
groupId  |  temp_gId_1 |  그룹 ID
topic  |  temp1_1 |  Kafka 큐의 이름

<br>

#### Writer

**FileBatchWriter** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
path  | dataset/simulation/output/temp_1Minute_1.csv  |  저장 경로
fileType  | CSV |   파일 형태
delimiter  | , |  구분자
saveMode  |  APPEND  |  저장 방식

<br>

#### Controller

**SparkStreamController** 를 드래그&드롭한 후 아래 표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
operationPeriod  |  3 |  스트리밍 처리 주기 (초 단위)

<br>

#### Runner

**SimpleSparkRunner** 를 드래그&드롭합니다. 디폴트 속성값을 사용합니다.

<br>

#### Operator

세 번째 Engine은 Operator를 사용하지 않습니다.


<br><br>

## 워크플로우 실행 및 모니터링하기

<br>

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

<br>

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

<이미지 넣기>


<이미지 넣기>

<br>

### 워크플로우 모니터링 하기

<br>

#### 워크플로우 상태 확인
Atelier WebToolkit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다.

<이미지 넣기>

<br>


#### 워크플로우 로그 보기
**WorkFlow History** 탭을 선택하면, Atelier 프레임워크에서 워크플로우가 동작하며 발생시킨 로그 정보를 확인 할 수 있습니다.

첫 번째 Engine에서 HttpServerReaderWriter를 통해 입력받은 데이터를 KafkaWriter로 두 번째 Engine으로 전송하여 두 번째 Engine에서는 첫 번째 Operator에서 설정한 Window 단위인 1분, 오버랩 단위인 30초 즉, 1분 30초 동안 취득한 데이터를 평균 계산(AVG)하여 세 번째 Engine으로 전송합니다.
세 번째 Engine은 수신 받은 데이터를 csv파일로 저장합니다.

**첫 번째 Engine 실행로드**  

![13.01.03_03](https://i.imgur.com/JrWAmFQ.png)

**세 번째 Engine 실행로드**  

![13.01.03_05](https://i.imgur.com/vDFFDFR.png)

<br>

#### Storage에서 결과 확인하기
**Atelier WebToolKit의 Storage** 메뉴를 선택하여, Kafka 스트림을 통해 가공되어진 데이터를 csv파일로 저장한 정보를 확인 할 수 있습니다.

![13.01.03_06](https://i.imgur.com/PWh1s5r.png)

![13.01.03_07](https://i.imgur.com/lDkrVCY.png)

<br><br>

## 워크플로우 종료하기
본 예제는 지속적으로 스트림데이터가 입력되므로 사용자가 워크플로우를 종료해야 합니다.
Atelier WebToolkit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)합니다.

<br><br>
<br><br>

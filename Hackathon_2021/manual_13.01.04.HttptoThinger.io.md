
# 타 IoT Platform과 연동하기 (<a> <span style="color:black">HttptoThinger.io</span></a>)

![13.01.04.000](https://i.imgur.com/1wC3Cvv.png)

HTTP RESTful 인터페이스를 통해서 스트림으로 입력되는 스트림데이터를 Kafka 스트림을 통해 전처리하여 타 IoT Platform으로 연동하는 예제 입니다.

<br>

## 워크플로우 생성하기
워크플로우 편집화면을 이용하여 아래의 과정을 통해 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | HttptoThinger,io | 워크플로우 이름     
description  | 타 IoT Platform과 연동하는 예제  | 워크플로우를 설명하는 글
isBatch  | false | 스트림 처리를 하는 워크플로우 이므로, false 로 지정
verbose  | true | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
runtype  | 즉시실행 |
Project | Hackathon 2021  |   워크플로우가 소속될 프로젝트 이름

<br>

### 엔진 선택

본 예제에서는 3개의 Engine을 사용하며 13.01.03.Kafka_Write/Read_1Minute 예제를 활용하고, 세 번째 Engine의 Writer만 수정하여 타 IoT Platform과 연동합니다.

[13.01.03.Kafka_Write/Read_1Minute 예제 바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/manual_13.01.03.Kafka_WriteRead_1Minute.md)

#### Writer

**HttpClientWriter** 를 드래그&드롭한 후 아래표와 같은 속성을 지정합니다.

field  |value   | 설명
--|:---:|--
url  | thingerio url  |  아래 설명에서 참고
methodType  |  _POST  |  데이터 전송 형태
header  |  세부 설정 |  아래 표 참고

<br>

field  |value   | 설명
--|:---:|--
name  |  Content-Type  |  데이터 형태
value  |  _POST  |  데이터 전송 형태
name  |  charset  |  문자 포맷
value  |  UTF-8  |  UTF-8 설정

<br>

![13.01.04_00](https://i.imgur.com/YIuAM9L.png)

![13.01.04_001](https://i.imgur.com/0d9tnbZ.png)

<br><br>

## Thinger io

URL : https://thinger.io/

![13.01.04_01](https://i.imgur.com/fsOVCAY.jpg)

<br>

#### 회원 가입 후 로그인

![13.01.04_02](https://i.imgur.com/MSU51Cj.png)

<br>

#### 디바이스 생성하기

Thingerio에서 총 2개의 Device를 생성합니다.

로그인 후 좌측메뉴에서 Devices를 클릭 후 Add Device 버튼을 클릭합니다.

![13.01.04_03](https://i.imgur.com/MwyCgZ4.png)

<br>

첫 번째 Device는 Atelier로부터 데이터를 수신 받을 Device를 생성합니다.  
Device Type을 HTTP Device를 선택하고, Device ID와 Device Name, Device Description을 입력 후 Add Device를 클릭합니다.

![13.01.04_04](https://i.imgur.com/3K321nR.png)

<br>

두 번째 Device는 첫 번 째 Device의 데이터를 저장할 Device입니다.  
Device Type을 ‘Generic Thinger Device’를 선택하고, Device ID와 Device Credentials, Device Name, Device Description을 입력 후 Add Device를 클릭합니다.

![13.01.04_05](https://i.imgur.com/dslG6Rh.png)

<br>

#### 데이터 저장소 생성하기

두 번째로 생성한 Device의 데이터 저장소를 생성합니다.  
좌측 메뉴에서 Data Buckets를 클릭 후 Add Bucket을
클릭합니다.

1. Bucket Id와, Bucket Name, Bucket Description을입력.
2. Data Source에서 ‘From Device Resource’를 선택.
3. Select Device에서 두 번째로 생성한 Device를 선택.
4. Enter Resource Name : ‘TEMP’ 입력
5. Sampling Interval : 1 minutes
6. Add Bucket 버튼 클릭

![13.01.04_06](https://i.imgur.com/1zGq0V9.png)

<br>

#### Thinger io URL 생성하기

Atelier에서 Thinger.io로 데이터를 전송하기 위해 첫 번째로 생성한 Device의 추가 설정을 진행하겠습니다.

1. 좌측 메뉴에서 Devices 클릭 후 첫 번째로 생성한 Device 선택
2. 우측 상단에 Callback 메뉴 클릭
3. Write Bucket : 이전에 만든 Temp_Save_Data 선택
4. Authorization : 체크 박스 클릭(자동 생성)
5. Save 버튼 클릭하여 저장

![13.01.04_07](https://i.imgur.com/blkWR5b.png)

<br>

첫 번째 Device의 추가 설정 후 상단에 Overview 메뉴를 클릭 후 나오는 정보를 이용하여 Atelier에서 전송할 Thinger.io의 URL을 작성합니다.  

URL 작성은 다음과 같습니다.

<span style="color:red">Method</span>?authorization=<span style="color:red">Authorization Header</span>  (※Bearer 제외)

![13.01.04_08](https://i.imgur.com/VBoIVrv.png)

<br>

Ex)

<a><span style="color:red">https://backend.thinger.io/v3/users/shinc/devices/Temp_Http_Rx/callback/data</span></a>?authorization=<span style="color:red">eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiJEZXZpY2VDYWxsYmFja19UZW1wX0h0dHBfUngiLCJ1c3IiOiJzaGluYyJ9.SKzJzBpcEcNUtSdwTnbUOVpu78go0utuWa-bPHcvMQA</span>

<br><br>

## 워크플로우 실행 및 모니터링하기

<br>

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![13.01.04_15](https://i.imgur.com/43Mpnmt.png)

<br>

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![13.01.04_16](https://i.imgur.com/iEpByFB.png)

<br>

### 워크플로우 모니터링 하기

#### 워크플로우 상태 확인
Atelier WebToolkit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 작성한 워크플로우가 실행 중 (<span style="color:#6698FF">Running</span>)인 것을 확인할 수 있습니다. 이 화면에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)하거나, 다시 실행(<span style="color:#6698FF">&#9654;</span>)할 수 있습니다.

![13.01.04_17](https://i.imgur.com/Oe9xNgB.png)

<br>

#### Thinger.io에서 결과 확인하기

**Thinger io**의 Data Buckets 메뉴에서 Atlier로부터 수신받은 데이터를 확인 할 수 있습니다.

![13.01.04_09](https://i.imgur.com/lEnGtSW.png)

<br>

#### Data Bucket 데이터 시각화 하기

Thinger.io 좌측 메뉴에서 ‘Dashboards’를 클릭 후 ‘Add DashBoard’를 클릭합니다.  
생성할 Dashboard의 정보를 입력 후 ‘Add Dashboard’ 버튼을 클릭하여 생성합니다.

![13.01.04_10](https://i.imgur.com/MEtPYTk.png)

<br>

Dashboard 생성 후 다시 좌측 메뉴에서 Dashboards를 클릭하여 생성한 Dashboard를 선택합니다.  
Dashboard가 비어 있다는 알림을 확인할 수 있으며, 우측 상단에 슬라이드 버튼을 클릭하면,
아래의 이미지와 같이 상단에 메뉴가 나타납니다.  
Add Widget을 통해 Data Bucket의 데이터를 실시간으로 확인할 수 있는 차트를 생성할 수 있습니다.

![13.01.04_11](https://i.imgur.com/3DvFwEO.png)

<br>

Add Widget 클릭하면 ‘Widget Settings’ 화면을 확인할 수 있습니다.  
Widget 탭에서 Title을 입력하고, Type을 ‘Time Series Chart’를 선택합니다.

![13.01.04_12](https://i.imgur.com/JTfsanB.png)

<br>

다음은 Time Series Chart 탭에서 다음과 같이 설정합니다.

1. Widget Input : From Bucket
2. Select Bucket : Temp_Save_Data
3. Select Fields to Plot : TEMP
4. Timeframe : Relative
5. Time Period : latest, 1 Days
6. Save


![13.01.04_13](https://i.imgur.com/L8i4h4f.png)

<br>

Save 버튼 클릭 후 Data Bucket의 저장되고 있는 Data를 아래의 이미지와 같이 차트로 확인할 수 있습니다.

![13.01.04_14](https://i.imgur.com/5kMm32E.png)

<br>

## 워크플로우 종료하기
본 예제는 지속적으로 스트림데이터가 입력되므로 사용자가 워크플로우를 종료해야 합니다.  
Atelier WebToolkit 상단의 **Monitoring** 메뉴를 클릭하여 진행내역/상태모니터링 화면으로 이동합니다. **Workflow** 탭에서 실행 중인 워크플로우를 종료(<span style="color:red">&#9724;</span>)합니다.

<br><br>
<br><br>

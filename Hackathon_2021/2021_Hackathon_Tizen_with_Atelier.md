# Hackathon 2021 Tizen with Atelier

![main](https://i.imgur.com/9GxB5S1.png)

<br><br>

## Atelier Sample App Download

Atelier Sample App은 아래의 '**Atelier Sample App 다운로드**'를 클릭 후 우측 상단에 다운로드 버튼을 클릭하여 다운로드 하실 수 있습니다.

[Atelier Sample App 다운로드](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/AtelierSampleApp.zip)

![Atelier_Sample_App_Download](https://i.imgur.com/QgWsZe2.png)

<br>

## Atelier 동영상 자료 Download

AI Framework Atelier 소개 영상 : [바로가기](https://www.youtube.com/watch?v=gc66T89_F5w)

AI Framework Atelier 기능 소개 및 실습 영상 : [바로가기](https://www.youtube.com/watch?v=UK4OqZGwsC4)

---

Atelier 동영상자료는 아래의 '**Atelier 동영상 자료**'를 클릭 후 우측 상단에 다운로드 버튼을 클릭하여 다운로드 하실 수 있습니다.

[Atelier 동영상 자료 다운로드](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/Hackathon_2021_Atelier_with_Tizen_.pdf)

![PDF_Download](https://i.imgur.com/ePFq8Ri.png)

<br>

## Hackathon Workflow Example

### Topic - 1 Example

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|0|13.01.00.Tizen 추가 설정 및 Topic 소개|Tizen 추가 설정 및 Topic 소개 문서 입니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/manual_13.01.00.Tizen_Setting.md)|
|1|13.01.01.HttpToMongodbExample|HTTP RESTful 인터페이스를 통해서 스트림으로 입력되는 스트림데이터를 MongoDB에 저장하는 예제를 설명합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/manual_13.01.01.HttpToMongodbExample.md)|
|2|13.01.02.HttpToMongodbExample(ResultValidator)|MongoDB에 적재된 데이터를 읽어 csv파일로 저장하는 예제입니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/manual_13.01.02.HttpToMongodbExample(ResultValidator).md)|
|3|13.01.03.Kafka_Write/Read_1Minute|HTTP RESTful 인터페이스를 통해서 스트림으로 입력되는 스트림데이터를 Kafka 스트림을 통해 전처리하는 예제 입니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/manual_13.01.03.Kafka_WriteRead_1Minute.md)|
|4|<a><span style="color:black">13.01.04.HttptoThinger.io</a></span>|HTTP RESTful 인터페이스를 통해서 스트림으로 입력되는 스트림데이터를 Kafka 스트림을 통해 전처리하여 타 IoT Platform으로 전송하는 예제입니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/manual_13.01.04.HttptoThinger.io.md)|
|5|13.01.05.TempPredictResult|온도 센서로부터 입력되는 데이터를 수집 후 전처리를 수행하여 Spark ML 모델에게 스트림 형태로  5분 후의 온도 예측을 요청하고, 온도 예측 결과를 Kafka 스트림형태로 받는 예제를 설명합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/manual_13.01.05.TempPredictResult.md)|

<br>

### Topic - 2 Eaxmple

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|13.02.01.InceptionServingExample|OnDemandServing 엔진을 활용한 TensorFlow 기반 Inception 모델을 이용한 이미지 분류 RESTful 서비스 예제를 소개합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/manual_13.02.01.InceptionServingExample.md)|

<br><br>
<br><br>

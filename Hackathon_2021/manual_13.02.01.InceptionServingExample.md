
# 온디맨드 방식으로 Inception 모델 서빙하기 (InceptionServingExample)
---
텐서플로 기반 Inception 모델을 이용한 이미지 분류 RESTful 서비스 예제를 소개합니다.

![13.02.01_00](https://i.imgur.com/EpWmIOC.png)

<br>

## 모델 등록하기
웹툴킷 화면에서 [Storage] 탭을 클릭하여, Inception 모델을 등록합니다.

웹툴킷 화면에서 모델이 model/model/inception/ 경로에 등록된 것을 확인할 수 있습니다.

![13.02.01_1_01](https://i.imgur.com/pMBe8is.png)

<br><br>

## 워크플로우 생성하기
워크플로우 편집기를 이용하여, 다음의 절차에 따라 워크플로우를 생성합니다.

- 워크플로우 속성

속성  | 값  | 비고
--|---|--
name  | InceptionServingExample | 워크플로우 이름     
description  | 텐서플로우 기반 딥러닝 모델을 온디맨드 서빙하는 예제  | 워크플로우를 설명하는 글
isBatch  | true | 배치 처리를 하는 워크플로우 이므로, true 로 지정
verbose  | false | 디버깅을 위해 로그정보를 보고자할 경우, true 로 지정
RunType | 즉시실행 |   
Project | Hackathon 2021  |   워크플로우가 소속될 프로젝트 이름

<br><br>

### 엔진 선택
본 예제는 온 디맨드 타입의 분류 서비스를 제공하므로, **OnDemandServing** 엔진을 선택합니다.

#### Reader
본 예제에서는 Reader를 사용하지 않습니다.

#### Writer
본 예제에서는 Writer를 사용하지 않습니다.

#### Controller
본 예제의 Controller는 **TensorflowServingController** 를 사용하고, 속성을 기본 값으로 설정합니다.

#### Runner
본 예제의 Runner는 **TensorflowServingRunner** 를 사용하고, 속성을 다음과 같이 설정합니다.

field  |value   | 설명
--|---|--
modelBasePath  |  model/model/inception/ |  모델 경로
port  |  8002 |  이미지 분류 결과를 서비스할 포트 번호
modelName  |   inception |  서비스에 사용할 inception 모델의 이름


![13.02.01_2_01](https://i.imgur.com/SBYpJFF.png)

<br>

#### Operator
본 예제의 Operator는 **TensorflowServingOperator** 를 사용하고, 속성을 기본 값으로 설정합니다.

완성된 워크플로우는 다음과 같습니다.

![13.02.01_03](https://i.imgur.com/kHmAiXK.png)

<br>


<br><br>

## 워크플로우 실행 및 모니터링하기

### 워크플로우 빌드하기
위에서 작성한 워크플로우를 빌드하기 위해서는 Workbench 운영화면의 **빌드** 버튼을 클릭합니다. 빌드결과는 **Engine빌드로그** 창에서 확인할 수 있습니다.

![13.02.01_05](https://i.imgur.com/1fkcMOl.png)

### 워크플로우 배포하기
위에서 작성한 워크플로우를 실행하기 위해서는 위해서는 Workbench 운영화면의 **배포** 버튼을 클릭합니다. 배포결과는 **Engine실행로그**, **Engine드라이버로그** 창에서 확인할 수 있습니다.

![13.02.01_06](https://i.imgur.com/kGbmKmK.png)

<br><br>

### 워크플로우 모니터링 하기

<br>

#### 워크플로우 상태 확인
웹툴킷 [Monitoring] 화면에서 [Workflow] 탭을 선택하여, 위에서 실행한 워크플로우와 엔진의 동작 상태를 확인할 수 있습니다.
정상적으로 실행되어 Status 값이 Running 인 것을 확인할 수 있습니다.

![13.02.01_07](https://i.imgur.com/aLe1mV5.png)

<br>

#### 워크플로우 로그 보기
[Workflow History] 탭을 선택하여, 현재 동작 중인 워크플로우의 로그 정보를 확인할 수 있습니다.

![13.02.01_08](https://i.imgur.com/XAwCqet.png)

<br><br>

## 결과 확인하기

Visual Studio에서 연결한 Tizen Sdb Command Prompt를 통해 이미지 추론 결과를 확인할 수 있습니다.  
이미지 추론 결과는 이미지 추론의 Score가 가장 높은 순서로 5개의 분류 카테고리와 점수를 알려줍니다.

![13.02.01_09](https://i.imgur.com/U3e1BrN.png)

<br>

## 워크플로우 종료하기
웹툴킷 [Monitoring] 화면의 [Workflow] 탭에서, 현재 Status가 Inprogress 인 InceptionServingExample 워크플로우의 정지(<span style="color:red">&#9724;</span>) 버튼을 클릭하여 종료시킵니다.

<br>

## 워크플로우 저장하기
워크플로우 편집기에서 작성한 워크플로우를 "Save Workflow" 메뉴를 이용하여 저장합니다.

<br><br>

---

<br><br>

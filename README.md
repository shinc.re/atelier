# Welcome to Atelier

<br>

![00](https://i.imgur.com/hKmAIEC.png)

## Atelier WebToolKit 매뉴얼

|제목|내용|링크|
|---|---|---|
|Atelier WebToolKit 사용 방법|Atelier WebToolKit을 편리하게 사용할 수 있도록 도움을 주는 도구인 Atelier WebToolKit 사용방법을 소개합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Atelier_WebToolKit_Manual.md)|

<br><br>

### 개념 및 구조

- Atelier는 IoT, 빅데이터, 기계학습 및 도메인 지식을 융합하여 다양한 인공지능 서비스 구축에 필요한 공통기능을 제공하는 인공지능 프레임워크입니다.
- Atelier는 분산병렬 인프라를 기반으로, 다양한 데이터 소스의 수집으로부터 도메인 응용서비스 제공까지의 전주기적 솔루션을 지향합니다.
- Atelier는 멀티모델 데이터처리, 멀티모델 동시학습 및 추론, 자동기계학습, 분산병렬학습 및 추론, 학습모델 및 도메인지식의 탑재·연동 등의 기능을 제공합니다.
- 각각의 기능들은, 워크플로우를 이용하여 선택·조합·실행·서빙 및 재활용이 가능하도록 설계되었습니다.

<br>

### 특장점

- 지속성 : SW · Data · Analytics · 경험 등 기업의 데이터를 활용한 인공지능 기술력 내재화 가능
- 다양성 : SW 컴퍼넌트들의 조합으로 다양한 응용 서비스 시스템 구성 가능
- 재활용성 : 워크플로우 재활용 및 공유 기능 제공
- 확장성 : 개발자 API를 이용한 3rd Party 컴퍼넌트 개발 및 등록 인터페이스 제공
- 편의성 :
  1. Web 기반의 DIY Workflow 제작도구 및 모니터링 · 실행 · 제어 인터페이스제공
  2. Stand-alone 실행환경과 웹툴킷 제작 환경을 포함한 컨테이너 기반 Toolbox 제공
  3. 모델 학습과 Serving Process 일원화

<br><br>

## Workflow Example::데이터 수집 및 적재

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|스트림 데이터를 배치 저장소에 적재하기|HTTP RESTful 인터페이스를 통해서 스트림으로 입력되는 스트림데이터를  MongoDB에 저장하는 예제를 설명합니다. MiniBatch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example1/manual_2.5.1.HttpToMongodbExample.md)|
|2|배치 데이터를 배치 저장소에 적재하기|관계형데이터베이스에 저장되어있는 테이블의 내용을 읽고  관계형 데이터베이스의 다른 테이블에 저장하는 예제를 설명합니다. Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example1/manual_2.5.19.DBtoDBExample.md)|
|3|외부 플랫폼 데이터를 Atelier Storage에 적재 하기|EdgeX 플랫폼으로부터 스트림으로 입력되는 데이터를 파일로 저장하는 예제를 설명합니다.   MiniBatch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example1/manual_2.5.15.EdgeXToFileExample.md)|

<br>

## Workflow Example::데이터 처리 및 학습 데이터 생성

#### 데이터 배치 처리

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|데이터 배치 처리 하기|CSV 파일을 읽어서, KMeans 클러스터링을 수행하는 배치데이터 처리 예제를 설명합니다.   Batch 엔진을 활용 합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example2/manual_2.5.2.KMeansExample.md)|
|2|OAuth 2.0 기반 외부 플랫폼의 OpenAPI 를 활용하여 데이터 배치 처리하기|OAuth 2.0 기반 외부 플랫폼의 OpenAPI 를 활용하여   데이터를 수집하고 KMeans 클러스터링 처리하는 예제를 설명합니다.   외부 플랫폼은 medbiz 헬스케어플랫폼을 사용합니다.  Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example2/manual_2.5.8.KMeansMedbizEdgeExample.md)|
|3|비식별 처리하기|원본 데이터에 총계처리, 가명처리, 부분식별자삭제, 암호화 비식별화 기능을 적용한 예제를 소개합니다.  Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example2/manual_2.5.3.DeidentificationExample.md)|

<br>

#### 스트림 / 실시간 스트림 데이터 처리

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|스트림데이터 처리하기|교통센서로부터 수집되는 실시간 속도 스트림 데이터가 5분 단위 평균 속도 스트림 데이터로 정제되어 입력될 때, 해당 스트림 데이터를 텐서플로우 학습코드의 입력 데이터 형태로 전처리하고, 파일로 저장하는 예제를 설명합니다. MiniBatch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example3/manual_2.6.1.TrafficPreprocessing.md)|
|2|실시간 스트림데이터 처리하기|교통센서로부터 동시에 무작위로 들어오는 실시간 속도 스트림 데이터를 수집하여 시간 순으로 데이터를 처리해서 5분 단위 평균 속도로 스트림데이터로 전처리하는 예제를 설명합니다. Stream 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example3/manual_2.6.5.TrafficTimeseriesProcessing.md)|

<br>

#### Python 사용자 정의 함수를 활용한 데이터 처리

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|PythonUDF 기반 이미지 처리하기|Python 사용자 정의 함수를 사용하여 Atelier 에서 이미지를 전처리하는 예제를 설명합니다.  이 기능을 이용하면 이미지 이외에도 json 문자열, nparray 등 다양한 형태의 자료를 Python 기반으로 처리할 수 있습니다. OnDemandExternalServing 을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example4/manual_2.9.3.PythonUDFExample.md)|
|2|Dockerize 기능 설명|사용자가 작성한 Python 모듈을 Atelier Framework에 탑재하기 위해 Docker 이미지로 만드는 방법에 대해 설명합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example4/manual_2.9.2.KSB_Dockerize.md)|

<br>


## Workflow Example::학습 모델 생성하기


#### SparkML을 활용한 머신러닝 모델 학습

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|Atelier Spark ML 이해하기|Atelier에서 제공하는 Spark Machine Learning을 활용한 기계학습 모델 생성 방법을 소개합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example5/manual_1.0.0.SparkML.md)|
|2|회귀 분석 모델 생성하기|보스턴 주택 가격 데이터 셋을 활용하여 회귀 분석 모델을 생성하는 방법을 소개합니다.  Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example5/manual_1.0.1.Regression.md)|
|3|다중 분류 작업 학습 모델 생성하기|iris 데이터 셋을 활용하여 다중 분류 작업 모델을 생성하는 방법을 소개합니다.  Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example5/manual_1.0.2.MulticlassClassification.md)|
|4|이진 분류 작업 학습 모델 생성하기|Adult 데이터 셋을 활용하여 이진 분류 작업 모델을 생성하는 방법을 소개합니다.  Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example5/manual_1.0.3.BinaryClassification.md)|


<br>

#### PySpark를 활용한 머신러닝 모델 학습

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|ML모델 학습하기|Atelier 에서 PySpark 을 이용하여 머신러닝 모델을 학습 하는 기능을 소개합니다.  이 예제에서는 DecisionTree 모델을 학습합니다. External 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example6/manual_2.5.4.BatchMLTrainInSingleEngine.md)|
|2|CSV 파일 Parquet로 저장하기|사용자 CSV 파일을 Parquet 형태의 파일로 변환하여 저장하는 방법에 대해 설명합니다. Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example6/manual_2.7.3.CSV2Parquet.md)|
|3|AutoML 학습하기|AutoML 학습 예제를 소개합니다. Atelier 의 AutoML 기능은 PySpark 을 이용하여 구현되어 있습니다.  External 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example6/manual_2.5.5.BatchAutoMLTrainInSingleEngine.md)|

<br>

#### 딥러닝 모델 학습

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|DL 모델 학습하기|텐서플로우를 이용한 GPU기반 교통속도 예측 딥러닝 모델의 학습기능을 소개합니다. External 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example7/manual_2.6.2.TrafficTraining.md)|
|2|DL 모델 주기적 학습하기|매분, 매시간, 매일, 매주, 매월, 매년 등 주기적으로 파일로부터 데이터셋을 읽어 딥러닝 학습을 수행하고, 그 결과로 새롭게 학습된 모델의 신규버전이 담긴 디렉토리와 파일을 생성하는 예제입니다. External 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example7/manual_2.5.10.HourlyTensorflowTraining.md)|
|3|Atelier와 연동을 위한  TensorFlow 학습코드 작성 가이드|TensorFlow 학습코드와 Atelier의 연동을 위한 Python 코드 작성 방법에 대해 설명합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example7/manual_2.9.1.KSB_TfPyCoding_Guide.md)|

<br>

## Workflow Example::학습 모델 서빙하기

#### 머신러닝 모델 서빙

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|ML 모델 온디맨드 서빙|AutoML 로 학습하여 저장되어있는 머신러닝 모델을 이용하여 예측결과를 파일로 저장 기능을 소개합니다.  Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example8/manual_2.5.6.BatchMLPredictInSingleEngine.md)|
|2|ML 모델 스트림 서빙|온도 센서로부터 입력되는 데이터를 수집 후 전처리를 수행하여 머신러닝 모델에게 스트림 형태로   5분 후 방의 온도의 예측 요청을하고, 온도 예측 결과를 Kafka 스트림형태로 받는 예제를 설명합니다.  MiniBatch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example8/manual_2.5.7.RealtimeIngestToPredictInSingleEngine.md)|

<br>

#### 딥러닝 모델 서빙

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|DL 모델 온디맨드 서빙|TensorFlow 기반 Inception 모델을 이용한 이미지 분류 RESTful 서비스 예제를 소개합니다.  모델의 서빙에 특정 GPU를 이용하고, GPU 메모리의 사용량을 설정하고, 모델을 자동 로딩하는 설정 방법도 함께 소개합니다.  OnDemandServing 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example9/manual_2.5.11.InceptionServingExample.md)|
|2|DL 모델 스트림 서빙 (교통 속도 예측 모델)|교통센서로부터 시계열 속도 데이터를 수집한 후 전처리를 수행하고, TensorFlow 기반  딥러닝 모델로부터 예측된 속도를 클라이언트로 전달하는 예제를 설명합니다. Stream 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example9/manual_2.6.4.TrafficStreamServing.md)|
|3|DL 모델 스트림 서빙 (MNIST 모델)|Kafka 스트림으로 입력받은 이미지(Tensor) 데이터를  텐서플로우 기반의 MNIST 모델을 이용하여 분류한 결과를 Kafka 스트림으로 출력하는 예제를 설명합니다.  Stream 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example9/manual_2.5.12.TfStreamPredictionMnist.md)|

<br>

#### 융합서빙

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|융합 REST API 만들기|사용자가 프로그래밍한 Python 모듈을 Dockerize를 이용하여 REST API로 만들고,   다수의 REST API 들을 연결하여 서빙하는 예제를 설명합니다. OnDemandPipeServing 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example10/manual_2.5.13.ConvergedServingEndToEndExample.md)|

<br>

## Workflow Example::학습 모델 검증 및 평가하기 튜토리얼

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|모델 검증 및 평가 시나리오 이해하기|학습 모델 검증 및 평가하기 튜토리얼 설명에 앞서 실시간 시계열 데이터 기반의 모델 검증 및 평가 절차의 전체적인 시나리오에 대해 설명합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example11/manual_2.8.1.ValidationEvaluation.md)|
|2|실시간 시계열 데이터 처리하기|[워크플로우 1] 교통속도 데이터를 실시간으로 처리하여 학습 데이터 형태로 바꾸는 워크플로우를 소개합니다.  MiniBatch 엔진과 Stream 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example11/manual_2.8.2.RealtimeData.md)|
|3|주기적 재학습하기|[워크플로우 2] 실시간 정제 및 처리된 데이터를 기반으로 모델 재학습 과정을 수행하는 워크플로우를 소개합니다.  FileSystem 엔진, Batch 엔진, External 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example11/manual_2.8.3.Retrain.md)|
|4|실시간 추론하기|[워크플로우 3] 실시간 추론 결과를 kafka로 전송하는 한편 모델 성능 평가를 위해 '평가데이터 DB'에 저장하는 워크플로우를 소개합니다.  MiniBatch 엔진, Stream 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example11/manual_2.8.4.RealtimeInference.md)|
|5|학습 모델 검증하기|[워크플로우 4] (재)학습된 딥러닝 모델을 실제 필드에 Deploy할지의 여부를 결정하는 워크플로우를 소개합니다.  FileSystem 엔진, Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example11/manual_2.8.5.ModelValidation.md)|
|6|모델 성능 실시간 평가하기|[워크플로우 5] 현재 서비스에 활용되고 있는 인공지능 모델의 성능을 실시간으로 평가하는 워크플로우를 소개합니다. FileSystem 엔진, Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example11/manual_2.8.6.ModelEvaluation.md)|
|7|다중 학습 모델 성능 평가하기|[워크플로우 6] 모델 저장소에 있는 최근 n개의 RNN 딥러닝 모델을 가져와 검증 데이터로 각 모델들의 성능을 평가하는 방법을 소개합니다.  FileSystem 엔진, Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example11/manual_2.8.7.MultiModelValidation.md)|
|8|자동 라벨 생성하기|시계열 데이터 기반 예측 서비스에 적용 가능한 자동 라벨링 기능을 소개합니다. Batch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example11/manual_2.8.8.AutoLabeling.md)|
|9|Atelier SparkML 모델 성능 측정하기|Atelier에서 Binary Classification, Multi-class Clasisfication, Regression 모델들의 성능을 측정하는 방법을 소개합니다.  Batch 엔진 또는 MiniBatch 엔진을 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example11/manual_1.0.5.PerformanceMeasure.md)|

<br>

## Workflow Example::교통 속도 예측 튜토리얼

|번호|제목|내용|링크|
|:---:|---|---|:---:|
|1|교통속도 스트림 데이터 전처리하기|교통센서로부터 수집되는 실시간 속도 스트림 데이터가 5분 단위 평균 속도 스트림 데이터로 정제되어 입력될 때, 해당 스트림 데이터를 텐서플로우 학습코드의 입력 데이터 형태로 전처리하고, 파일로 저장하는 예제를 설명합니다.  MiniBatch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example12/manual_2.6.1.TrafficPreprocessing.md)|
|2|교통속도예측 텐서플로우 모델 학습하기|텐서플로우를 이용한 GPU기반 교통속도 예측 딥러닝 모델 학습기능을 소개합니다.  External 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example12/manual_2.6.2.TrafficTraining.md)|
|3|온디맨드 방식으로 교통속도예측 모델 서빙하기|텐서플로우 기반 딥러닝 모델을 이용한 교통 속도 예측 RESTful 서비스 예제를 설명합니다. OnDemandServing 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example12/manual_2.6.3.TrafficOnDemandServing.md)|
|4|스트림 방식으로 교통속도예측 모델 서빙하기|교통센서로부터 시계열 속도 데이터를 수집한 후 전처리를 수행하고, 텐서플로우 기반 딥러닝 모델에 의해 예측된 속도를 클라이언트로 전달하는 예제를 설명합니다.   MiniBatch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example12/manual_2.6.4.TrafficStreamServing.md)|
|5|실시간 시계열 교통속도 센서스트림 처리하기|교통센서로부터 동시에 무작위로 들어오는 실시간 속도 스트림 데이터를 수집하여 시간 순으로 데이터를 처리해서 5분 단위 평균 속도로 스트림데이터로 전처리하는 예제를 설명합니다.  Stream 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example12/manual_2.6.5.TrafficTimeseriesProcessing.md)|
|6|실시간 시계열 교통속도 센서스트림 처리 및 텐서플로우 모델 서빙하기|교통센서로부터 시계열 속도 데이터를 수집한 후 전처리를 수행하고, 텐서플로우 기반 딥러닝 모델에 의해 예측된 속도를 클라이언트로 전달하는 예제를 설명합니다.  Stream 엔진, MiniBatch 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example12/manual_2.6.6.TrafficStreamingPredict.md)|
|7|실시간 시계열 교통속도 센서스트림 처리/학습/예측 및 주기적 모델 갱신하기|교통센서로부터 시계열 속도 데이터를 수집한 후 전처리를 수행하고, 텐서플로우 기반 딥러닝 모델에 의해 예측된 속도를 클라이언트로 전달하는 한편, 전처리한 속도 데이터를 계속해서 파일에 저장한 후 텐서플로우 기반 딥러닝 모델을 주기적으로 학습하는 예제를 설명합니다.  Stream 엔진, MiniBatch 엔진, External 엔진을 활용합니다.|[바로가기](https://gitlab.com/shinc.re/atelier/-/blob/main/Atelier_Manual/Workflow_Example12/manual_2.6.7.TrafficEndToEnd.md)|

<br><br>
<br><br>

# 2021 Hackathon Tizen with Atelier

2021 Hackathon Tizen with Atlier 관련자료는 아래 링크를 참고하시기 바랍니다.

[2021 Hackathon Tizen with Atelier](https://gitlab.com/shinc.re/atelier/-/blob/main/Hackathon_2021/2021_Hackathon_Tizen_with_Atelier.md)
